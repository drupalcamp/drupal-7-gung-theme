var symbol = '&';
var introVideo = 2046357428001;
var videoViewCount = 0;
var player;
var modVP;
var modExp;
var mpvideoid;
var adModule;
var modSocial;
var previousVideoID = 0;
var nextVideo = 0;
var startingVideo = 0;
var activeVideoNID = 0;
var globalCarouselControl;
var modIntroVP, modIntroExp, introPlayer;
var expertVidNids = [];
var videoList = [];
var queuedVids = [];
var currentVid = '';
var adDuration = '0';
var playerRebuilt = '';
var jumpVid = '';



// Gung: fixed the hardcode express checkout code
var membershipCode = "";
if (location.hostname == 'gungwang.com') {
  membershipCode = 'AXmCJasEjk6PM5EmJBqi5XVbUQT_rNmNO8A52yCVbp4';
} else if (location.hostname == 'dev2.gungwang.com') {
  membershipCode = 'fd5S84Qp9uRaMNnjEJCL2nyaQINptGAVolSzSFAt0b0';
} else {
  membershipCode = 'rZmTWHTVvknuErdN6AzGJkyK-fBm8NbRdjurdTwJ8lE';
}
//var membershipCode= session["MEMBERSHIPCODE"];

var navigate = '<div class="slide-nav"><a class="jcarousel-prev jcarousel-prev-horizontal">&lt;</a><a class="jcarousel-next jcarousel-next-horizontal">&gt;</a></div>';

window.alert = function(text) {
  //console.log('tried to alert: ' + text);
  location.reload(true);
};

function GetURLParameter(sParam) {
  var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split('&');
  //console.log('sParam:' + sParam + ' :: GetURLParameter: ' + sURLVariables);

  for (var i = 0; i < sURLVariables.length; i++) {
    var sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] == sParam) {
      return sParameterName[1];
    }
  }
}

function isPlaylistList() {
  if (jQuery('#playlist_list_table').length) {
    return true;
  } else {
    return false;
  }
}


function videoLock() {

  if (Drupal.settings.userid == 0 && jQuery('body').hasClass('node-type-videos')) {
    // console.log('not logged in');
    var videolock = localStorage.getItem("videolock");
    // console.log('videolock is ' + videolock);
    var redirection = window.location.href;
    //console.log("destination2=" +  window.location.pathname );

    if (!videolock) {
      videolock = 1;
      try {
          localStorage.setItem('videolock', 1);
      } catch (e) {
          Storage.prototype._setItem = Storage.prototype.setItem;
          Storage.prototype.setItem = function() {};
          alert('Your web browser does not support storing settings locally. In Safari, the most common cause of this is using "Private Browsing Mode". Some settings may not save or some features may not work properly for you.');
        }
    }

    // Gung: 3/22/2016: unauthorized user can watch all videos. change 3 to 10000
    // Gung: 6/224/2016: change back to 3
    if (videolock >= 3) {
      jwplayer("main_player").stop();
      // var oops_lockup = '<div class="video-lockout"><div class="left-vid-sm"><span class="lg-red-ttl">OOPS! </span> <span class="lg-black-stl">Sign Up to Continue Watching Videos. </span> </div> <div class="right-vid-sm"> <img src="/sites/all/themes/gung_theme/images/premium/oops-small.jpg" alt="Oops Sign Up" /> </div> <div class="clear-all"> <a href="/commerce-express-checkout/2/' + membershipCode + '" class="red-button">Signup for Premium Access</a> <br /> <a class="grey-button" href="/kith-register/nojs/choice">Click for Standard Access</a> </div> </div>';
      // Gung: 10/14/2016
      //var oops_lockup = '<div class="video-lockout"><div class="left-vid-sm"><span class="lg-red-ttl">OOPS! </span> <span class="lg-black-stl">Sign Up to Continue Watching Videos. </span> </div> <div class="right-vid-sm"> <img src="/sites/all/themes/gung_theme/images/premium/oops-small.jpg" alt="Oops Sign Up" /> </div> <div class="clear-all"> <a href="/kith-register/nojs/choice?expresscheckout=go-premium-to-view&"  class="red-button">Signup for Premium Access</a> <br /> <a class="grey-button" href="/kith-register/nojs/choice">Click for Standard Access</a> </div> </div>';
      // Gung: 9-29-17, 10-13-2017
      var oops_lockup = '<div class="video-lockout"><div class="left-vid-sm"><span class="lg-red-ttl">OOPS! </span> <span class="lg-black-stl">Sign Up to Continue Watching Videos. </span> </div> <div class="right-vid-sm"> <img src="/sites/all/themes/gung_theme/images/premium/oops-small.jpg" alt="Oops Sign Up" /> </div> <div class="clear-all"> <a class="grey-button signup-free-bar" href="/kith-register/nojs/choice?destination=' + redirection + '">Sign up for FREE  Access</a></div></div>';

      jQuery('#video_player_wrapper').addClass('videocountwrapper');

      if (!jQuery("#responsive-indicator").is(":visible")) {
        clickCreateAccount();
      }

      setTimeout(function() {
        jQuery('#video_player_wrapper #video_player_inner_wrapper').html(oops_lockup);
        // console.log('video locked');
      }, 3000);


    } else {
      // console.log('add to video lock');
      videolock++;
      //  jQuery.cookie('videocount', videocount);
      localStorage.setItem("videolock", videolock);
    }

  }

}

function videoCount() {
  //  var foo = localStorage.getItem("bar");
  //   localStorage.setItem("bar", foo);
  // var videocount = jQuery.cookie('videocount');
  var videocount = localStorage.getItem("videocount");

  if (!videocount) {
    videocount = 1;
    localStorage.setItem("videocount", 1);

    //    jQuery.cookie('videocount', videocount, { expires: 1, path: '/' });
  } else {
    videocount++;
    //  jQuery.cookie('videocount', videocount);
    localStorage.setItem("videocount", videocount);
  }

  // var videomod = videomod%3;
  console.log('videocount is' + videocount);

  if (videocount > 2) {
    videoLock();
  }

  //  console.log('viewcount modulus 3 is '+videomod);
  if (videocount % 3 == 0) {
    return true;
  } else {
    return false;
  }

}

function setMPvideoid(id) {
  mpvideoid = id;
  console.log('mpvideoid is');
  console.log(mpvideoid);
}


function setCurrentVideo(PlayingVideo) {
  //console.log(PlayingVideo);
  //console.log(PlayingVideo.id);

  currentVid = PlayingVideo.id;
  //console.log(currentVid);
  //console.log(PlayingVideo.id);
}

function setPreRollAds(adServerURL) {
  var adPolicy = new Object();
  adPolicy.adServerURL = adServerURL;
  adPolicy.prerollAds = true;
  adPolicy.prerollAdKeys = "LR_PUBLISHER_ID=18291&LR_SCHEMA=vast2-vpaid&LR_CONTENT=6&LR_AUTOPLAY=1";
  adModule.setAdPolicy(adPolicy);
}

/*
  Gung: AWS project on Oct. 31, 2017
  This function is not used by any pages
  Change => return "";
*/
function buildPlayerParams() {
  return "";

  //console.log('Current Video is '+currentVid);
  var mpbcexperience = jQuery("#main-player-info").data("mp-bcexperience");
  var mpwidth = jQuery("#main-player-info").data("mp-width");
  var mpheight = jQuery("#main-player-info").data("mp-height");
  var mpplayerid = jQuery("#main-player-info").data("mp-player-id");
  var mpplayerkey = jQuery("#main-player-info").data("mp-player-key");

  if (mpvideoid == '') {
    mpvideoid = jQuery("#main-player-info").data("mp-video-id");
  }

  if (mpvideoid == '') {
    console.log('mpvideoid is null');
    if (currentVid == '') {
      console.log('currentVid is null');
      modVP.getCurrentVideo(setCurrentVideo);
    }


    if (currentVid !== '' && mpvideoid == '') {
      mpvideoid = currentVid;
    }

    console.log(mpvideoid);

    // jQuery('#now_playing').html('this is a test '+mpvideoid);

    //  mpvideoid = queuedVids[0];
  }
  if (mpvideoid == '') {
    //mpvideoid = queuedVids[0];
    console.log('still null');
  }

  if (adDuration == '0') {

  }

  console.log("mpvideoid=" + mpvideoid);
  newParam = '<object id="' + mpbcexperience + '" class="BrightcoveExperience" width="' + mpwidth + '" height="' + mpheight + '"><param name="width" value="' + mpwidth + '" /><param name="height" value="' + mpheight + '" /><param name="isVid" value="true" /><param name="isUI" value="true" /><param name="dynamicStreaming" value="true" /><param name="includeAPI" value="true" /><param name="bgcolor" value="#FFFFFF" /><param name="playerID" value="' + mpplayerid + '" /><param name="playerKey" value="' + mpplayerkey + '" /><param name="@videoPlayer" value="' + mpvideoid + '" /> <param name="additionalAdTargetingParams" value="LR_DURATION="' + adDuration + '" /><param name="templateLoadHandler" value="myTemplateLoaded" /><param name="templateReadyHandler" value="onTemplateReady" /></object><!-- dynamic swap-->';

  return newParam;

}

// Gung: AWS project on Nov. 1st, 2017
function rebuildBCExperience(experienceid, newParam) {
  return "";

  jQuery("#myExperience").remove();
  document.getElementById("video_player").innerHTML = newParam;
  brightcove.createExperiences();

  console.log('experience has been rebuilt');
  playerRebuilt = true;
}


function videoLimit() {
  var count = jQuery.cookie('videoLimit');
  //console.log('count is '+count);

  if (!count) {
    count = 1;
    jQuery.cookie('videoLimit', count, {
      expires: 1,
      path: '/'
    });
  } else {
    count++;
    jQuery.cookie('videoLimit', count);
  }

  if (count > 3 && !jQuery('.page-home').length &&
    (!Drupal.settings.authenticated_user || Drupal.settings.pre_authorized_user)) {
    return true;
  } else {
    return false;
  }
}

function viewCount() {
  //console.log('viewCount was called');
  var viewcount = jQuery.cookie('viewCount');

  if (!viewcount) {
    viewcount = 1;
    jQuery.cookie('viewCount', viewcount, {
      expires: 1,
      path: '/'
    });
  } else {
    viewcount++;
    jQuery.cookie('viewCount', viewcount);
  }

  var viewmod = viewcount % 3;
  //console.log('viewcount modulus 3 is '+viewmod);
  if (viewcount % 3 == 0) {

    //console.log(jQuery(window).width());

    // jQuery("a[title='Create new account']").trigger("click");

    // console.log('viewed 3 things prompt to register');

  }

  //console.log('viewcount is '+viewcount);

}

function highlightActiveVideo(nid) {
  jQuery('.video-search-result-video').removeClass('active');
  jQuery('.video-search-result-video').removeClass('loading');

  var video_div = jQuery('#video-' + nid);
  video_div.addClass('active');


  if (globalCarouselControl) {
    var parent = video_div.parent();
    var jCarouselIndex = parent.attr('jcarouselindex');
    globalCarouselControl.scroll(jCarouselIndex - 2);
  }
}

// Gung: AWS project on Nov. 1st, 2017
/*
function myTemplateLoaded(experienceID) {
  player = brightcove.api.getExperience(experienceID);
  modVP = player.getModule(brightcove.api.modules.APIModules.VIDEO_PLAYER);
  modExp = player.getModule(brightcove.api.modules.APIModules.EXPERIENCE);
}
*/
// Gung: AWS project on Nov. 1st, 2017
/*
function introTemplateLoaded(experienceID) {
  introPlayer = brightcove.api.getExperience(experienceID);
  modIntroVP = introPlayer.getModule(brightcove.api.modules.APIModules.VIDEO_PLAYER);
  modIntroExp = introPlayer.getModule(brightcove.api.modules.APIModules.EXPERIENCE);
  //modIntroExp.addEventListener(brightcove.api.events.ExperienceEvent.TEMPLATE_READY, introOnTemplateReady);
}
*/
// Gung: AWS project on Nov. 1st, 2017
/*
function onTemplateReady(evt) {
  console.log('brightcove.api.events.MediaEvent.COMPLETE: '+ brightcove.api.events.MediaEvent);
  modVP.addEventListener(brightcove.api.events.MediaEvent.COMPLETE, onExpertComplete);

  if (startingVideo) {
    modVP.loadVideoByID(startingVideo);
    modVP.addEventListener(brightcove.api.events.MediaEvent.COMPLETE, onMediaComplete);
    modVP.addEventListener(brightcove.api.events.MediaEvent.CHANGE, onMediaChange);
  }


  /*Detect chrome and pepflashplayer.dll  https://code.google.com/p/chromium/issues/detail?id=140620
   Display error message
   */
  /*

   var type = 'application/x-shockwave-flash';
   var mimeTypes = navigator.mimeTypes;

   if (Drupal.settings.authenticated_user && !Drupal.settings.pre_authorized_user && mimeTypes && mimeTypes[type] && mimeTypes[type].enabledPlugin &&
   (mimeTypes[type].enabledPlugin.filename == "pepflashplayer.dll" ||
   mimeTypes[type].enabledPlugin.filename == "libpepflashplayer.so"
   )) {
   jQuery('body').prepend('<div id="chrome_plugin" style="display:none">We have detected a chrome flash plugin conflict that is <a target="_blank" href="https://code.google.com/p/chromium/issues/detail?id=140620">known</a> to cause audio and video to be out of sync</div>');
   jQuery('#chrome_plugin').slideDown();;
   }
   */

// }

function setVidList(videoArray) {
  //console.log('setVidList is called');
  //console.log(videoArray);
  //console.log(videoArray);

  if (videoArray.length > 0) {
    //console.log('setVidList is going to queue');
    //queuedVids = videoArray;
    videoList = videoArray;
  } else {
    //console.log('not an array or length 0');
  }
}


function setVidqueue(videoArray) {
  //console.log('setVidqueue Called');
  //console.log(videoArray);
  //console.log(videoArray.length);

  //video queue variable
  if (videoArray.length > 0) {
    queuedVids = videoArray;
  } else {
    console.log('not array or length 0');
  }

}


// Gung: AWS project on Oct. 31, 2017
function advanceVidqueue(videoid) {
  return "";

  //console.log(videoid);
  //console.log(videoList);
  //console.log(jQuery.inArray(videoid, videoList));
  var currentpos = jQuery.inArray(videoid, videoList);

  //console.log('current pos is'+currentpos);

  if (currentpos == 0) {
    //console.log('currpos is 0 man');

    queuedVids = videoList;
    queueVidstoPlay();

    //setCurrentVideo(videoid);
    //queueRestart();

    //  setCurrentVideo(videoid);
    //  setVidqueue(videoList);
    //  queueVidstoPlay();
    //advanceVidqueue(videoid);

    //location.reload();
    //videoList = queuedVids;
    //setVidqueue(videoList);

    //queueVidstoPlay();
    //queueRestart();
    //   setVidqueue(videoList);
    //   setTimeout(function(){queueVidstoPlay();
    //   }, 3000);
    //buildexpertVideoList();
    //queuedVids = videoList;


  } else if (currentpos > -1) {

    //console.log('currentpos is '+currentpos);

    queuedVids = _.rest(videoList, currentpos);
    //console.log(queuedVids);
    queueVidstoPlay();
  } else if (currentpos == -1) {

    // console.log('currentpos is '+currentpos);

    //console.log(videoList);
    buildexpertVideoList();
    queuedVids = _.rest(videoList, currentpos);
    //console.log(queuedVids);
    queueVidstoPlay();
    var expertVids = [];
    jQuery('a.expert-play-vid').each(function(i) {
      // console.log(jQuery(this).data('bc-video-id'));
      expertVids.push(jQuery(this).data('bc-video-id'));
    });
    //console.log(expertVids);
    //(expertVids);
    setVidqueue(expertVids);
    // queueVidstoPlay();
    advanceVidqueue(videoid);

  }

  //Find current position of video in videoList
  //create a queuedVids array consisting of the remaing vids
  //Stop current video from playing
  // queue new vidlist to play through

}

function swapExpertBio(expertBio, expertTitleLink) {
  jQuery('#block-kih-experts-kih-experts-more-from .content #experts_bio').html(expertBio);
  jQuery('#block-kih-experts-kih-experts-more-from .content #experts_bio .more-expert-vids-link').remove();
  jQuery('#block-kih-experts-kih-experts-more-from .content #title').html(expertTitleLink);
}

// Gung: AWS project on Oct. 31, 2017
function buildexpertVideoList() {
  return "";

  var expertVids = [];
  // console.log('buildexpertVideoList was called');
  if (jQuery("body").hasClass("node-type-playlist-old")) {

    // console.log('first check passed');
    jQuery('a.expert-play-vid').each(function(i) {
      // console.log(jQuery(this).data('bc_video_id'));
      // console.log(jQuery(this).data('bc-video-id'));
      expertVids.push(jQuery(this).data('bc-video-id'));
    });
    //	console.log(expertVids[0]);
    console.log(expertVids);
    //console.log('node expert page');

    setVidList(expertVids);
    setVidqueue(expertVids);
    setTimeout(function() {
      queueVidstoPlay();
    }, 3000);

  }
}

// Gung: AWS project on Oct. 31, 2017
function queueRestart() {
  return "";

  console.log('Vids queue to play');
  //modVP.loadVideoByID(currentVid);
  changeVids(currentVid);

  swapExpertBio(jQuery('.teaser-bio-data[data-bc-video-id="' + currentVid + '"]').html(), jQuery('.teaser-bio-data[data-bc-video-id="' + currentVid + '"] .more-expert-vids-link').html());

  var videoNID = jQuery('.video-search-result-video[data-bc-video-id="' + currentVid + '"]').attr('id');

  if (!videoNID) {
    videoNID = jQuery('.expert-play-vid[data-bc-video-id="' + currentVid + '"]').data('nid');
  } else {
    videoNID = videoNID.substr(6);
  }

  //var testNID =   jQuery('.expert-play-vid[data-bc-video-id="'+currentVid+'"]').data('nid');
  //console.log('test is'+testNID);
  //console.log('videoNID = '+ videoNID);
  jQuery('#active_video_nid').text('' + videoNID);
  jQuery('.expert-play-vid[data-bc-video-id="' + currentVid + '"]').parent('.video-search-result-video').addClass('active');
  jQuery('.nowPlayingMask').hide();
  jQuery('.expert-play-vid[data-bc-video-id="' + currentVid + '"]').siblings('.nowPlayingMask').show();

}

// Gung: AWS project on Oct. 31, 2017
function queueVidstoPlay() {
  return "";

  console.log('Vids queue to play');
  console.log(introVideo);

  console.log(videoCount());
  //var currentVid = '';
  //Play through all
  // play through entire queue, if queue is null then play entire list from page
  if (queuedVids.length > 0) {
     console.log('queued vids is'+queuedVids);
    currentVid = queuedVids.shift();
     console.log(currentVid);
    jQuery('.video-search-result-video').removeClass('active');
    var vidincrease = videoCount();

    if (vidincrease == true) {
      adDuration = '15';
      var newplayer = buildPlayerParams();
      rebuildBCExperience("myExperience", newplayer);
    } else if (playerRebuilt == true) {
      adDuration = '0';
      //   console.log('playerRebuilt is true in queueVids');
      var newplayer = buildPlayerParams();
      rebuildBCExperience("myExperience", newplayer);
      playerRebuilt = false;
    } else {
      adDuration = '0';
      playerRebuilt = false;
      //	  console.log('playerRebuilt is false');
    }

    try {
      //modVP.loadVideoByID(currentVid);
      //  changeVids(currentVid);
      //videoCount();
      videoLock();

    } catch (error) {
      //   console.log(error.message);
      try {
        setTimeout(function() {
          //modVP.loadVideoByID(currentVid);
          //   changeVids(currentVid);
          //videoCount();
          videoLock();
        }, 3000);
      } catch (error) {
        setTimeout(function() {

          //   modVP.loadVideoByID(currentVid);
          //   changeVids(currentVid);

          //videoCount();
          videoLock();
        }, 10000);
      }
    }

    console.log(jQuery('.expert-play-vid[data-bc-video-id="'+currentVid+'"]').closest('teaser-bio-data').html());


    swapExpertBio(jQuery('.teaser-bio-data[data-bc-video-id="' + currentVid + '"]').html(), jQuery('.teaser-bio-data[data-bc-video-id="' + currentVid + '"] .more-expert-vids-link').html());

    var videoNID = jQuery('.video-search-result-video[data-bc-video-id="' + currentVid + '"]').attr('id');

    if (!videoNID) {
      videoNID = jQuery('.expert-play-vid[data-bc-video-id="' + currentVid + '"]').data('nid');
    } else {
      videoNID = videoNID.substr(6);
    }

    //var testNID =   jQuery('.expert-play-vid[data-bc-video-id="'+currentVid+'"]').data('nid');
    console.log('test is'+testNID);


    console.log('videoNID = '+ videoNID);

    jQuery('#active_video_nid').text('' + videoNID);
    jQuery('.expert-play-vid[data-bc-video-id="' + currentVid + '"]').parent('.video-search-result-video').addClass('active');
    jQuery('.nowPlayingMask').hide();
    jQuery('.expert-play-vid[data-bc-video-id="' + currentVid + '"]').siblings('.nowPlayingMask').show();

  }

}

function changeVids(vid) {
  myPlayer.catalog.getVideo(vid, function(error, video) {
    console.log(video)
    myPlayer.catalog.load(video);
    myPlayer.play();
  });
}

// Gung: AWS project on Oct. 31, 2017
function onExpertComplete(evt) {
  return "";

  console.log('onExpertComplete is complete');

  if (jQuery('.playlist-next-backbone').length) {
    jQuery('.playlist-next-backbone').trigger("click");
    console.log('.playlist-next-backbone');
  }
  else if (jQuery('.view-videos-by-expert a.more-expert-vids').hasClass('playnext')
              || jQuery('#block-apachesolr-search-mlt-001 a').hasClass('playnext')) {
    console.log('refresh is about to be called');
    refreshvid();
  } else {
    //console.log(evt);
    console.log('queueVidstoPlay is about to be called');
    queueVidstoPlay();
  }

}


function introOnTemplateReady(evt) {
  if (startingVideo) {
    // modIntroVP.loadVideoByID(startingVideo);
    changeVids(startingVideo);
  }
}


function onMediaComplete() {
  console.log('onMediaComplete is complete');
  var active_link = jQuery('.video-search-result-video.active');
  if (active_link.length) {
    var active_item;
    var next_item;

    if (isPlaylistList()) {
      //Travers up table
      active_item = active_link.parent().parent();
      next_item = active_item.next();
      next_item = jQuery('.video-search-result-video', next_item);
    } else {
      active_item = active_link.parent();
      next_item = active_item.next();
    }

    if (next_item.length) {
      jQuery('a.use-ajax-vid', next_item).trigger('click');
    }
  }
}

function closeVideoExperience() {
  jQuery('#video_player_wrapper #video_player, .field-name-field-video-source').addClass('limit_reached');
  //if(!Drupal.settings.pre_authorized_user) {
  if (true) {
    //First Link in Ajax register links section is login
    //clickCreateAccount();
    jQuery('#video_player_wrapper #video_player, .field-name-field-video-source').html(
      '<h2>To continue watching videos <a href="javascript: void(0);" onClick="clickLogin()" >Login</a> or <a href="javascript: void(0);" onClick="clickCreateAccount()" >Create an Account</a>.</h2><br>' +
      '<ul><li>Watch, save and share unlimited videos</li><li>Discuss parenting issues in private forums</li><li>Get recommended videos & tips specific for you</li><li>Meet new parents and parenting professionals</li><li>Access quick answers on the go via mobile</li></ul>'
    );
  } else {
    jQuery('#validation-reminder a').trigger('click');
    jQuery('#video_player_wrapper #video_player').html('Validate your email to watch videos.  ');
  }
}

/*
function swapPlayer() {
}
function onMediaChange(evt) {
}
*/
function clickCreateAccount() {
  jQuery('.ajax-register-links a[title="Create new account"]').trigger('click');
}

function clickLogin() {
  jQuery('.ajax-register-links a[title="Login"]').trigger('click');
}

function playVideo(videoId) {
  var docHeight = jQuery(document).height() + 50;
  var docWidth = jQuery(document).width();
  jQuery('#modalBackdropManual').css('top', 0).css('height', docHeight + 'px').css('width', docWidth + 'px').show();
  jQuery('#video-player-intro-video #videos_player_close').click(function() {
    jQuery('#modalBackdropManual').hide();
  });

  introVideo = videoId;
  if (modIntroVP) {
    //  modIntroVP.loadVideoByID(introVideo);
    changeVids(introVideo);
  } else {
    //Its being initialized so save in global variable
    startingVideo = introVideo;
  }
}

function carousel_initCallback(carousel) {
  globalCarouselControl = carousel;
}


function refreshvid() {
  var nextvid = jQuery('.playnext').attr('href') + '?qt-more_videos=1#qt-more_videos';
  console.log('refreshvid(): next video is '.nextvid);
  window.location.href = nextvid;
}

function openVideoPlayer() {
  jQuery('#block-kih-videos-kih-primary-video-player').show();
}

function closeVideoPlayer() {
  jQuery('#block-kih-videos-kih-primary-video-player').hide();
  jQuery('#video-player-intro-video').hide();
}


function setBetaCookie() {
  jQuery.cookie('termsAgreement', 2, {
    expires: 365,
    path: '/'
  });
}

/***** GW: Main function *******/
(function($) {
  //console.log('Start function($)');
  // Constant for setting the second part of the page title in Ajax operations
  var titleSuffix = " | gungwang.com";
  var home = 'home';
  var savetid = 'all';


  jQuery.fn.center = function() {
    this.css("position", "absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
      $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
      $(window).scrollLeft()) + "px");
    return this;
  }


  $(document).ready(function() {

    //console.log('document.ready function');

    var uid = Drupal.settings.userid;
    /*   Gung: disable the Ads block-block-214
            setTimeout(function () {

                if (jQuery('#block-block-214').height() == 0) {
                    console.log('no Google ad');
                    jQuery('#block-block-214 .block-inner').html(
                        '<div style="text-align: center; padding-bottom: 10px;"><a href="/baby-care-101"><img src= "https://gungwang.com/sites/default/files/ad-premium-babycare-728x90.jpg" Alt="Baby Care 101" /></a></div>'
                    );
                }
                ;
                if (jQuery('#block-block-215').height() == 0) {
                    jQuery('#block-block-215 .block-inner').html(
                        '<div style="text-align: center;"><a href="/go-premium-to-view"><img src= "https://gungwang.com/sites/default/files/ad4-premium-300x250.jpg" Alt="Premium Advertisement" /></a></div>'
                    );
                }
            }, 2000);
    */
    if (Drupal.settings.premium == false) {


      if (uid == 0 || uid == '' || typeof uid == 'undefined') {
        jQuery('.dl-menu a[href="/premium"]').addClass('premium-tab');

        console.log('not logged in and not premium');
        jQuery('.premium-tab').removeAttr("href");
        jQuery('.premium-tab').click(function() {

          window.location.href = '/go-premium-to-view';

        });

      } else {
        jQuery('.premium-tab').removeAttr("href");
        jQuery('.premium-tab').click(function() {
          window.location.href = '/go-premium-to-view';
        });
      }

      if (window.location.pathname == '/premium') {
        if (uid == 0 || uid == '' || typeof uid == 'undefined') {
          // console.log('not logged in');
          //clickCreateAccount();
          //$('.premium-tab').removeAttr("href");
          window.location.href = '/go-premium-to-view';
        } else {
          //  console.log('redirect to upgrade page');
          window.location = '/go-premium-to-view';
        }
      }

    }


    console.log('uid is ' + Drupal.settings.userid);

    if (uid != 0 && uid != '' && typeof uid != 'undefined') {
      //console.log('authenticated');
    } else {
      //console.log('anon');
      if (videoCount()) {
        //   clickCreateAccount();
      }

    }


    $('a.close').html('X');

    var ref = '';
    ref = GetURLParameter('ref');
    //console.log('ref = ' + ref);
    //console.log(document.location);
    var currentURL = document.location.pathname;
    var refreshvid = '';

    $('.view-videos-by-expert a.more-expert-vids').each(function() {
      if (refreshvid == 'TRUE') {
        $(this).addClass('playnext')
        refreshvid = '';
      } else {
        var expertlink = $(this).attr('href');
        //console.log('expertlink = ' + expertlink);
        if (expertlink.indexOf(currentURL) >= 0) {
          //console.log('Gung: check passed');
          $(this).addClass('active nowplaying');
          if (GetURLParameter('qt-more_videos') == 1) {
            refreshvid = 'TRUE';
          }
        }
      }
    });






    $('#block-apachesolr-search-mlt-001 td a').each(function() {
      //$(this).addClass('playnext');
      refreshvid = '';
    });


    // Google GA Object
    if (ref != null) {
      var _gaq = _gaq || [];
      _gaq.push(['_setCustomVar', 5, 'Referred By', ref, 1]);
    }


    //console.log('GW: page is ready');
    //buildPlayerParams();

    if ($('body').hasClass('not-logged-in')) {
      console.log('not logged in');
      viewCount();
      $('.video-search-result-video').click(function() {
        viewCount();
      });
    }

    // Mobile Tabs on Video Page
    $('.tab').click(function() {
      $('#tabs_container > .tabs > li > a.active')
        .removeClass('active');
      $(this).addClass('active');

      $('#tabs_container > .tab_contents_container > div.tab_contents_active')
        .removeClass('tab_contents_active');
      $(this.rel).addClass('tab_contents_active');
    });


    var cache = [];
    var imagePath = '/sites/all/themes/gung_theme/images/';
    // Arguments are image paths relative to the current page.
    jQuery.preLoadImages = function() {
      var args_len = arguments.length;
      for (var i = args_len; i--;) {
        var cacheImage = document.createElement('img');
        cacheImage.src = arguments[i];
        cache.push(cacheImage);
      }
    }

    if (!Drupal.settings.authenticated_user) {
      jQuery.preLoadImages(imagePath + "register_bg.png", imagePath + "register_box_bg.png");
    }
    jQuery(".jcarousel-skin-gungwang ul.jcarousel").hide().css({
      "position": "relative",
      "left": "inherit"
    }).fadeIn(1100);


    //**** HASHCHANGE - this is what orchestrates the loading of a page based on URL
    // if there is a change in url (back/forward button) trigger this
    // jQuery(window).bind("hashchange", function (e) {
    //console.log('updating content');
    //url = jQuery.bbq.getState('page');

    // console.log('Url is '+url);

    //   );

    //	 if(url) {
    //  console.log('Url is '+url);
    //console.log('video id queue is '+jQuery('a[href="'+url+'"]').data('bc-video-id'))
    // currentVid = jQuery('a[href="'+url+'"]').data('bc-video-id');

    //  if(jQuery("body").hasClass("page-taxonomy-term")){
    //  jumpVid = jQuery('a[href="'+url+'"]').data('bc-video-id');
    //	}
    //	else if(jQuery("body").hasClass("node-type-expert")){
    //  jumpVid = jQuery('a[href="#page='+url+'"]').data('bc-video-id');
    //	}


    //	 get_vid_block(url);

    //}
    /*
     if (!url) {
     // back to home page
     home = 'home'; //back on home page
     jQuery(document).attr('title', 'Home' + titleSuffix);
     home = 'home'; //back on home page
     } else {
     if (url.search(/^\/video\//) !== -1) {
     if (jQuery('#block-views-slider-block').length != 0) {
     console.log('Hiding carousel, if it exists...'); // Repetitive, and a better way may exist.
     jQuery('#block-views-slider-block').hide('fast');
     } else {
     console.log('Not clicking the carousel...');
     }
     get_vid_block(url);

     var title = url.replace("/video/",""); // remove /video/ from title
     title = title.replace(/-/g,' '); // re-spacify
     title = title.replace(/\w\S /g, function(txt){
     return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
     }); // convert to title case
     jQuery(document).attr('title', title + titleSuffix);
     } else {
     if (url.search(/^\/categories\//) !== -1) {
     get_cat_block(url); // Is this happening? -K Fri, Jul 13, 2012 at 12:10 PM PDT
     } else {
     //search page
     get_search_block(url);
     jQuery(document).attr('title', 'Search: ' + url  + titleSuffix);
     }
     }
     }
     // FIXME this is a hack.  Hide the auto-complete box on each hashchange
     jQuery(".ac_results").hide();*/


    // });


    var count = jQuery.cookie('termsAgreement');
    if (count != 2) {
      //jQuery('#beta-form a').trigger('click');
    }


    if (jQuery('.video-search-result-video').length) {

      //If on User Page, than don't play playlist automatically
      if (jQuery('.page-user').length) {
        return;
      }

      var id = jQuery('#default_id').html();
      var firstList, url;

      if (id) {
        firstList = jQuery('#video-' + id);
      } else {
        firstList = jQuery('.video-search-result-video').first();
      }

      url = jQuery('a.use-ajax-vid', firstList).attr('href');

      //  var currentState = jQuery.bbq.getState('page');
      //if(!currentState) {
      //	  jQuery.bbq.pushState({
      //          'page': url
      //      });
      //  }
    }

    jQuery(document).bind('eventAfterRate', function(event, data) {

      var widget = jQuery('.rate-widget-' + data.widget_id);
      var text = jQuery('.rate-widget-label', widget).html();

      var counter = parseInt(jQuery('.rate-counter').html());

      if (text == 'Like') {
        counter++;
        widget.addClass('active');
      } else {
        counter--;
        widget.removeClass('active');
      }
      jQuery('.rate-counter').html(counter);
    });

    $i = 0;
    jQuery('.view-expert-directory .views-row').each(function() {
      random = Math.ceil(Math.random() * 10);
      if (random == 0) {
        random = 1;
      }
      jQuery(this).css('margin-top', random);

      if ($i % 6 == 0) {
        jQuery(this).css('clear', 'left');
      }
      $i++;
    });


  });

  Drupal.behaviors.rateActiveState = {
    attach: function(context, settings) {
      var widget = jQuery('.rate-widget-2');
      var text = jQuery('.rate-widget-label', widget).html();
      if (text != 'Like') {
        widget.addClass('active');
      } else {
        widget.removeClass('active');
      }

    }
  }

  Drupal.behaviors.placeHolders = {
    attach: function(context, settings) {
      jQuery('input, textarea').placeholder();
    }
  }


  Drupal.behaviors.attachHashChange = {
    attach: function(context, settings) {

      //check if video link is clicked (modules/custom/kih_views_bundle/kih_views_bundle.views_default.inc)
      jQuery("a.use-ajax-vid").once('click-processed').bind("click", function(event) {
        // event.preventDefault();


        //Show Throbber
        var parent = jQuery(this).parent('.video-search-result-video');
        parent.addClass('loading');


        //  var url = jQuery(this).attr('href');
        // jQuery.bbq.pushState({
        //    'page': url
        //});
      });
    }
  }


  Drupal.behaviors.homepageCarousel = {
    attach: function() {
      // jcarousel shenanigans
      // hide the out-of-site carousel and then position it so we can fade in

    }
  };


  // get the video and play or display the expert info
  function get_vid_block(url) {
    //console.log('get_vid_block called this will be removed later');

    //jQuery('#block-kih-experts-kih-experts-more-from .content').html(jQuery().html);

  }

  function socialIconsUpdate(socialButtonsHTML) {

    //jQuery('#social_buttons').html(socialButtonsHTML);
    var script = 'https://s7.addthis.com/js/300/addthis_widget.js?domready=1#pubid=xa-50be98440a7f3ec0';
    if (window.addthis) {
      window.addthis = null;
      window._adr = null;
      window._atc = null;
      window._atd = null;
      window._ate = null;
      window._atr = null;
      window._atw = null;
    }
    jQuery.getScript(script);
  }


  function addActiveVideoNIDToArgs() {
    jQuery('#video_info .button a').each(function() {

      //Re-attach cTools Modal information becase we changed the href
      jQuery(this).removeClass('ctools-use-modal-processed');
      jQuery(this).unbind('click');
      Drupal.behaviors.ZZCToolsModal.attach();
    });
    $("#video_info SELECT").selectBox();

  }

  //display category carousels - only called in prev function
  function get_cat_block(url) {
    //$("#search-results").html('<li>Loading results...</li>');
    $.getJSON(
      url + '/ajax',
      function(data) {}).success(function(data) {
      if (!jQuery.isEmptyObject(data.search_result)) {
        var result = "";
        //$('#search-results').html(result);
        result_list = $('.item-list', data.search_result);
        $('#results-wrapper').html(result_list.html()); // trim out the surrounding divs on the result
        $('#search-results-list').html(data.search_result_list);

        // tweak the results display because of absolute positioning issue
        $('#results-wrapper').addClass("category-results-wrapper");
        $('#search-results-list').addClass("category-results-list-wrapper");

        // use URL to find title of Category in megamenu.  90% evil, 100% genius
        categoryTitle = $("#block-megamenu-main-menu a[href$=\"" + url + "\"]").html();
        $('#results-wrapper ul').before('<h3 class="search-term">"' + categoryTitle + '"</h3>');

        var selectedval = $("input[@name=whichview]:checked").val();
        if (selectedval == 'thumb') {
          $('#search-results-thumb').show();
          $('#search-results-list').hide();
        } else {
          $('#search-results-thumb').hide();
          $('#search-results-list').show();
        }
      } else {
        //$('#search-results').html('<li>No results.</li>');
      }
    }).error(function() {
      //$('#search-results').html('<li>Error.</li>');
    }).complete(function() {
      $('#search-tab li').removeClass('active-tab');
      $('#search-tab').fadeIn().find('li.content-result').addClass('active-tab');
      $('#content').hide();
      $('#results').show();
      $('#results-wrapper').jcarousel('destroy');
      $('#results-wrapper').jcarousel({
        'wrap': 'circular'
      }).after(navigate);

      $('.slide-nav .jcarousel-prev').click(function() {
        $(this).parents('.block-content').find('.item-list').jcarousel('scroll', '-=5');
      });

      $('.slide-nav .jcarousel-next').click(function() {
        $(this).parents('.block-content').find('.item-list').jcarousel('scroll', '+=5');
      });
    });
  }

  //****

  /**
   *  Javascript Tools to handle Forms in FAdmashion
   */

  jQuery(document).ready(function() {
    clearAllForms();

    $('.comment-read-more').click(function(event) {
      //  console.log(event)
      $(event.target).parent('div').hide();

      //  console.log(event.target.id);
      var showid = 'full-' + event.target.id;
      //  console.log(showid);

      $('#' + showid).slideDown();

      //alert('this was clicked');
    });


    $('.comment-read-less').click(function(event) {
      // console.log(event)
      $(event.target).parent('div').slideUp();

      // console.log(event.target.id);
      var showid = 'short-' + event.target.id;
      // console.log(showid);

      $('#' + showid).fadeIn();

      //alert('this was clicked');
    });


    if (!jQuery.isFunction(jQuery.validator)) {
      return;
    }

    //ADD validator for clear forms
    jQuery.validator.addMethod("notEqual", function(value, element, param) {
      return (this.optional(element) || value !== param);
    }, "This field is required");
  });

  /*
   * Code for clearing defaults in forms
   */
  function clearAllForms() {
    //Call the Form clear and restore
    jQuery("input.clear-defaults").cleardefault();
    jQuery("textarea.clear-defaults").cleardefault();
    jQuery(".clear-defaults input").cleardefault();
  };

  jQuery.fn.cleardefault = function() {
    return this.focus(function() {
      if (this.value == this.defaultValue) {
        this.value = "";
        jQuery(this).removeClass('default');
      }
    }).blur(function() {
      if (!this.value.length) {
        this.value = this.defaultValue;
        jQuery(this).addClass('default');
      }
    });
  };

  (function($) {
    Drupal.behaviors.addClearForms = {
      attach: function(context, settings) {
        if (!$.isFunction($.colorbox)) {
          return;
        }
        clearAllForms();
      }
    };
  })(jQuery);


  $(document).ready(function() {


    //  $(window).trigger( 'hashchange' );  // configure the page based on the hash in the URL

    if (jQuery.browser.msie) {
      if (parseInt(jQuery.browser.version, 10) < 8) {
        if (!jQuery('.context-unsupported-browser').length) {
          window.location.href = 'https://gungwang.com/unsupported-browser';
        }
      }
    }

    if (jQuery.browser.msie && (jQuery.browser.version < 11)) {
      console.log('using old IE?');
      jQuery('.video-js').attr('style', 'position:static;');
      jQuery('.vjs-control-bar').hide();

      jQuery('.video-js').mouseenter(function() {
        jQuery('.vjs-control-bar').show();
      }).mouseleave(function() {
        jQuery('.vjs-control-bar').hide();
      });
    }


    $('#edit-sortby').change(function() {
      var selected = $('#edit-sortby option:selected').val();
      // if on home page
      if (home == 'home') {
        var url = selected;
        get_home_block(url);
      } else {
        // video page
        var url = savetid + '/' + selected;
        get_video_block(url);
      }
    });
    // home block with sort - only called in previous event handler
    function get_home_block(url) {
      $.getJSON('/homeajax/' + url + '/ajax', function(data) {}).success(function(data) {
        if (!jQuery.isEmptyObject(data)) {
          var result = "";
          //$('#search-results').html(result);
          $('#block-views-video-search-results-block .block-inner .block-content').html(data.latest);
          $('#block-views-video-search-results-block-1 .block-inner .block-content').html(data.most_pop);
          $('#block-views-video-search-results-list-block .block-inner .block-content').html(data.latest_list);
          $('#block-views-8ab0f97376fb4affd962a1ac599b8156 .block-inner .block-content').html(data.most_pop_list);
        }
      }).error(function() {
        //$('#search-results').html('<li>Error.</li>');
      }).complete(function() {
        $('#block-views-video-search-results-block, #block-views-video-search-results-block-1').find('.item-list').jcarousel({
          'wrap': 'circular'
        }).after(navigate);
      });
    }

    // get video block with sort - only called in previous event handler
    function get_video_block(url) {
      $.getJSON('/videoajax/' + url + '/ajax', function(data) {}).success(function(data) {
        if (!jQuery.isEmptyObject(data)) {
          var result = "";
          //$('#search-results').html(result);
          $('#block-views-video-search-results-block-4 .block-inner .block-content').html(data.like_vids);
          $('#block-views-9d0528c831e5b3adb31134f3ff7844e3 .block-inner .block-content').html(data.like_vids_list);
        }
      }).error(function() {
        //$('#search-results').html('<li>Error.</li>');
      }).complete(function() {
        $('#block-views-video-search-results-block-4, #block-views-video-9d0528c831e5b3adb31134f3ff7844e3').find('.item-list').jcarousel({
          'wrap': 'circular'
        }).after(navigate);
      });
    }

    //Toggle between the two search tabs (page.tpl.php)
    $('#search-tab li a').click(function() {
      //$('#content').hide();
      var tab = $(this).attr('href');
      $(tab).show();
      $('#search-tab li').removeClass('active-tab');
      $(this).parents('li').addClass('active-tab');
      if (tab == '#content') {
        $('#results').hide();
      } else {
        $('#content').hide();
      }
      return false;
    });


    // ****


    // **** MEGAMENU!  TOO BIG FOR ONE COMMENT!
    // check if category link is clicked
    /*jQuery("a[href*='videos/categories']").bind( 'click', function (event) {
     event.preventDefault();

     //Hide all Dropdown containers
     $('.li_container').hide();

     var url = $(this).attr('href');
     var pos = url.search(/categories\//);
     if ( pos !== -1) {
     url = url.substring(pos).split('/');
     var category_id = url[1];

     window.location.href = Drupal.settings.video_player_category_path + "&category_id=" + category_id;

     }
     });
     // ****

     /* **** MEGAMENU!  Expert Categories!
     // check if category link is clicked
     jQuery("a[href*='experts/']").bind( 'click', function (event) {
     event.preventDefault();

     //Hide all Dropdown containers
     $('.li_container').hide();

     var url = $(this).attr('href');
     var pos = url.search(/experts\//);
     if ( pos !== -1) {
     url = url.substring(pos).split('/');
     var category_id = url[1];

     window.location.href = Drupal.settings.video_player_category_path + "&category_id=" + category_id;

     }
     });*/
    // ****


    // when table header tab is clicked for sort
    $("body").delegate(".view-expert-list th.views-field a", "click", function(event) {
      event.preventDefault();
      var url = $(this).attr('href');
      get_expert_modal(url);
    });

    // sort block in modal
    $('#sort_modal_block').change(function() {
      var selected = $('#sort_modal_block option:selected').val();
      var url = '/expert-modal/all/' + selected;
      get_expert_modal(url);
    });

    function get_expert_modal(url, callback) {
      // show ems
      $('#ems_display').show();
      // and hide expert
      $('#em_display').hide();
      $('#expert_modal_window').tabs();
      if (!url) {
        url = '/expert-modal';
        //$('#ui-tab-dialog-close').append($('a.ui-dialog-titlebar-close'));
      }
      $('#abc, #def, #ghi, #jkl, #mno, #pqr, #stu, #vwx, #yz, #all').html('<li>Loading results...</li>');
      // load the expert list
      load_expert_list_data(url, callback);

      //display modal
      $('#expert_modal_window').dialog({
        dialogClass: "expert_modal_window",
        position: 'center',
        modal: true,
        draggable: true,
        resizable: false,
        height: 500,
        width: 960
      });
    }

    // Event handler for expert a-z tabs
    $('#alpha_tabs li a').click(function() {
      var tab = $(this).attr('href');
      $('.tab-content').hide();
      $(tab).show();
      // show ems
      $('#ems_display').show();
      // and hide expert
      $('#em_display').hide();
      return false;
    });

    // expert modal (see util.module ~107)
    $('#em_tabs li a').click(function() {
      $('#em_tabs li').removeClass('active');
      var tab = $(this).attr('href');
      $('.tab-content').hide();
      $(this).parent('li').addClass('active');
      $(tab).show();
      return false;
    });


    //**** ALSO LOGIN/REGISTER and NEWSLETTER
    // check if expert link is clicked
    // This selector "a.use-modal" affects "Login/Register", "Newsletter", and expert names links.
    $("body").delegate("a.use-modal", "click", function(event) {
      event.preventDefault();
      var url = $(this).attr('href');
      var urls = url.split('/');
      // search is giving the base url
      if (urls[0] == 'http:') {
        url = '/' + urls[3] + '/' + urls[4];
      }
      get_vid_block(url);
    });

    //check if video link is clicked
    $("body").delegate("a.use-modal-ajax", "click", function(event) {
      event.preventDefault();
      var url = $(this).attr('href');
      // var url = $(this).attr('href');is this a video url?

      if (url.search(/^\/video\//) !== -1) {
        $('#block-views-slider-block').hide();
        $('#block-utils-primary-video-player').css('height', '396');
        $.bbq.pushState({
          'page': url
        });
        // close modal window
        $('#expert_modal_window').dialog("close");
        get_vid_block(url);
      }

    });

    // expert is clicked in expert modal
    $("body").delegate(".view-expert-list td.views-field a", "click", function(event) {
      event.preventDefault();
      var url = $(this).attr('href');

      // find first letter of last name
      var lastName = getLastNameFor(this.innerHTML);
      var letter = lastName.charAt(0);

      var section = getSectionFor(letter);
      // make a selector and add selected classes
      var selector = "li#" + section + "-tab a";

      // NOTE this doesn't work if either we guessed the wrong tab, or the tab wasn't loaded before.
      // unselect current tab
      //$("#alpha_tabs li").removeClass("ui-tabs-selected ui-state-active");
      //$(selector).addClass("ui-tabs-selected ui-state-active");

      // Select the tab by clicking on it.  This will preload the tab's data, so if user clicks tab there's something to show.
      $(selector).click();

      // load content
      get_vid_block(url);
    });


    // show expert modal information - only called in function above
    // Gung: AWS project on Nov. 2nd, 2017; not used by any code;
    function show_expert(rtn) {
      return "";

      $('#expert_modal_window').tabs();
      // hide ems
      $('#ems_display').hide();
      // and show expert
      $('#em_display').show();
      // set up the content
      $('#em_expert_name').html(rtn.node.title);
      if (rtn.node.field_job_title.und[0]['safe_value']) {
        $('#em_areas').html(rtn.node.field_job_title.und[0]['safe_value']);
      }
      $('#em_vid').html("Videos");
      $('#em_vid').html(rtn.expert_videos);
      $('#em_vid').show();

      if (rtn.node.field_expert_bio.und[0]['safe_value']) {
        $('#em_bio').html(rtn.node.field_expert_bio.und[0]['safe_value']);
      } else {
        $('#em_tabs .tab-bio').hide();
      }

      //$('#em_reso').html("Resources");
      if (rtn.node.field_expert_resources.und[0]['safe_value']) {
        $('#em_reso').html(rtn.node.field_expert_resources.und[0]['safe_value']);
      } else {
        $('#em_tabs .tab-resor').hide();
      }

      if (rtn.node.field_expert_tips.und[0]['safe_value']) {
        $('#em_tips').html("<div class='label'>My 5 Favorite Tips</div>" + rtn.node.field_expert_tips.und[0]['safe_value']);
      }
      $("#em_links").html('');
      $.each(rtn.node.field_expert_links.und, function(index, data) {
        $("#em_links").append("<div><a href='" + this.url + "' target='_blank'>" + this.title + "</a></div>");
      });

      var faceit = "https://www.facebook.com/sharer.php?u=" + window.location.protocol + "//" + window.location.host + "/node/" + rtn.node.nid + "&t=" + encodeURI(rtn.node.title);
      var tweetit = "https://twitter.com/share?text=" + encodeURI(rtn.node.title) + "&url=" + window.location.protocol + "//" + window.location.host + "/node/" + rtn.node.nid;
      $("#em_social .facebook a").attr('href', faceit);
      $("#em_social .twitter a").attr('href', tweetit);
      // close expert list modal window (if open) reason to put it here is the time before the new one opens

      // choose the right tab
      // find first letter of last name
      var lastName = getLastNameFor(rtn.node.title);
      var letter = lastName.charAt(0);

      var section = getSectionFor(letter);
      // make a selector and add selected classes
      var selector = "li#" + section + "-tab";

      // Select the tab by clicking on it.  This will preload the tab's data, so if user clicks tab there's something to show.
      //console.log("tab: " + selector);

      $("#alpha_tabs li").removeClass("ui-tabs-selected ui-state-active");
      $(selector).addClass("ui-tabs-selected ui-state-active");

      // show the window
      $('#expert_modal_window').dialog({
        modal: true,
        draggable: true,
        resizable: false,
        height: 550,
        width: 960
      });
      //create video player template
      BCL.playerTemplate = "<div style=\"display:none\"></div><object id=\"myExperience1\" class=\"BrightcoveExperience\"><param name=\"bgcolor\" value=\"#FFFFFF\" /><param name=\"width\" value=\"256\" /><param name=\"height\" value=\"124\" /><param name=\"playerID\" value=\"1649028586001\" /><param name=\"playerKey\" value=\"AQ~~,AAABM-VRYqE~,LzoKrsWwczf3ZAjUAQTjCEqiVZUWZJQV\" /><param name=\"isVid\" value=\"true\" /><param name=\"isUI\" value=\"true\" /><param name=\"dynamicStreaming\" value=\"true\" /><param name=\"@videoPlayer\" value=\"" + rtn.expert_video_id + "\"; /><param name=\"templateLoadHandler\" value=\"BCL.onTemplateLoaded\"</object>";
      // insert into block
      $('#em_expert_intro_video').html(BCL.playerTemplate);
      //start player
      brightcove.createExperiences();

      // check if expertlist data is loaded if not load into the background
      if ($('#a-z-tab').hasClass('tab-hide')) {
        var url = '/expert-modal';
        load_expert_list_data(url);
      }
    }

    // loading the expertlist data into the containers.
    function load_expert_list_data(url, callback) {
      $.getJSON(url, function(data) {}).success(function(data) {
        if (!jQuery.isEmptyObject(data.abc)) {
          $('#abc').html(data.abc);
          $('.abc').removeClass('tab-hide');
        }
        if (!jQuery.isEmptyObject(data.def)) {
          $('#def').html(data.def);
          $('.def').removeClass('tab-hide');
        }
        if (!jQuery.isEmptyObject(data.ghi)) {
          $('#ghi').html(data.ghi);
          $('.ghi').removeClass('tab-hide');
        }
        if (!jQuery.isEmptyObject(data.jkl)) {
          $('#jkl').html(data.jkl);
          $('.jkl').removeClass('tab-hide');
        }
        if (!jQuery.isEmptyObject(data.mno)) {
          $('#mno').html(data.mno);
          $('.mno').removeClass('tab-hide');
        }
        if (!jQuery.isEmptyObject(data.pqr)) {
          $('#pqr').html(data.pqr);
          $('.pqr').removeClass('tab-hide');
        }
        if (!jQuery.isEmptyObject(data.stu)) {
          $('#stu').html(data.stu);
          $('.stu').removeClass('tab-hide');
        }
        if (!jQuery.isEmptyObject(data.vwx)) {
          $('#vwx').html(data.vwx);
          $('.vwx').removeClass('tab-hide');
        }
        if (!jQuery.isEmptyObject(data.yz)) {
          $('#yz').html(data.yz);
          $('.yz').removeClass('tab-hide');
        }
        if (!jQuery.isEmptyObject(data.all)) {
          $('#all').html(data.all);
          $('.a-z').removeClass('tab-hide');
        }
      }).error(function() {
        $('#abc').html('<li>Error Loading results...</li>');
      }).complete(new function(callback) {
        if (callback) {
          callback();
        }
      }(callback)); // pass callback to function https://benalman.com/news/2010/11/immediately-invoked-function-expression/
    }

    // Given a name, return the last name
    function getLastNameFor(name, nameSeparator) {
      // break by commas to separate titles
      var commas = name.split(',');
      // 0th entry is name part.  Break by spaces to separate names
      if (nameSeparator == undefined) nameSeparator = ' ';
      var spaces = commas[0].split(nameSeparator);
      // very last name is last name.
      return spaces[spaces.length - 1];
    }

    // Given a letter, return the "section" that includes it
    function getSectionFor(letter) {

      letter = letter.toLowerCase();
      var section = "all"

      // find tab to click
      switch (letter) {
        case "a":
        case "b":
        case "c":
          section = "abc";
          break;
        case "d":
        case "e":
        case "f":
          section = "def";
          break;
        case "g":
        case "h":
        case "i":
          section = "ghi";
          break;
        case "j":
        case "k":
        case "l":
          section = "jkl";
          break;
        case "m":
        case "n":
        case "o":
          section = "mno";
          break;
        case "p":
        case "q":
        case "r":
          section = "pqr";
          break;
        case "s":
        case "t":
        case "u":
          section = "stu";
          break;
        case "v":
        case "w":
        case "x":
          section = "vwx";
          break;
        case "y":
        case "z":
          section = "yz";
          break;
      }
      return section;
    }

    // ****


    // **** This is a hack for the errant autosuggestion box in issue #62.
    // Evil solution to # 62
    setInterval(function() {
      $(".ac_results").filter( // Find all ac_results
        function() {
          return $(this).offset().top == 0; // filter out only those that are at 0px
        }
      ).hide(); // hide them
    }, 500); // every 500ms


  }); //document.ready

  /* LILA STUFF STARTS HERE*/
  $(function() {

    /* Main Nav Menu :
    //********* Gung removed the drop-down submenu on Dec. 8, 2017  ***********
    $("#nav-menu-block li.menu-parent").hoverIntent(function() { //user mouses over main link
      $(this).find("h2 a").addClass("active");
      $(this).find(".container").fadeIn("fast");
      //console.log("GW: menu-parent");
    }, function() {
      if (!$(this).find(".container").data('hover')) { //user mouses out of main link and container
        $(this).find("h2 a").removeClass("active");
        $(this).find(".container").hide();
        //console.log("GW: menu-parent hide");
      }
    });

    $("#nav-menu-block li.menu-parent .container").hover( //use this instead of is(':hover') for Firefox
      function() {
        $.data(this, 'hover', true);
        //console.log("GW: menu-parent hover");
      },
      function() {
        $.data(this, 'hover', false);
      }
    ).data('hover', false);

    */

    /* Feedback Form default text */
    $(".webform-client-form #edit-submitted-comments, .webform-client-form #edit-submitted-comments--2").live("focus", function() {
      if ($(this).val() == "enter comments...") $(this).val("");
    }).live("blur", function() {
      if ($(this).val().trim() == "") $(this).val("enter comments...");
    });


    /* Forum AJAX Login links */
    $(".forum-header .ajax-register-links li a").each(function() {
      $(this).addClass("submit-button");
      if ($(this).html() == "Create My Account") $(this).html("Join");
    });

    /* Forum Featured Advice fader */
    $('.view-featured-forum-topic-comments .view-content').cycle({
      fx: 'fade'
    });


    /* User Register form */
    //change Terms & Conditions text
    $("#user-register-form .form-item-legal-accept label").html('I have read and agree to abide by the <strong><a href="/terms">Gung Wang Terms of Use</a></strong> and <strong><a href="/privacy-policy">Gung Wang Privacy Policy</a></strong>.');


    /* User Login non-AJAX form */
    //$("body.page-user-me-edit form#user-profile-form input.password-field").val("");


    /* Playlists edit form */
    var is_editor = ($("#is_editor").val() == 1);
    if (!is_editor) {
      $("input[id^=edit-kih-video-node-references-und-add-more]").each(function() { //remove Add Another buttons
        $(this).closest("div.clearfix").remove();
      });
    }
    $("table[id^=kih-video-node-references-values] .form-text").each(function() {
      var video_title = $(this).val();
      if (video_title == "") {
        if (!is_editor) $(this).closest("tr").remove(); //remove the blank additional row
      } else {
        var video_html = "<div id=\"saved_title\" style=\"display: none\">" + video_title + "</div>"; //save title for Undelete
        video_title = truncate(video_title.substring(0, video_title.indexOf(" [nid")), 40);
        video_html += "<div class=\"playlist-video-title\">" + video_title + "</div><div class=\"playlist-video-delete\"><a href=\"javascript:void(0)\">Delete</a></div>";
        $(this).hide(); //hide the input box and
        $(this).closest("div").append(video_html); //append video title and Delete link
      }
    });
    $("table[id^=kih-video-node-references-values] .playlist-video-delete").live("click", function() { //when user clicks Delete
      if ($(this).find("a").html() == "Delete") { //Delete
        $(this).siblings("input").val(""); //set input value = "" so the video gets deleted on Save
        $(this).closest("tr").addClass("disabled");
        $(this).find("a").html("Undelete");
        if ($(this).closest("table").siblings(".tabledrag-changed-warning").size() == 0) {
          //$(this).closest("table").before("<div class=\"tabledrag-changed-warning messages warning\" style=\"display: none;\"><span class=\"warning tabledrag-changed\">*</span>Changes made in this table will not be saved until the form is submitted.</div>");
          //$(this).closest("table").prev().fadeIn();
        }
      } else { //Undelete
        var orig_title = $(this).siblings("#saved_title").html();
        $(this).siblings("input").val(orig_title); //set input value back to original title
        $(this).closest("tr").removeClass("disabled");
        $(this).find("a").html("Delete");
      }
    });

    //set up Confirmation dialog on Delete playlist
    $(".page-user-me-playlists input[id^=edit-delete]").click(function(event) {
      return confirm("Are you sure you want to delete this playlist?");
    });


    /* User Profile Menu tabs */
    //fix VIew tab not having active class
    if (location.pathname == "/user/me") {
      $("#block-menu-menu-user-profile-menu li:first-child a").addClass("active-trail");
    }

    //
    jQuery('#user-name').textfill({
      debug: true,
      innerTag: 'span',
      minFontPixels: 10,
      maxFontPixels: 14,
      explicitHeight: 27,
      callback: userProfessionTextFill
    });

    function userProfessionTextFill() {
      var height = jQuery('#user-name').height();
      //console.log(height)
      var explicitheight;
      if (height > 20) {
        explicitheight = 13;
      } else {
        explicitheight = 27;
      }
      jQuery('#user-profession').textfill({
        debug: true,
        innerTag: 'span',
        minFontPixels: 9,
        explicitHeight: explicitheight,
        maxFontPixels: 14
      });
    }

    /* User Profile form */
    /* tabs */
    var cur_profile_tab = -1; //no tab selected initially
    $("#user-profile-tabs-list li").click(function() { //on tab click, show/hide content
      var i = $(this).index() + 1;
      if (i != cur_profile_tab) {
        $("#user-profile-content #content-" + i).show();
        $("#user-profile-content #content-" + i).addClass("active");
        $("#user-profile-tabs #tab-" + i).addClass("active");
        if (cur_profile_tab > 0) {
          $("#user-profile-content #content-" + cur_profile_tab).hide();
          $("#user-profile-content #content-" + cur_profile_tab).removeClass("active");
          $("#user-profile-tabs #tab-" + cur_profile_tab).removeClass("active");
        }
        cur_profile_tab = i;
      }
      //add tab as URL hash, e.g. /user/me/edit#family
      var hashloc = location.href.lastIndexOf("#");
      if (hashloc == -1) hashloc = location.href.length;
      if ($(this).text() == "About You") location.href = location.href.substring(0, hashloc) + "#";
      else if ($(this).text() == "Family") location.href = location.href.substring(0, hashloc) + "#family";
      else if ($(this).text() == "Account Information") location.href = location.href.substring(0, hashloc) + "#account";
    });
    //load User Profile form tabs based on URL hash
    if (location.pathname.match(/user\/.*\/edit/)) { //matches user/me/edit or user/USERID/edit (loaded during password reset)
      var lastchar = document.location.toString().substr(document.location.toString().length - 1);
      if (location.hash == "#family") $("#user-profile-tabs-list li").filter(':contains("Family")').click();
      else if (location.hash == "#account") $("#user-profile-tabs-list li").filter(':contains("Account Information")').click();
      else if (lastchar == "#") $("#user-profile-tabs-list li").filter(':contains("About You")').click();
      else if (document.location.search.match(/pass-reset-token=/)) $("#user-profile-tabs-list li").filter(':contains("Account Information")').click(); //user is resetting password, so show Account Info tab by default
      else $("#user-profile-tabs-list li").filter(':contains("About You")').click();
    }

    /* username / real name */
    $username = $("div.form-item-field-username-or-real-name-und input[value=0]");
    $realname = $("div.form-item-field-username-or-real-name-und input[value=1]");
    //if no display name is selected, check Username
    if (!$username.attr("checked") && !$realname.attr("checked")) {
      $("div.form-item-field-username-or-real-name-und input[value=0]").attr("checked", "checked");
    }

    //if user has no First/Last, don't allow Real Name
    $("input#edit-field-first-name-und-0-value").change(function() {
      $("input#edit-field-first-name-und-0-value").val($.trim($("input#edit-field-first-name-und-0-value").val()));
      checkRealName();
    });
    $("input#edit-field-last-name-und-0-value").change(function() {
      $("input#edit-field-last-name-und-0-value").val($.trim($("input#edit-field-last-name-und-0-value").val()));
      checkRealName();
    });

    function checkRealName() {
      if (($("input#edit-field-first-name-und-0-value").val() == "") && ($("input#edit-field-last-name-und-0-value").val() == "")) {
        $username.attr("checked", "checked");
        $realname.attr("disabled", "disabled");
      } else {
        $realname.attr("disabled", "");
      }
    }

    /* user button hover options */
    $("#user-profile .user-button").mouseenter(function() { //user mouses over main link
      $("#user-profile .user-button-options").fadeIn("fast");
    }).mouseleave(function() {
      if (!$("#user-profile .user-button-options").data('hover')) { //user mouses out of main link and container
        $("#user-profile .user-button-options").fadeOut("fast");
      }
    });

    $("#user-profile .user-button-options").hover( //use this instead of is(':hover') for Firefox
      function() {
        $.data(this, 'hover', true);
      },
      function() {
        $.data(this, 'hover', false);
      }
    ).data('hover', false);


    /* Friends */
    /* tabs */
    var cur_friends_tab = -1; //no tab selected initially
    $("#user-friends-tabs-list li").click(function() { //on tab click, show/hide content
      var i = $(this).index() + 1;
      if (i != cur_friends_tab) {
        $("#user-friends-content #content-" + i).show();
        $("#user-friends-content #content-" + i).addClass("active");
        $("#user-friends-tabs #tab-" + i).addClass("active");
        if (cur_friends_tab > 0) {
          $("#user-friends-content #content-" + cur_friends_tab).hide();
          $("#user-friends-content #content-" + cur_friends_tab).removeClass("active");
          $("#user-friends-tabs #tab-" + cur_friends_tab).removeClass("active");
        }
        cur_friends_tab = i;
      }
      //add tab as URL hash, e.g. /friends#received
      var hashloc = location.href.lastIndexOf("#");
      if (hashloc == -1) hashloc = location.href.length;
      if (i == 1) location.href = location.href.substring(0, hashloc) + "#";
      else if (i == 2) location.href = location.href.substring(0, hashloc) + "#received";
      else if (i == 3) location.href = location.href.substring(0, hashloc) + "#sent";
    });
    //load Friends tabs based on URL hash
    if (location.hash == "#received") $("#user-friends-tabs-list li#tab-2").click();
    else if (location.hash == "#sent") $("#user-friends-tabs-list li#tab-3").click();
    else $("#user-friends-tabs-list li#tab-1").click();

    /* My Playlists tabs */
    /* tabs */
    var cur_playlists_tab = -1; //no tab selected initially
    $("#user-playlists-tabs-list li").click(function() { //on tab click, show/hide content
      var i = $(this).index() + 1;
      if (i != cur_playlists_tab) {
        $("#user-playlists-content #content-" + i).show();
        $("#user-playlists-content #content-" + i).addClass("active");
        $("#user-playlists-tabs #tab-" + i).addClass("active");
        if (cur_playlists_tab > 0) {
          $("#user-playlists-content #content-" + cur_playlists_tab).hide();
          $("#user-playlists-content #content-" + cur_playlists_tab).removeClass("active");
          $("#user-playlists-tabs #tab-" + cur_playlists_tab).removeClass("active");
        }
        cur_playlists_tab = i;
      }
      //add tab as URL hash, e.g. /friends#received
      var hashloc = location.href.lastIndexOf("#");
      if (hashloc == -1) hashloc = location.href.length;
      if (i == 1) location.href = location.href.substring(0, hashloc) + "#";
      else location.href = location.href.substring(0, hashloc) + "#" + i;
    });
    //load Playlists tabs based on URL hash
    if (location.pathname == "/user/me/playlists") {
      if (location.hash.substr(1) != "") $("#user-playlists-tabs-list li#tab-" + location.hash.substr(1)).click();
      else $("#user-playlists-tabs-list li#tab-1").click();
    }

    //clear out User Profile edit form Password field being auto-populated on some Firefox browsers
    $("body.page-user-me-edit form#user-profile-form input.password-field").val("");

    if ($("#page").length) {
      /* align Footer to bottom of page (for short pages on tall displays)
       -- do this down here after everything else to make sure tabs are set up so we get proper height numbers */
      var page_top = $("#page").offset().top; //accounts for top admin bar
      var page_height = $("#page").height() + page_top;
      var win_height = $(window).height();
      if (win_height > page_height) {
        page_height = win_height;
        $("#page").height(win_height - page_top);
        var footer_top = Math.floor($("footer").offset().top);
        var footer_height = $("footer").height();
        var footer_new_top = page_height - (footer_top + footer_height);
        if (footer_new_top > 0) $("footer").css({
          "position": "relative",
          "top": footer_new_top + "px"
        });
      }
    }

    /* P2P Newest Member tabs */
    $("#search_tab_1").mousedown(function() {
      $("#search_pics_1").fadeIn("fast");
      $("#search_pics_2").fadeOut("fast");
      $("#search_tab_1").addClass("active");
      $("#search_tab_2").removeClass("active");
    });
    $("#search_tab_2").mousedown(function() {
      $("#search_pics_2").fadeIn("fast");
      $("#search_pics_1").fadeOut("fast");
      $("#search_tab_2").addClass("active");
      $("#search_tab_1").removeClass("active");
    });


    /* Responsive */
    //add body class to Category and Subcategory pages for responsive CSS
    if ($("h1.first-level-cat-title").length > 0) $("body").addClass("page-cat");
    if ($("h1.second-level-cat-title").length > 0) $("body").addClass("page-subcat");

    if ($('#main-newsletter style').length) {
      $('#main-newsletter').html($("#main-newsletter-column").html());
    }

    //check onload and after window resized
    checkResponsiveness();

    var rtime = new Date(1, 1, 2000, 12, 00, 00);
    var timeout = false;
    var delta = 200;
    $(window).resize(function() {
      rtime = new Date();
      if (timeout === false) {
        timeout = true;
        setTimeout(resizeend, delta);
      }
    });

    function resizeend() {
      if (new Date() - rtime < delta) {
        setTimeout(resizeend, delta);
      } else {
        timeout = false;
        checkResponsiveness();
      }
    }

    function checkResponsiveness() {
      if ($("#responsive-indicator").is(":visible")) {

        // Change Playlists link to not be a modal
        if ($('.oops-block a[title="Oops"]').length) {
          var oopsurl = $('.oops-block a[title="Oops"]').attr('href');
          oopsurl = oopsurl.replace('content-modal', 'node');
          $('.oops-block a[title="Oops"]').attr('href', oopsurl);
          $('.oops-block a[title="Oops"]').removeClass('ctools-use-modal ctools-modal-ctools-kith-wide-style ctools-use-modal-processed');
        }


        //Home page - move Cartoon of the Week below Featured Experts
        $("body.front #block-views-cartoon-of-the-week-block").insertAfter("body.front #block-views-featured-experts-block");
        //move Newsletter below Cartoon
        $("body.front #block-kih-downloads-kih-dls-all-parents-long").insertAfter("body.front #block-views-cartoon-of-the-week-block");
        //move Help below Newsletter
        $("body.front #block-block-200").insertAfter("body.front #block-kih-downloads-kih-dls-all-parents-long");
        //remove <br> tags from Mailchimp signup text
        $("body.front #mc_embed_signup .download-title").html('<label for="mce-EMAIL" class="main-download-title">FREE Ten Tips for Raising a Happy Child</label>Enter your email to download &amp; subscribe to our newsletter');

        //Expert Profile page - move Videos
        $("body.node-type-expert #quicktabs-more_videos_from_expert").appendTo("#region-content .region-inner");

        //Articles page -- move Latest Posts
        $("body.page-articles #block-views-articles-block-1").prependTo("body.page-articles #region-content .region-inner");
        $("body.page-articles .view-kih-voices.view-display-id-block_1 .view-footer a").html("See More");

        //Video pages -- move Disqus to bottom
        $("body.node-type-videos #disqus_thread").prependTo("body.node-type-videos #region-content .region-inner");

        //Forum move member quicktab block to bottom of forum;
        $("body.page-forum #block-quicktabs-members, . body.context-forum-topic #block-quicktabs-members").appendTo('#block-system-main');

        //Fix newsletter
        if ($('#main-newsletter style').length) {
          $('#main-newsletter').html($("#main-newsletter-column").html());
        }


        if (!$("#mobile-indicator").is(":visible")) {
          //move breadcrumbs for Tablet
          $("#block-easy-breadcrumb-easy-breadcrumb").appendTo("#region-content .region-inner");


          //Video pages -- move Expert Bio below Transcript for Tablet
          $("body.node-type-videos #block-kih-videos-kih-videos-single-transcription").prependTo("body.node-type-videos #region-preface-second .region-inner");
          $("body.node-type-videos #block-kih-experts-kih-experts-more-from").insertAfter("body.node-type-videos #block-kih-videos-kih-videos-single-transcription");
        } else {
          $("#block-easy-breadcrumb-easy-breadcrumb").appendTo("#region-header-first .region-inner");
        }
      } else {
        //move things back for Desktop
        $("body.front #block-views-cartoon-of-the-week-block").insertAfter("body.front #block-block-208");
        $("body.front #block-kih-downloads-kih-dls-all-parents-long").insertAfter("body.front #block-views-c6070c9c1575897226a422a58514e96d");
        $("body.front #block-block-200").insertAfter("body.front #block-views-cartoon-of-the-week-block");

        if ($('.oops-block a[title="Oops"]').length) {
          var oopsurl = $('.oops-block a[title="Oops"]').attr('href');
          oopsurl = oopsurl.replace('node', 'content-modal');
          $('.oops-block a[title="Oops"]').attr('href', oopsurl);
          $('.oops-block a[title="Oops"]').addClass('ctools-use-modal ctools-modal-ctools-kith-wide-style ctools-use-modal-processed');
        }


        $("body.page-forum #block-quicktabs-members, body.context-forum-topic #block-quicktabs-members").prependTo('#region-preface-second .region-inner');

        $("body.front #mc_embed_signup .download-title").html('<label for="mce-EMAIL" class="main-download-title">FREE Ten Tips for<br>Raising a Happy Child</label>Enter your email to download &amp;<br>subscribe to our newsletter');

        $("body.node-type-expert #quicktabs-more_videos_from_expert").insertAfter("body.node-type-expert #expert-profile-topbox");

        $("body.page-articles #block-views-articles-block-1").insertAfter("body.page-articles #block-system-main");
        $("body.page-articles .view-kih-voices.view-display-id-block_1 .view-footer a").html("More");

        //Video pages -- move everything back for Desktop
        $("body.node-type-videos #block-kih-experts-kih-experts-more-from").prependTo("body.node-type-videos #region-preface-second .region-inner");
        $("body.node-type-videos #block-kih-videos-kih-videos-single-transcription").insertAfter("body.node-type-videos #block-block-215");

        $("body.node-type-videos #disqus_thread").insertAfter("body.node-type-videos #region-preface-first a[name=comments]");

        $("#block-easy-breadcrumb-easy-breadcrumb").appendTo("#region-header-first .region-inner");
      }

      if ($("#mobile-indicator").is(":visible")) {
        // Profile Page
        $('.page-user #user-pic').insertBefore('#profile-accordion');
        $('.page-user #user-content').insertBefore('#user-family');

        //Category pages
        $("body.page-cat #categories-landing .taxblock .subcatlist").css('display', '');
        $("body.page-cat #categories-landing .taxblock .cat-arrow").removeClass("expanded");

        //Expert Profile pages -- move name/occupation after bio
        $("body.node-type-expert #expert-profile-topbox #expert-name").insertAfter("body.node-type-expert #expert-profile-topbox .field-name-field-profile-photo");
        $("body.node-type-expert #expert-profile-topbox #expert-job").insertAfter("body.node-type-expert #expert-profile-topbox #expert-name");

        //Blog Author pages -- move name/occupation before photo
        $("body.context-blogs .view-id-blogger_bio .views-field-name-1").prependTo("body.context-blogs .view-id-blogger_bio .views-row-1");
        $("body.context-blogs .view-id-blogger_bio .views-field-field-blogger-tagline").insertAfter("body.context-blogs .view-id-blogger_bio .views-field-name-1");
        $("body.context-blogs .view-id-blogger_bio .views-field-field-blogger-company-website").insertAfter("body.context-blogs .view-id-blogger_bio .views-field-field-blogger-tagline");
        $("body.context-blogs .view-id-blogger_bio .views-field-field-blogger-social-profiles").insertAfter("body.context-blogs .view-id-blogger_bio .views-field-field-blogger-company-website");

      } else {
        //Profile Page
        $('.page-user #user-pic').prependTo('#user-profile');
        $('#profile-accordion #user-pic').remove();

        //Category pages
        $("body.page-cat #categories-landing .taxblock .subcatlist").show();

        //Expert Profile pages
        $("body.node-type-expert #expert-profile-topbox #expert-name").insertBefore("body.node-type-expert #expert-profile-topbox .field-name-field-profile-photo");
        $("body.node-type-expert #expert-profile-topbox #expert-job").insertAfter("body.node-type-expert #expert-profile-topbox #expert-name");

        //Blog Author pages
        $("body.context-blogs .view-id-blogger_bio .views-field-name-1").insertAfter("body.context-blogs .view-id-blogger_bio .views-field-picture");
        $("body.context-blogs .view-id-blogger_bio .views-field-field-blogger-tagline").insertAfter("body.context-blogs .view-id-blogger_bio .views-field-name-1");
        $("body.context-blogs .view-id-blogger_bio .views-field-field-blogger-company-website").insertAfter("body.context-blogs .view-id-blogger_bio .views-field-field-blogger-tagline");
        $("body.context-blogs .view-id-blogger_bio .views-field-field-blogger-social-profiles").insertAfter("body.context-blogs .view-id-blogger_bio .views-field-field-blogger-company-website");

      }

    }

    //Category pages -- show/hide subcategory list on Mobile only
    $("body.page-cat #categories-landing .taxblock .cat-arrow").on('mousedown', function() {
      if ($("#mobile-indicator").is(":visible")) {
        if ($(this).hasClass("expanded")) {
          console.log('collapse');
          $(this).removeClass("expanded");
          $(this).parent().find(".subcatlist").css('display', '');
          /* doing this keeps Masonry from overlapping boxes that have been expanded and collapsed on Mobile before switching back to Tablet/Desktop */
        } else {
          console.log('expand');
          $(this).addClass("expanded");
          $(this).parent().find(".subcatlist").show();
        }
      }
    });

    //Experts pages -- move responsive submenu into Preface area
    $("body.context-experts #experts-menu-responsive").prependTo("body.context-experts #region-preface-first .region-inner");

    //Playlists page -- show/hide list on Mobile only
    $("body.context-playlists #playlist_board_wrapper .playlist_content_item_text .cat-arrow").on('mousedown', function() {
      if ($("#mobile-indicator").is(":visible")) {
        if ($(this).hasClass("expanded")) {
          console.log('collapse');
          $(this).removeClass("expanded");
          $(this).parent().find(".playlist_list").css('display', '');
          /* doing this keeps Masonry from overlapping boxes that have been expanded and collapsed on Mobile before switching back to Tablet/Desktop */
          $(this).parent().find(".experts_featured").css('display', '');
        } else {
          console.log('expand');
          $(this).addClass("expanded");
          $(this).parent().find(".playlist_list").show();
          $(this).parent().find(".experts_featured").show();
        }
      }
    });

    //Video pages -- set up video accordion
    $("body.node-type-videos #video-accordion div.related").html($("body.node-type-videos #block-apachesolr-search-mlt-001 .content").html());
    $("body.node-type-videos #video-accordion div.transcript").html($("body.node-type-videos #block-kih-videos-kih-videos-single-transcription .field-item.even").html());
    $("body.node-type-videos #video-accordion div.expert-bio").html($("body.node-type-videos #block-kih-experts-kih-experts-more-from .content").html());
    $("body.node-type-videos #video-accordion div.more-expert").html($("body.node-type-videos #quicktabs-tabpage-more_videos-1").html());

    $("body.node-type-videos #video-accordion h3").click(function() {
      if ($(this).next("div").is(":hidden")) {
        $("body.node-type-videos #video-accordion > div").slideUp();
      }
      $(this).next("div").slideToggle();
    });

    //Experts page -- set up submenu
    $("body.context-experts #experts-cat").change(function() {
      var sel_class = $(this).find("option:selected").attr("class");
      $("body.context-experts #experts-subcat option:first-child").attr("selected", true);
      if (sel_class == "all") {
        $("body.context-experts #experts-subcat").attr("disabled", "disabled");
      } else {
        $("body.context-experts #experts-subcat").removeAttr("disabled");
        $("body.context-experts #experts-subcat option:not('.all')").hide();
        $("body.context-experts #experts-subcat option." + sel_class).show();
      }
    });

    $("body.context-experts #experts-subcat").change(function() {
      var sel_val = $(this).find("option:selected").val();
      if (sel_val == "") location.href = $("body.context-experts #experts-cat option:selected").val();
      else location.href = sel_val;
    });


  });

  function truncate(str, maxchars) {
    if (str.length > maxchars) return jQuery.trim(str).substring(0, maxchars).trim(this) + "...";
    return str;
  }


})(jQuery);


/**** Gung: September 2016  ****/
jQuery(document).ready(function($) {
  var coupon1 = $("#commerce-checkout-coupon-ajax-wrapper tr.views-row-first td.views-field-granted-amount").text();
  //console.log("coupon: " + coupon1);
  if(coupon1 ){
    $("#edit-commerce-coupon-coupon-code" ).prop( "disabled", true );
    //console.log("#commerce-checkout-coupon-ajax-wrapper hide");
  }

  $("#edit-commerce-coupon .form-item-commerce-coupon-coupon-code .description").hide();
  var promoCodeHtml = '<div id="promo-code-vip-link"> <a ' +
          ' href="/vip"><i>Click here if you have VIP promo code</i></a></div>';
  $("#edit-commerce-coupon .fieldset-wrapper").append(promoCodeHtml);

});
