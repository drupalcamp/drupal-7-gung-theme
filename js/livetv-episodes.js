Drupal.behaviors.solrSearchListsContentBoard= {
  attach: function(context, settings) {
	var $container = jQuery('#videos-livetv-episodes');
    if(!$container.length) {
      return;
    }
    //console.log("container :"  + $container);
    $container.imagesLoaded( function(){
    	jQuery('.views-row', $container).removeClass('new');
    });

    jQuery.autopager({
      content: ".views-row",
      link: '#next',
      appendTo: "#videos-livetv-episodes-next",
    	start: function() {
    		jQuery('.views-row', $container).removeClass('new');
    		jQuery('#solrSearchListsLoader').fadeIn();
    	},
		 load: function(current, next){
			 var $newElems = jQuery('.new', $container).css({ opacity: 0 });
			 $newElems.imagesLoaded( function(){
		       $newElems.css({ opacity: 1 });
			   //$container.masonry( 'appended', $newElems, true );
			 });
			 jQuery('#solrSearchListsLoader').hide();
		 }
     //
    //  break: function(i) {
    //   // this.fadeIn('slow');
    //   jQuery('#solrSearchListsLoader').hide();
    //  }

	});

  }
}
