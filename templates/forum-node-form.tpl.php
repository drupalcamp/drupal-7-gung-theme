<div id="forum-node-form">
<?php
//print ctools_modal_text_button(t('Download'), 'download-manager/nojs/download/122', t('Download this asset'), 'download-button');
?>

<?php 
//print_r($form);

if (!user_access("edit any content")) {
	hide($form['field_editorial_topic']);
}
hide($form['body']['und'][0]['format']);

print drupal_render_children($form);
?>
</div>