
<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>
<?php $user_loaded = user_load($row->entity_id);

if(!isset($user_loaded->uid)) {
	 $env_id = apachesolr_default_environment();
	  module_load_include('inc', 'apachesolr', 'apachesolr.index');
	 $ret = apachesolr_index_delete_entity_from_index($env_id, 'user', $row->entity_id);
	 return;
}

?>
<div class="search_users_picture">
<?php print theme('user_picture', array('account' => $user_loaded, 'style_name'=>'forum_user_thumbnail', 'attr'=>array('class'=>'forum-user-picture'))); ?>
</div>
<div class="search_users_info" >
<?php print theme('username', array('account' => $user_loaded) );?>

<?php 

if(!user_is_logged_in()) {
	$link = '<a style="font-size: 12px;" onClick="clickLogin();" href="javascript:void(0)" >Add as Friend</a>';
} else {
	//$link = '<a style="font-size: 12px;" class="user_relationships_popup_link"  href="/relationship/' . $user_loaded->uid . '/request/1">Add as Friend</a>';
	$link = l(t('Add as Friend'), 'relationship/' . $user_loaded->uid . '/request/1', array('query' => drupal_get_destination(), 'attributes' => array('class' => 'user_relationships_popup_link', 'style' => "font-size: 12px;") ));
	
	if(isset($user->uid)) {
		$relationships = user_relationships_load(array('between' => array($user->uid, $user_loaded->uid) ));
		$friend = false;
		 foreach($relationships as $relationship) {
		 	 if ($relationship->name == 'Friend') {
		 	 	 $friend = true;
		 	 }
		 }
		 
		 if($friend) {
		 	 $link = '<span style="color: green">✓&nbsp;</span> Friends';
		 }
	}
	
	if($user->uid == $user_loaded->uid) {
		$link = '';
	}
}

?>
<div style="font-size: 12px;"><?php print $link;?></div>
<div style="font-size: 11px; font-style: italic;"> joined <?php print timeago_format_date($user_loaded->created)?>
</div>
