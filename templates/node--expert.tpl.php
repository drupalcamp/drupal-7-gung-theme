<div id="expert-profile-topbox">

  <div class="expert-titles">
    <h1 class="expert-h1"><?php print $node->title;?></h1>
    <h2><?php print render($content['field_job_title']); ?></h2>
  </div>
  <div class="social-buttons">
    <?php echo kih_videos_social_buttons($node->title, url('node/' . $node->nid, array('absolute' => true) )); ?>
  </div>
  <div class="social-buttons-clear"></div>
  <?php print render($content['field_profile_photo']); ?>

  <?php
  $bio = $node->field_expert_bio['und'][0]['value'];
  $teaser_len = 550;
  $class_a = "bio-rest";
  //print "<pre>"; print_r($bio); print "</pre>";
  $readmore = "... <br><span class='readmore-p'>".
      "<a id=\"read-more-bio\"  href=\"javascript: void(0);\" >Read more >></a></span>";
  list($summary, $rest) = kih_video_trim_text2($bio, $teaser_len, $readmore, false);

  //print $rest;
  ?>

  <div id="expert-bio-profile">
    <div id="bio-summary">
      <div class=""><?php  print $summary; //print render($content['field_expert_bio']); ?></div>
      <script>
      jQuery("#read-more-bio").click(function () {
        jQuery('#bio-rest').toggle();
        jQuery(this).text(function(i, text){
          return text === "Read more >>" ? "Read less <<" : "Read more >>";
        })
      });
      </script>
    </div>

  </div>

  <div id="bio-rest" style="display: none">
    <div class="bio-rest-content"><?php print $rest; ?></div>
    <div class=""><?php print render($content['field_expert_links']); ?></div><br />

    <?php if(!empty($content['field_experts_books'])) { ?>
      <h2>Books Written By This Expert</h2>
      <div class=""><?php print render($content['field_experts_books']); ?></div>
    <?php }?>
    <?php if(!empty($content['field_expert_resources'])) { ?>
      <h2>Recommended Resources</h2>
      <div class=""><?php print render($content['field_expert_resources']); ?></div>
    <?php }?>
    <br clear="all">
    <div><?php print kih_experts_get_video_tags($node->nid); ?></div>
  </div>

  <br clear="all">
</div>


<?php
$qmachinename = 'more_videos_from_expert';
$quicktabs = quicktabs_load($qmachinename);
$quicktabs =  quicktabs_build_quicktabs($qmachinename);
print render($quicktabs);

?>
