<?php

//Nav Menu level 1 (All Parents, Pregnancy, etc.)
/*$all_parents = taxonomy_get_children(562);
$pregnancy = taxonomy_get_children(656);
$adoption = taxonomy_get_children(604);
$first_year = taxonomy_get_children(701);
$toddler = taxonomy_get_children(745);
$preschool = taxonomy_get_children(577);
$elementary = taxonomy_get_children(578);
$teen = taxonomy_get_children(899);
$special_needs = taxonomy_get_children(549);*/

//********* Gung removed the drop-down submenu on Dec. 8, 2017  ***********
?>

<div id="nav-menu-block">
	<ul class="nav-menu">
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/all-parents">All Parents</a></span>
			<!-- <div class="container">
				<?php //include("nav-menu-table-562.tpl.php"); ?>
			</div> -->
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/pregnancy">Pregnancy</a></span>
			<!-- <div class="container">
				<?php //include("nav-menu-table-656.tpl.php"); ?>
			</div> -->
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/adoption">Adoption</a></span>
			<!-- <div class="container">
				<?php //include("nav-menu-table-604.tpl.php"); ?>
			</div> -->
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/baby">Baby</a></span>
			<!-- <div class="container">
				<?php //include("nav-menu-table-701.tpl.php"); ?>
			</div> -->
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/toddler">Toddler</a></span>
			<!-- <div class="container">
				<?php //include("nav-menu-table-745.tpl.php"); ?>
			</div> -->
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/preschooler">Preschool</a></span>
			<!-- <div class="container">
				<?php //include("nav-menu-table-577.tpl.php"); ?>
			</div> -->
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/elementary">Elementary</a></span>
			<!-- <div class="container">
				<?php //include("nav-menu-table-578.tpl.php"); ?>
			</div> -->
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/teenager">Teen</a></span>
			<!-- <div class="container">
				<?php //include("nav-menu-table-899.tpl.php"); ?>
			</div> -->
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/special-needs">Special Needs</a></span>
			<!-- <div class="container">
				<?php //include("nav-menu-table-549.tpl.php"); ?>
			</div> -->
		</li>
	</ul>


</div>
