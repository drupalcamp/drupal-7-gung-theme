<?php
//Nav Menu level 1: TEEN
$first_year = taxonomy_get_children(899);
?>

<!-- List of terms -->
<div id="cat2-list">

  <div id="cat2-list-left">
    <div class="cat2-cat3-column1 cat2-cat3">
      <?php show_submenu($first_year, 0); ?>
      <?php show_submenu($first_year, 1); ?>
      <?php show_submenu($first_year, 2); ?>
    </div>
    <div class="cat2-cat3-column2 cat2-cat3">
      <?php show_submenu($first_year, 3); ?>
      <?php show_submenu($first_year, 4); ?>
      <?php show_submenu($first_year, 5); ?>
    </div>
    <div class="clear"></div>
  </div>

  <div id="cat2-list-right">
    <div class="cat2-cat3-column3 cat2-cat3">
      <?php show_submenu($first_year, 6); ?>
      <?php show_submenu($first_year, 7); ?>
      <?php show_submenu($first_year, 8); ?>
    </div>
    <?php echo show_submenu_ads(899);?>
    <div class="cat2-cat3-column4 cat2-cat3">
      <?php show_submenu($first_year, 9); ?>
      <?php show_submenu($first_year, 10); ?>
      <?php show_submenu($first_year, 11); ?>
      <?php show_submenu($first_year, 12); ?>
      <?php show_submenu($first_year, 13); ?>
      <?php show_submenu($first_year, 14); ?>
      <div class="clear"></div>
    </div>

  </div>

  <div class="clear"></div>


</div>
<!-- END: List of terms -->
