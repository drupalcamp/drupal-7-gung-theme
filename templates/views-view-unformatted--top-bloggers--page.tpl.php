<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
 drupal_add_css(drupal_get_path('theme', 'gung_theme').'/css/style-top-blogger.css');
//print_r (arg(0));
?>
<div id="top-bloggers">

	<div class="left" >
		<?php		echo get_intro( arg(0)); ?>
	</div>
	<div class="right" >
		<?php echo get_img( arg(0)); ?>
	</div>
	<div style="clear: both; margin-bottom: 25px"></div>


	<div id="top-blogger-text">
	<div id="top-blogger-board-top"></div>
	<div id="top-blogger-board">
	<?php if (!empty($title)): ?>
	  <h3><?php print $title; ?></h3>
	<?php endif; ?>
	<?php foreach ($rows as $id => $row): ?>
	  <div class="<?php print $classes_array[$id]; ?>">
		<div class="tape"></div>
		<div class="play-btn inline-play"></div>
		<?php print $row; ?>
	  </div>
	<?php endforeach; ?>
	</div>
	<div id="top-blogger-board-bottom"></div>
	</div>

</div>


<?php
function get_intro($path){
	if(stristr($path, "top-bloggers")) {
		return get_intro_top_bloggers();
	}
	elseif (stristr($path, "ambassadors")){
		return get_intro_ambassadors();
	}
}

function get_intro_top_bloggers(){
	$content = '<h2 class="left-h2">Meet the gungwang<br/>
	<span class="red">17 Most Influential<br/>
	Parenting Bloggers!</span></h2>
	<p>When it comes to parenting advice, there is no one-size-fits all. In our journey to bring a variety of parenting solutions to aspiring, expecting, new and devoted parents everywhere, we recognize the commitment others have made to creating community and making connections with one another.</p>
	<p>gungwang celebrates the countless cyber-superheroes who dedicate their time, voices and thoughts, helping others navigate the beautiful and complicated journey of parenting.</p>
	<p>This month, we are thrilled to honor 17 of these beloved individuals with The Parent Blogger Influencer Award.</p>';

	return $content;
}

function get_intro_ambassadors(){
	$content = '<h2 class="left-h2">Meet the gungwang<br/>
	<span class="red">2017 Ambassaddors</span></h2>
	<p>When it comes to parenting advice, there is no one-size-fits all. In our journey to bring a variety of parenting solutions to aspiring, expecting, new and devoted parents everywhere, we recognize the commitment others have made to creating community and making connections with one another.</p>
	<p>gungwang celebrates the countless cyber-superheroes who dedicate their time, voices and thoughts, helping others navigate the beautiful and complicated journey of parenting.</p>
	<p>We are honored to introduce our gungwang Ambassadors that we have chosen to feature and to spread the word about gungwang.</p>';

	return $content;
}

function get_img($path){
	if(stristr($path, "top-bloggers")) {
		$img1 = "/" . drupal_get_path('theme', 'gung_theme') . "/images/top-bloggers/top_blogger.png";
		$img2 = "/" . drupal_get_path('theme', 'gung_theme') . "/images/top-bloggers/blogger_video_new.png";
		return '<img class="img1" src="' . $img1 . '"/><br/>
		<a href="javascript: void(0);" onClick="openIntroVideoPlayer();playVideo(2305792814001);">
		<img class="img2" src="' . $img2 . '" /></a>';
	}
	elseif(stristr($path, "ambassadors")){
		$img1 = "/" . drupal_get_path('theme', 'gung_theme') . "/images/top-bloggers/award-ambassadors.png";
		return '<img class="img1" src="' . $img1 . '"/>';
	}
}
?>
