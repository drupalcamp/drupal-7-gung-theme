<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
//print "<pre>"; var_dump($row); print "</pre>";
//print "id=". $id;
// field_top_product_category field_product_image field_top_product_video title body field_price_information field_shopping_url
?>

<?php //echo "<div id='product-$id'";?>
<?php //dpm(get_defined_vars()); ?>

<div class="product-desc">
<?php print $fields['field_top_product_category']->content; ?>
<?php if($id == 8 || $id==9): ?>
<?php print $fields['title']->content; ?>
<?php endif;?>
<?php print $fields['body']->content; ?>
<?php
$price = $fields['field_price_information']->content;
$price = str_replace(';', '<br>', $price );
print $price;
?>
<?php print $fields['field_shopping_url']->content; ?>
</div>
<?php print $fields['field_product_image']->content; ?>
<?php print $fields['field_top_product_video']->content; ?>

<!-- <div class="clear-<?php echo $id;?>"></div> -->
