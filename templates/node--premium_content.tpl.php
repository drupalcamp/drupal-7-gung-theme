<?php
$url = drupal_get_path_alias(current_path());
// print $url;
if($url == "premium"){
  $header_img = base_path(). path_to_theme() . "/images/premium/classes-ban.jpg";
}
else{
  $header_img = "/sites/default/files/premium-header.jpg";
}
?>
<link rel="stylesheet" href="<?php echo base_path(). path_to_theme();?>/css/styles-premium.css" type="text/css">
<img src="<?php echo $header_img;?>" id="premium-header" />
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.js"></script> -->
<!-- <div class="premium-tv-header">
  <img src="/sites/default/files/kithtvlog.png" id="premium-responsive-img" />
</div> -->
<div class="clear"></div>
<article<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
  <header>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  </header>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($display_submitted): ?>
  <footer class="submitted"><?php print $date; ?> -- <?php print $name; ?></footer>
  <?php endif; ?>

  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
  </div>
</article>
