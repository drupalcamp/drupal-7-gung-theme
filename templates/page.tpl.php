<?php
/**
 * @file
 * Alpha's theme implementation to display a single Drupal page.
 */
//dpm($page);

/**** Gung: ShareASale  ******
global $user;
$order = commerce_cart_order_load($user->uid);
$ordernumber=$order->order_number;
$amount = $order->commerce_order_total['und']['0']['amount'];
$myid = '60172';
if($user->uid ==1){
	//print "<pre>ordder"; print_r($order); print "</pre>";
}
$element = "<img src=\"https://shareasale.com/sale.cfm?amount=$amount&tracking=$ordernumber&transtype=sale&merchantID=$myid\" width=\"1\" height=\"1\"> ";
print $element;

*/
// drupal_add_js("/jwplayer/jwplayer.js");
// drupal_add_js('jwplayer.key="BNjEbsJqJrn7XGgzaS03+1+ueB2wC7mHsq/NwVnnwWJdskJr";', 'inline');
if(isset($shareasale)) print $shareasale;
?>


<div<?php print $attributes; ?>>
  <?php if (isset($page['header'])) : ?>
    <?php print render($page['header']); ?>
  <?php endif; ?>

  <?php if (isset($page['content'])) : ?>
    <?php print render($page['content']); ?>
  <?php endif; ?>

  <?php if (isset($page['footer'])) : ?>
    <?php print render($page['footer']); ?>
  <?php endif; ?>

  <div id="block-menu-menu-kith-mobile2-menu" class="block block-menu first odd" role="navigation">

  <?php
   $menu_depth = 2;
   $menu_kth_mobile2 = menu_tree_output(menu_tree_all_data('menu-kith-mobile2-menu', null, $menu_depth));
  print(drupal_render($menu_kth_mobile2));
  ?>

 </div>

</div>
