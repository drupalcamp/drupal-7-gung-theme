<style>
html { background: url('/sites/all/themes/gung_theme/images/landing_page_brick_bg.jpg') repeat !important; font-family: "Franklin Book Gothic", sans-serif; }
body { background: none !important; }


h1 { font-family: Eraser; font-size: 60px; font-weight: normal; color: #eb1d23; margin-bottom: 40px; }
#unsup-page { position: relative; width: 95%; height: 574px; margin: 0px auto; 
  /*
  background: url('sites/all/themes/gung_theme/images/unsupported_browser_page_bg.png') no-repeat; 
  */
}
#unsup-content { 
  position: absolute; top: 10px; left: 30px; 
  width: 70%; 
  max-width: 500px;
  font-size: 1.6em; line-height: 1.5em; color: #414141; 
  box-shadow: 10px 10px 5px #888888;
  border: #888888;
  border-style: solid;
  border-width: 4px;
  border-color: #dddddd;
  padding: 28px;
}

img {
  padding: 28px;
}

</style>
<?php if (preg_match('/(?i)msie [1-7]/', $_SERVER['HTTP_USER_AGENT'])): //browser less than IE8 '/(?i)msie [1-7]/' ?>
<style>


h1 { font-size: 0px; color: transparent; width: 229px; height: 55px; background: url('/sites/all/themes/gung_theme/images/oops.png') no-repeat; }
#unsup-content { top: 210px; }
</style>
<?php endif; ?>


<a href="/"><img src="/sites/all/themes/gung_theme/images/kith_logo.png" alt="gungwang the Ultimate Parenting Resource"></a>

<div id="unsup-page">

	<div id="unsup-content">
		<?php print $content; ?>
	</div>

</div>
