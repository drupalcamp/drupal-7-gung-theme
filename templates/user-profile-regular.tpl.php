<div id="profile-accordion">

  <h3 class="my-profile"><a href="/user/me">My Profile</a></h3>
  <div class="my-profile"></div>
  <h3 class="edit-profile"><a href="/user/me/edit">Edit Profile</a></h3>
  <div class="edit-profile"></div>
  <h3 class="friends"><a href="/friends">Friends</a></h3>
  <div class="friends"></div>
  <h3 class="profile-messages"><a href="/messages">Messages</a></h3>
  <div class="profile-messages"></div>
  <h3 class="playlists"><a href="/user/me/playlists">Playlists</a></h3>
  <div class="playlists"></div>
  <h3 class="privacy"><a href="/messages/blocked">Privacy</a></h3>
  <div class="privacy"></div>
  <h3 class="orders"><a href="/orders/me">Orders</a></h3>
  <div class="orders"></div>
</div>

<div id="regular-profile-wrapper">

<?php
 //get user picture or default
$picture = theme('user_picture', array('account'=>$profile_user, 'style_name'=>'user_profile_thumbnail'));

//get username or real name
$name = theme('username', array('account' => $profile_user));
global $user;
$current_uid = $user->uid;
$profile_uid = $profile_user->uid;

if($name == ''){
  $name = $profile_user->name;
}

//get playlists

$playlists = $user_profile['kih_playlist_node_references'];
$playlist_output = '';
$playlist_count = 0;

if(count($playlists)>0){
  foreach($playlists['#items'] as $item) {
    $playlist_node = node_load($item['nid']);
    if(is_object($playlist_node)) {
      if($playlist_node->kih_public_private_option['und'][0]['value'] != 'private' &&
      $playlist_node->kih_playlist_type['und'][0]['value'] != 'save_for_later') {
        $playlist_view = node_view($playlist_node);
        $playlist_output .= render($playlist_view['kih_video_node_references']);
        $playlist_count++;
      }
    }
  }
}

$videos = kih_videos_must_watch();
$must_watch_playlists = theme('kih_videos_carousel', array('items' => $videos,'link_type' => 'must_watch', 'ajax_vid' => FALSE, 'id' => 'must-watch-videos', 'extra_option' => '', 'scroll' => 3));
$videos = kih_videos_most_popular();
$most_popular_videos = theme('kih_videos_carousel', array('items' => $videos, 'ajax_vid' => FALSE, 'link_type' => 'most_popular', 'id' => 'most-popular-videos', 'extra_option' => 'views', 'scroll' => 3));


$login_url = "/user/login?destination=" . drupal_get_path_alias(current_path());

$current_town = $user_profile['field_current_town'][0]['#markup'];
$home_town = $user_profile['field_home_town'][0]['#markup'];
$marital_status = $user_profile['field_marital_status'][0]['#markup'];
$profession = $user_profile['field_profession'][0]['#markup'];
$philosophy = $user_profile['field_philosophy'][0]['#markup'];
$current_town = $user_profile['field_current_town'][0]['#markup'];



//show Friend/Message buttons based on relationships
$relations = $user_profile['user_relationships_ui']['relations']['#markup'];
$actions = $user_profile['user_relationships_ui']['actions']['#markup'];
//print "rel=" . $relations . " actions=" . $actions;
if ($current_uid == $profile_uid) {	//self
  $friend_btn = "";
  $message_btn = "";
} else if ($current_uid == 0) {
  // $friend_btn = '<a class="submit-button"  onClick="clickLogin();" href="javascript:void(0)">Add As Friend</a>'
  $friend_btn = '<a class="submit-button"  href="'. $login_url . '">Add As Friend</a>';
  $message_btn = '<a class="submit-button-gray" onClick="clickLogin();" href="javascript:void(0)">Send Message</a>';
}
else if (preg_match("/Friend/", $relations)) {	//Friend
  $remove_link = substr($relations, strpos($relations, "<a href"), strpos($relations, "</a>") + 4 - strpos($relations, "<a href"));
  $remove_link = str_replace("Remove", "Remove Friend", $remove_link);
  $friend_btn = "<div class=\"user-button\"><a class=\"submit-button-trans\">✓&nbsp; Friends</a><div class=\"user-button-options\"><div class=\"user-button-options-arrow\"></div><ul><li>" . $remove_link . "</li></ul></div></div>";

  $message_btn = "<a class=\"submit-button-gray\" href=\"/messages/new/" . $profile_uid . "?destination=user/" . $profile_uid . "\">Send Message</a>";
}
else if (preg_match("/You have sent/", $actions)) {	//Friend request sent to this user
  // $resend_link = "<a href=\"/relationship/" . $profile_uid . "/request/1?destination=user/" . $profile_uid . "\">Resend</a>";
  // Gung: delete the "/1" from "/request/1?destination=user/"
  $resend_link = "<a href=\"/relationship/" . $profile_uid . "/request?destination=user/" . $profile_uid . "\">Resend</a>";

  $pending_link = l('View Pending Requests', 'friends', array('fragment' => 'sent'));
  $friend_btn = "<div class=\"user-button\"><a class=\"submit-button-trans\">Friend Request Sent</a><div class=\"user-button-options\"><div class=\"user-button-options-arrow\"></div><ul><!--<li>" . $resend_link . "</li>--><li>" . $pending_link . "</li></ul></div></div>";
  $message_btn = "<a class=\"submit-button-gray\" href=\"/messages/new/" . $profile_uid . "?destination=user/" . $profile_uid . "\">Send Message</a>";
}
else if (preg_match("/This user has requested/", $actions)) {	//Friend request received from this user
//$approve_link = str_replace("pending requests", "View pending requests", substr($actions, strpos($actions, "<a href"), strpos($actions, "</a>") + 4 - strpos($actions, "<a href")));
//$decline_link = str_replace("pending requests", "View pending requests", substr($actions, strpos($actions, "<a href"), strpos($actions, "</a>") + 4 - strpos($actions, "<a href")));
//$pending_link = str_replace("pending requests", "View pending requests", substr($actions, strpos($actions, "<a href"), strpos($actions, "</a>") + 4 - strpos($actions, "<a href")));
  $pending_link = l('View Pending Requests', 'friends', array('fragment' => 'received'));
  $friend_btn = "<div class=\"user-button\"><a class=\"submit-button-trans\">Respond to Friend Request</a><div class=\"user-button-options\"><div class=\"user-button-options-arrow\"></div><ul><li>" . $pending_link . "</li></ul></div></div>";
  $message_btn = "<a class=\"submit-button-gray\" href=\"/messages/new/" . $profile_uid . "?destination=user/" . $profile_uid . "\">Send Message</a>";
}
else {	//not Friend
  //$friend_btn = "<a class=\"submit-button user_relationships_popup_link\" href=\"/relationship/" . $profile_uid . "/request/1?destination=user/" . $profile_uid . "\">Add As Friend</a>";
  // Gung: delete "/1"
  $friend_btn = "<a class=\"submit-button user_relationships_popup_link\" href=\"/relationship/" . $profile_uid . "/request?destination=user/" . $profile_uid . "\">Add As Friend</a>";

  $message_btn = "<a class=\"submit-button-gray\" href=\"/messages/new/" . $profile_uid . "?destination=user/" . $profile_uid . "\">Send Message</a>";
}



 $home_town = $user_profile['field_home_town'][0]['#markup'];
 $marital_status = $user_profile['field_marital_status'][0]['#markup'];

  $interests_obj = $user_profile['field_interests']['#object']->field_interests['und'];
  $interests = "";

  if(count($interests_obj)>0){
    foreach ($interests_obj as $k=>$v) {
      if ($interests != "") $interests .= ", ";
      $interests .= $v['taxonomy_term']->name;
    }
  }

  $advice_obj = $user_profile['field_advice']['#object']->field_advice['und'];
  $advice = "";

  if(count($advice_obj)>0){
    foreach ($advice_obj as $k=>$v) {
      if ($advice != "") $advice .= ", ";
      $advice .= $v['taxonomy_term']->name;
    }
  }
  //get children info
  $children = "";
  $num_children= count($user_profile['field_children']['#object']->field_children['und']);
  for ($i=0; $i<$num_children; $i++) {
    $child = array_values(entity_load('field_collection_item', array($profile_user->field_children['und'][$i]['value'])));
    $child_name = $child[0]->field_child_name['und'][0]['value'];
    $dob = $child[0]->field_child_dob['und'][0]['value'];
    $gender = $child[0]->field_child_gender['und'][0]['value'];

    $children .= '<li>' . kih_p2p_children_image_path($gender, $dob, $child_name) . "</li>";
  }

?>

 <div style="position: relative;">
    <div class="user-paperclip"></div>
    <div id="user-profile">

      <div id="user-pic">
        <?php print $picture; ?>
        <div id="user-text" style="overflow: hidden; height: 40px; line-height: 11px;">
          <div id="user-name" ><span><?php print $name; ?></span>
            <?php if ($online_status == "Online"): ?>
              <img class="online-status" src="/sites/all/modules/custom/kih_p2p/advanced_forum_styles/gung_theme/images/forum_user_online.png" alt="Online" />
            <?php endif; ?></div>
          <div id="user-profession" style=""><span><?php print $profession; ?></span></div>
        </div>
      </div>

      <div id="user-right">
        <?php //if($profile_uid != $current_uid) { ?>
        <?php print $top_commentor?>
        <div id="user-buttons">
          <?php
          // Gung: disable the "Add as Friend" on 2/22/2016 => it is a temporary solustion.
          print $friend_btn;
          ?>
          <?php print $message_btn; ?>
        </div>

        <?php //	}?>

        <div id="user-family">
          <?php
          //scale icons if # children > 4
          $zoom = 1;
          if ($num_children > 4):
            $zoom = floor(100*393/(85*$num_children))/100;	//393 = box width, 85 = icon width
            $top = floor(130*$zoom/2);	//130 = icon height
            ?>
            style="width: 450px; overflow: hidden; zoom: <?php print $zoom ?>; -moz-transform: scale(<?php print $zoom ?>); position: relative; top: <?php ($zoom != 1 ? print '0' : print $top); ?>px"
          <?php endif; ?>



          <ul><?php print $children; ?>





          <!--<div class="teen_male">Donna 15</div>
          <div class="kid_female"></div>
          <div class="toddler_male"></div>
          <div class="baby_female"></div>-->
        </div>
      </div>


      <div id="user-content">
        <?php if ($founding_member != "") : ?>
          <div class="founding-member"><?php print $founding_member; ?></div>
        <?php endif; ?>
        <?php if ($current_town != "") : ?>
          Lives in <strong><?php print $current_town; ?></strong><br/>
        <?php endif; ?>
        <?php if ($home_town != "") : ?>
          From <strong><?php print $home_town; ?></strong><br/>
        <?php endif; ?>
        <?php if ($profession != "") : ?>
          <!--<?php print $profession; ?><br/>-->
        <?php endif; ?>
        <?php if ($interests != "") : ?>
          My Parenting Interests: <strong><?php print $interests; ?></strong><br/>
        <?php endif; ?>
        <?php if ($advice != "") : ?>
          I recommend: <strong><?php print $advice; ?></strong><br/>
        <?php endif; ?>

        <?php if ($marital_status != "") : ?>
          <div style="font-style: italic; clear: both;"> Marital Status: <?php print $marital_status; ?></div>
        <?php endif; ?>
      </div>

    </div>
  </div>

  <?php if ($philosophy != "") : ?>
    <h3>My Golden Nugget Advice</h3>
    <div id="user-profile-philosophy"><span><?php print $philosophy; ?></span></div>
  <?php endif; ?>

  <?php if ($playlist_output != "") : ?>
    <h3>My Playlists</h3>
    <?php print $playlist_output;?>
    <br><br>
  <?php endif; ?>

  <?php if($playlist_count <= 2) :?>
    <h3>gungwang's Recommended Videos</h3>
    <h2 class="block-title">Must Watch Videos</h2>
    <?php print $must_watch_playlists; ?>
    <h2 class="block-title">Most Popular</h2>
    <?php print $most_popular_videos ;?>
    <br><br>
  <?php endif;  ?>



</div>
