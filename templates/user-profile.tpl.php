<?php

//print_r($user_profile);
//dpm($user_profile);
global $user;
$current_uid = $user->uid;
$profile_uid = arg(1);
if ($profile_uid == "me") $profile_uid = $current_uid;
$profile_user = user_load($profile_uid);

$is_professional = false;
if (in_array('professional', array_values($profile_user->roles))) {
  $is_professional = true;
}

if($is_professional){
  print theme('user_profile_professional_display', array(
    "user_profile" => $user_profile,
    "profile_user" => $profile_user,
  ));

}
else{
  print theme('user_profile_regular_display', array(
    "user_profile" => $user_profile,
    "profile_user" => $profile_user,
  ));

}


?>


<script>
  jQuery(window).bind("load", function() {	//make sure special fonts have loaded, so height calculation is correct
    <?php if (!$is_professional): ?>
    //set philosophy div height so bg lines are evenly spaced
    //height rounds up to nearest 55 + 26.5n
    var ht = jQuery("div#user-profile-philosophy").height();
    jQuery("div#user-profile-philosophy").height(55 + 26.5*Math.ceil((ht - 55)/26.5));
    <?php else: ?>
    //set philosophy-pro div height so bg lines are evenly spaced
    //height rounds up to nearest 55 + 15n
    var ht = jQuery("div#user-profile-pro-philosophy").height();
    jQuery("div#user-profile-pro-philosophy").height(55 + 15*Math.ceil((ht - 55)/15));
    <?php endif; ?>

  });
</script>

