
<div class="left" id="form_fields">
	<?php
	print render($form['account']['name']);
	print render($form['account']['mail']);
	print render($form['account']['pass']);
	print render($form['field_secret_code']);
	print '<div style="font-size: 0.85em; margin-top: -13px;">' . ctools_modal_text_button('Request Access Code', 'feedback/nojs', t('Talk To Us'), 'ctools-modal-ctools-ajax-register-style') . '</div>';

	//print render($form['field_first_name']);
	//print render($form['field_last_name']);
	//print render($form['field_zip_code']);
	
	
	?>
	<p style="margin-top: 20px;" class="red">Help us get to know you:</p>
	
	<?php
	print render($form['field_is_parent']);
	print render($form['field_is_expecting']);
	print render($form['field_is_trying']);
	print render($form['field_is_adopting']);
	?>
</div>



<div style="clear: both;" id="children-info">
<p style="margin-top: 10px;" class="red">Tell us about your children: &nbsp;&nbsp;(optional &mdash; this will help us provide you with specific content)</p>
<?php
print render($form['field_children']);
?>
</div>

<?php
print render($form['legal']);

print render($form['actions']);
print render($form['form_build_id']);
print render($form['form_id']);

//print_r($form);
?>

<div class="pencil"></div>
