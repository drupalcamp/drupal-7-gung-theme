<?php
// gung view template for top_products View
// May 31, 2017
//
/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */

 drupal_add_js(drupal_get_path('theme', 'gung_theme') . '/js/jquery.masonry.min.js');
 drupal_add_js(drupal_get_path('theme', 'gung_theme') . '/js/jquery.autopager.min.js');
 drupal_add_js(drupal_get_path('theme', 'gung_theme')  . '/js/livetv-episodes.js');

drupal_add_css(base_path(). 'sites/all/themes/gung_theme/css/style-topproducts.css', array('type' => 'external'));
$url = drupal_get_path_alias(current_path());
$image_path = base_path(). 'sites/all/themes/gung_theme/images/top_products/';
//$output = $view;
// print "<pre>"; print_r($variables); print "</pre>";

/*for ($i=0; $i<count($view_result); $i++){
 $node = $result[0];
 print "<pre>"; print_r($node->nid); print "</pre>";
}
*/
?>

<div itemscope="" class="easy-breadcrumb" itemtype="http://data-vocabulary.org/Breadcrumb">
<a href="/" class="easy-breadcrumb_segment easy-breadcrumb_segment-front" itemprop="url">
<span itemprop="title">Home</span>
</a>
<span class="easy-breadcrumb_segment-separator"> » </span>
<a href="/top-products" class="easy-breadcrumb_segment easy-breadcrumb_segment-1" itemprop="url">
<span itemprop="title">Top Products</span>
</a>
</div>

<div class="products-bar">
  <!-- <h1 class="title">Top Products</h1> -->
  <div class="products-text ">
    <h1 class="red-eraser">Leana's Favorite New Products</h1>
  </div>
  <div class="leana-photo"><img src="<?php echo $image_path?>leana.png" alt="Leana"></div>
</div>

<div id="videos-livetv-episodes">
  <?php if ( empty($title) ): ?>
    <?php $title = $view->get_title(); ?>
  <?php endif; ?>
  <?php if ($title): ?>
    <h2 class="views-title"><?php print t($title)?></h2>
  <?php endif; ?>

<div class="<?php print $classes; ?>">

  <?php if ($rows): ?>
    <div class="view-content">
       <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php //print $pager; ?>
  <?php endif; ?>


  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

</div>

</div>

<div id="videos-livetv-episodes-next" class="view view-top-products view-id-top_product"></div>
<?php
//$load_items = 10;
$page = isset($_GET['page']) ? $_GET['page'] : 0  ;
//$from = $page * $load_items;
$path = current_path();
$next_url = (stristr($path, 'kids-and-family')) ? "kids-and-family" : "baby-products" ;

$pager_output = l(
    'Next', 'top-products/'. $next_url ,
    array('query' => array('page' => $page + 1),
    'attributes' => array('id' => 'next') ));
// the code is working
$pager_output .= '<div class="autopager_loaders" id="solrSearchListsLoader"></div>';
echo $pager_output;
?>
