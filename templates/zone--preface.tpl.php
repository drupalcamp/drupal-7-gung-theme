<?php if(!$is_front) print $mobile_searchform_other; ?>
<div id="talk-to-us"><?php print ctools_modal_text_button('<img src="' . pp() . 'talk_to_us.png" />', 'feedback/nojs/8091', t('Talk To Us'), 'ctools-modal-ctools-ajax-register-style'); ?></div>
<div id="take-the-tour">
<?php
  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();

 $imgtour =  '<img src="/'.drupal_get_path('theme', 'gung_theme') . '/images/KITH-tabs-tour.png" alt="Gung Wang Tour">';
 print ctools_modal_text_button($imgtour, 'content-modal/48584', t('About Gung Wang'),'about-intro-link ctools-modal-ctools-kith-wide-style');


  ?>
 

</div>

<div id="step_2_button" style="display: none"><?php print ctools_modal_text_button(t('step2'), 'kith-register/nojs/form/step2', t('step2'), 'ctools-modal-ctools-kith-wide-style'); ?></div><!--hidden text link for facebook connect-->
<div id="validation-reminder" style="display:none;"><?php print ctools_modal_text_button('validation reminder', 'validation-reminder/nojs', t('validation reminder'), 'ctools-modal-ctools-ajax-register-style'); ?></div>
<div style="display: none;z-index: 1000; position: absolute; left: 0px; margin: 0px; background: none repeat scroll 0% 0% rgb(0, 0, 0); opacity: 0.7; top: 0px; height: 100%; width: 100%;" id="modalBackdropManual"></div>

<?php if ($wrapper): ?><div<?php print $attributes; ?>><?php endif; ?>  
  <div<?php print $content_attributes; ?>>    
    <?php if ($breadcrumb): ?>
      <div id="breadcrumb" class="grid-<?php print $columns; ?>">
         <div style="float: left; margin-right: 10px"> <?php print $breadcrumb; ?></div>
         <div style="float: right"><?php print kih_global_short_add_this_button(); ?></div>
      </div>
    <?php endif; ?>    
    <?php if ($messages): ?>
      <div id="messages" class="grid-<?php print $columns; ?>"><?php print $messages; ?></div>
    <?php endif; ?>
    <?php print $content; ?>
  </div>
<?php if ($wrapper): ?></div><?php endif; ?>

<?php


/*
function __get_mobile_searchform_other(){
  $str = '
<div id="block-search-form--2-mobile" class="block block-search">
   <form action="/" method="post" id="search-block-form--2-mobile" accept-charset="UTF-8">
      <div>
         <div class="form-item form-type-textfield form-item-search-block-form">
            <label class="element-invisible" for="edit-search-block-form--4">Search </label>
            <input title="Enter the terms you wish to search for." class="custom-solr-autocomplete unprocessed clear-defaults default form-text form-autocomplete" type="text" id="edit-search-block-form--4-mobile" name="search_block_form" value="Search &amp; learn from 9,000 videos from top experts &amp; parents" size="15" maxlength="128" autocomplete="off">
         </div>
         <div class="form-actions form-wrapper" id="edit-actions--2"><input type="submit" id="edit-submit--2" name="op" value="Search" class="form-submit"></div>
         <input type="hidden" name="form_build_id" value="form-50S-pzHpgew7Cx-zV3oXT-VWqiG1S47vkDGZ-mlaG2o">
         <input type="hidden" name="form_id" value="search_block_form">
      </div>
   </form>
</div>
';

return $str;

}

*/

?>