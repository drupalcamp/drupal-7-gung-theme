

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <?php if ($css): ?>
    <style type="text/css">
      <!--
      <?php print $css ?>
      -->
      table, caption, tbody, tfoot, thead, tr, th, td {
        margin: 0;
        padding: 0;
        border: 0;
        outline: 0;
        font-size: 100%;
      }

      a{
        margin: 0px;
        padding: 0px;
        border:0px;
        text-decoration: none;
        color: red;
      }

      img{
        margin: 0px;
        padding: 0px;
        border:0px;
      }


      h1{
        font-family: 'Eraser';
        src:       url('http://gungwang.com/sites/all/themes/gung_theme/css/fonts/eraser.eot?') format('eot');
        src:  url('http://gungwang.com/sites/all/themes/gung_theme/css/fonts/eraser.eot?#iefix') format('embedded-opentype'),
        url('http://gungwang.com/sites/all/themes/gung_theme/css/fonts/eraser.woff') format('woff'),
        url('http://gungwang.com/sites/all/themes/gung_theme/css/fonts/eraser.ttf') format('truetype'),
        url('http://gungwang.com/sites/all/themes/gung_theme/css/fonts/eraser.otf') format('opentype'),
        url('http://gungwang.com/sites/all/themes/gung_theme/css/fonts/eraser.svg') format('svg');
        color: red;
        font-size: 18px;
        font-weight: normal;

      }


      h2{

      }


      .eraser{
        font-family: 'Eraser';
        src:       url('http://gungwang.com/sites/all/themes/gung_theme/css/fonts/eraser.eot?') format('eot');
        src:  url('http://gungwang.com/sites/all/themes/gung_theme/css/fonts/eraser.eot?#iefix') format('embedded-opentype'),
        url('http://gungwang.com/sites/all/themes/gung_theme/css/fonts/eraser.woff') format('woff'),
        url('http://gungwang.com/sites/all/themes/gung_theme/css/fonts/eraser.ttf') format('truetype'),
        url('http://gungwang.com/sites/all/themes/gung_theme/css/fonts/eraser.otf') format('opentype'),
        url('http://gungwang.com/sites/all/themes/gung_theme/css/fonts/eraser.svg') format('svg');

      }

      .logo-bottom{
        width: 100%;
      }

      body{
        font-family: 'Open Sans', Helvetica, sans-serif;
        font-size: 13px;
        color: #565656;
        border: 0px;
      }

      .test{
        font-family: Eraser;
      }

      p{
        border: 0px;
        line-height: 1.5;
      }



    </style>
  <?php endif; ?>
</head>


<body id="mimemail-body" <?php if ($module && $key): print 'class="'. $module .'-'. $key .'"'; endif; ?>  style="margin: 0px; padding: 0px;" background="http://gungwang.com/sites/all/themes/gung_theme/images/body_bg.jpg">
<div style="background-color:#cccccc;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="http://gungwang.com/sites/all/themes/gung_theme/images/body_bg.jpg" color="#cccccc"/>
  </v:background>
  <![endif]-->
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td valign="top" align="left" background="http://gungwang.com/sites/all/themes/gung_theme/images/body_bg.jpg">





        <!-- begin main newsletter-->
        <div id="main-newsletter">

          <table >
            <tr></td>

              <table  border="0px;" style="width: 690px; margin: 0px; padding: 0px; margin-left: auto; margin-right: auto; background-color: #ffffff; margin-top: 0px;" cellspacing="0px" cellpadding="0px" border="1px;">
                <tbody>

                <tr>
                  <td colspan="2" cellpadding="0px;" cellspacing="0px;">
                    <img width="710" height="1" alt="" src="http://gungwang.com/sites/default/files/spacer2.jpg" typeof="foaf:Image">
                  </td>
                </tr>


                <tr >
                  <td width="465px" style="text-align: left;" >
                    <a href="http://gungwang.com" style="text-decoration: none;"><img src="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/KITH-email-logo-with-space.jpg" border="0px;" /></a>
                  </td>
                  <td width="215px;" style="text-align: center; valign: center;">

  <span>
  <a href="https://www.facebook.com/gungwang" style="border: 0px; text-decoration: none;" border="0px"><img src="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/footer_facebook.png" alt="Facebook" border="0px" style="border: 0px;"/></a>
  <a href="https://twitter.com/gungwang"><img src="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/footer_twitter.png" alt="Twitter" border="0px"/></a>
  <a href="https://www.pinterest.com/gungwang/"><img src="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/footer_pinterest.png" alt="Pinterest" border="0px" /></a>
  <a href="https://plus.google.com/+gungwang"><img src="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/footer_google.png" alt ="Google Plus" border="0px"/></a>
  <a href="https://www.youtube.com/gungwangtv"><img src="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/footer_youtube.png" alt="YouTube" border="0px"/></a>
  </span>
                  </td>
                </tr>

                <tr cellspacing="0" cellpadding="0" style="margin: 0px; padding: 0px;"><td colspan="2" cellspacing="0" cellpadding="0" style="margin: 0px; padding: 0px; height: 36px; width: 690px; vertical-align: bottom; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid;">


                    <a href="http://gungwang.com"><img src="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/video-tab.jpg" alt="Video Tab" style="margin: 0px; padding: 0px; vertical-align: bottom;" border="0px;" /></a>
                    <a href="http://gungwang.com/premium"><img src="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/premiumico.jpg" style="margin: 0px; padding: 0px; vertical-align: bottom;" border="0px;" alt="Playlists Tab" /></a>
                    <a href="http://gungwang.com/experts"><img src="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/experts-tab.jpg" alt="Experts Tab" style="margin: 0px; padding: 0px; vertical-align: bottom;" border="0px;" /></a>
                    <a href="http://gungwang.com/forum"><img src="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/parent-parent-tab.jpg" alt="Parent to Parent" style="margin: 0px; padding: 0px; vertical-align: bottom;" border="0px;" /></a>
                    <a href="http://gungwang.com/articles"><img src="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/blog-tab.jpg"  alt="Blog tab" style="margin: 0px; padding: 0px; vertical-align: bottom;" border="0px;"/></a>



                  </td></tr>



                </tbody>
              </table>

              <table  style="width: 690px; margin: 0px; padding: 5px; margin-left: auto; margin-right: auto; background-color: #ffffff; cellpadding: 5px" cellspacing="0px" cellpadding="5px" width="690px;">
                <tbody width="690px;" style="width: 690px;">

                <tr>
                  <td  width="690px" style="padding: 10px; cellpadding: 10px width: 690px;  font-family: 'Open Sans', Helvetica, sans-serif;" valign="top" cellpadding="10px"   font-family="'Open Sans', Helvetica, sans-serif;" cellpadding="10px;">

                    <!-- <span style="border-width: 1px; border-color: #cccccc; border-style: solid; display: block; padding: 10px; border-bottom: 0px;"> -->

                    <img width="690" height="1" alt="spacer" style="width: 690px; height: 1px;" src="http://gungwang.com/sites/default/files/spacer.jpg" typeof="foaf:Image">




                    <p><?php
                      //   watchdog('commerce-order-invoice','<pre>'.print_r($info,TRUE).'</pre>');

                      $order = commerce_order_load($info['order_number']);
                      $line_items = $order->commerce_line_items['und'];

                      foreach($line_items as $line_item) {

                        $line_item = commerce_line_item_load($line_item['line_item_id']);
                        $product_id = $line_item->commerce_product['und'][0]['product_id'];

                        $product = commerce_product_load($product_id);
                        $title = $product->title;

                        if (isset($product->field_premium_video_ref['und']['0']['target_id'])) {
                          $nid = $product->field_premium_video_ref['und']['0']['target_id'];
                          $nid = 'node/' . $nid;
                          $options = array('absolute' => TRUE);
                          $nicepath = url($nid, $options);

                          print <<<EOD
                          <img src="http://gungwang.com/sites/all/themes/gung_theme/images/premium/baby-for-email.jpg" align="right"/>
<h1 style="color: #e81118; font-weight: bold; font-size: 26px;">Thank you for purchasing $title!</h1>

<p>You can access your video by visiting <a href="$nicepath">$nicepath</a>.</p>

<p>You can also view the video at any time by clicking on the "Orders" tab from your Gung Wang profile page.</p>
EOD;
                        }
                        else{

                          print <<<EOD
                          Thank you for becoming a Premium Member of gungwang.com! You are on your way to becoming a Super Parent! You now have access to the PREMIUM tab which contains exclusive content.
EOD;


                        }

                      }
                      ?>
                      <br /><em>Note: You must be logged in to view premium content. </em>
                    </p>

                    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#CCC">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#FFF" style="font-family: verdana, arial, helvetica; font-size: 10px;">
                            <tr>
                              <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: verdana, arial, helvetica; font-size: 11px;">
                                  <tr>
                                    <td nowrap="nowrap" style="line-height: 1.6em;" valign="middle">
                                      <!-- Invoice Header -->

                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: verdana, arial, helvetica; font-size: 11px;">

                                  <tr>
                                    <td colspan="2">

                                      <table class="details" width="100%" cellspacing="0" cellpadding="0" style="font-family: verdana, arial, helvetica; font-size: 1em;">
                                        <tr>
                                          <td valign="top" width="50%">

                                            <p style="font-size: 18px;"><strong><?php print t('Web Order No:'); ?></b> <?php print $info['order_number']; ?></strong></p><br/>
                                            <br/>
                                            <?php print t('Order Date:'); ?></b> <?php print date('F j, Y', $info['order_created']); ?><br/>

<br />

                                                  <?php
                                                  if($product_id == '2'){
                                                    print '<b>';
                                                    print t('Renewal Date: ');
                                                    print '</b>';
                                                    print date('F j, Y',strtotime(date("F j, Y", $info['order_created']) . " + 365 day"));
                                                    print '</br>';
                                                  }

                                                  ?>

                                            <?php print t('Email Address:'); ?></b> <?php print $info['order_mail']; ?><br/>
                                            Price: <?php print  	commerce_currency_format($product->commerce_price['und']['0']['amount'], $product->commerce_price['und']['0']['currency_code'],$product); ?><br />
                                            Title: <?php print $title ?>

                                          </td>
                                          <td valign="top" width="50%">


                                          </td>
                                        </tr>
                                      </table>

                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td>

                              </td>
                            </tr>
                            <tr>
                              <td>
                                <table>
                                  <tr>
                                    <td colspan="2" style="background: #EEE; color: #666; padding: 1em; font-size: 0.9em; line-height: 1.6em; border-top: #CCC 1px dotted; text-align: center;">
                                      <!-- Invoice Header -->

                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>



                    <img width="690" height="1" alt="spacer" style="width: 690px; height: 1px;" src="http://gungwang.com/sites/default/files/spacer.jpg" typeof="foaf:Image">
                    <!-- </span> -->


                  </td>


                </tr>

                <tr>
                  <td colspan="2" style="background-image:url('http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/footer_bg.jpg'); width: 690px; height: 145px; background-repeat: no-repeat; text-align: center;" valign="bottom" background="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/footer_bg.jpg">
                    <!--[if gte mso 9]>
                    <v:rect style="width:690px;height:145px;" strokecolor="none">
                      <v:fill type="tile" color="#FFFFFF" src="http://gungwang.com/sites/all/themes/gung_theme/images/newsletter/footer_bg.jpg" /></v:fill>
                    </v:rect>
                    <v:shape id="theText" style="position:absolute;width:600px;height:120px;">
                    <![endif]-->
                    <table>



                    </table>

                    <!--[if gte mso 9]>
                    </v:shape>
                    <![endif]-->

                  </td>
                </tr>

                </tbody>
              </table>
              <div style="text-align: center;">
                <br>
                (C) 2015 gungwang.com All rights reserved | <a href="http://gungwang.com/privacy-policy">Privacy policy</a> | <a href="http://gungwang.com/support">Support</a> | <a href="http://gungwang.com/terms-use">Terms</a>
              </div>
              </td></td></table>




        </div>
        <!-- end main newsletter-->

      </td>
    </tr>
  </table>
</div>


</body>
</html>






  </body>
</html>
