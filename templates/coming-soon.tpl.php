<style>
BODY { background: url('/sites/all/themes/gung_theme/images/landing_page_brick_bg.jpg') repeat !important; font-family: "Franklin Book Gothic", sans-serif; }
h1.node-title { display: none; }
#webform-component-email { float: left; margin: 0px 15px 0px 0px; }
#webform-component-email input { height: 30px; }
#webform p { margin: 5px 0px; }
.form-actions { float: left; margin: -1px 0px 0px; }
li.forward_link { display: none !important; }
div.status { border: none; background: none; color: #090; padding: 0px; font-size: 12px; }
div.status p { padding-top: 3px; clear: both; }

#coming-soon-play {position: absolute; text-indent: -99999px; top: 91px; left: 606px; height: 310px; width: 421px; opacity: .7; background: url('sites/all/themes/gung_theme/images/play-button.png') no-repeat scroll 50% 50% transparent}
#coming-soon-play:hover{opacity: 1}
#video-player{position: absolute; z-index: 1001; background-color: #fff; padding: 5px; top: 100px; left: 250px;}
</style>
<script>
jQuery(function() {
	jQuery("#edit-submitted-email").focus(function() {
		if (jQuery(this).val() == "Enter your email") jQuery(this).val("");
	}).blur(function() {
		if (jQuery(this).val().trim() == "") jQuery(this).val("Enter your email");
	});

	var introVideo = 2046357428001;

	//Homepage Intro video load
	//For the First Video 
    jQuery('#coming-soon-play').live('click', function(e) {
    	 e.preventDefault();
    	 
    	 jQuery('#video-player').show();
    	 jQuery('#videos_player_close').show();
    	 jQuery('#modalBackdrop').show();
    	 
    	 if(modVP) {
	       modVP.loadVideoByID(introVideo);
	     } else {
	        //Its being initialized so save in global variable
	        startingVideo = introVideo;
	     }
    	 
    	 jQuery('#block-views-slider-block').hide();
    	 
    	 jQuery('#videos_player_close').click(function() {
    		 jQuery('#video-player').hide();
    		 jQuery('#modalBackdrop').hide();
    		 if(modVP) {
    			 modVP.pause();
    		 }
    	 });
    	 
    });
    
});


</script>

<div style="display: none;z-index: 1000; position: absolute; left: 0px; margin: 0px; background: none repeat scroll 0% 0% rgb(0, 0, 0); opacity: 0.7; top: 0px; height: 100%; width: 100%;" id="modalBackdrop"></div>

<div style="position: relative; width: 1110px; height: 812px; margin: 0px auto; background: url('sites/all/themes/gung_theme/images/landing_page_bg.png') no-repeat;">

<a id="coming-soon-play" href="#">test</a>
<div id="video-player" style="display: none; positio"><?php print kih_videos_player()?></div>


	<div id="webform" style="position: absolute; top: 285px; left: 120px">
	<?php
	//display "Request Your Invite" Webform (display as a block so success message shows up within block)
	$query = new EntityFieldQuery();

	$entities = $query->entityCondition('entity_type', 'node')
	  ->propertyCondition('type', 'webform')
	  ->propertyCondition('title', 'request your invite')
	  ->propertyCondition('status', 1)
	  ->range(0,1)
	  ->execute();

	  if (!empty($entities['node'])) {
	    $node = node_load(array_shift(array_keys($entities['node'])));

		print '<p>' . $node->body['und'][0]['value'] . '</p>';
		$block = module_invoke('webform', 'block_view', 'client-block-' . $node->nid);
		print render($block['content']);
		if (!empty($messages)) print $messages;
	  }
	 ?>
	 </div>

	<div style="position: absolute; top: 394px; left: 87px"><?php print $content; ?></div>

	<div style="position: absolute; top: 785px; left: 80px; font-size: 12px">&copy;<?php print date("Y"); ?> gungwang.com. All rights reserved.</div>

</div>