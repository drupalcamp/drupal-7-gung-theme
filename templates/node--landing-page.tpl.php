<?php

  $playlistnid = $node->field_playlist_reference['und'][0]['nid'];
  $playlistnode = node_load($playlistnid);
  $playlistalias = drupal_get_path_alias('node/'.$playlistnode->nid);

?>
<article<?php print $attributes; ?>>

  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>

  <header>
    <h2<?php print $title_attributes; ?>><a href="<?php print '/'.$playlistalias ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  </header>

  <?php print render($title_suffix); ?>
  <div<?php print $content_attributes; ?> id="fullcontainer">
  
  
  
  
  <?php 

  print '<div id="landing-main-page-content">';
     
 
  
  print '<div id="mainplaylist">';
  
  if($node->field_display_featured['und']['0']['value'] == 'Image'){
  
  print '<a href="/'.$playlistalias.'">';
  //print '<div class="play tooltip maintip"></div>';
  print render($content['field_ld_featured_image']);
  print '</a>';
 
  
  }
  else{
  print'<div id="landing-featured-vid">';
  print render($content['field_ld_featured_video']);
  print '</div>';
  
  }
  
   print render($content['body']);

   print '</div>';
   
   //dpm($playlistnode);
   $node_references = $playlistnode->kih_video_node_references['und'];
   print '<div id="mainplaylist">';
 //  print '<ul id="mainplaylist">';
 
 print '<div id="related_videos">';
 print '<table><tbody>';
 
   $c = 0;
 
   foreach($node_references as $item => $value){
     $zebra = ($c++%2==1) ? 'odd' : 'even';
	 print '<tr class="'.$zebra.'">';
     print '<td>';	
	 $tempnid = $value['nid'];
	  $videonode = node_load($tempnid);
	// dpm($videonode);
	$pathalias = drupal_get_path_alias('node/'.$videonode->nid);
	//dpm($pathalias);
	
	 print '<a href="/'.$playlistalias.'#page=/'.$pathalias.'"><div class="play tooltip"></div>';
	 print $videonode->title;
	 print '</a>';
	 print '<td>';
	 
	 $expertnode = node_load($videonode->field_video_expert['und']['0']['nid']);
	 
	 print $expertnode->title;
	 //print 'Name of Expert';
	 
	 print '</td>';
	 print '</td>';
	 print '</tr>';
   }
 // print '</ul>';
  print '</tbody></table></div>';
  print ' </div>';
  ?>   
  
  </div>
  
  
   <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
	  hide($content['field_ld_featured_image']);
	  hide($content['field_ld_featured_video']);
    //  print render($content);
    ?>
  </div>
  

  
  <div class="clearfix">
   

    <?php print render($content['comments']); ?>

  </div>
  </div>
</article>