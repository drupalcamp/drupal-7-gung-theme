<?php
$include_css = drupal_get_path("theme", "gung_theme") . "/css/style-tier1-cat1.css";
drupal_add_css($include_css);
//print "<pre>page--taxonomy.tpl.php "; print_r(arg(2)); print "</pre>";
//print "<pre>"; print_r($page['content']); print "</pre>";
$term_id = arg(2);
$cat1_term = taxonomy_term_load($term_id);
$hot_a = $cat1_term->field_hot_link_a['und'][0];
$url_a = l($hot_a['title'], $hot_a['url']);
//print "<pre>cat1_term "; print_r($hot_a); print "</pre>";
$hot_b = $cat1_term->field_hot_link_b['und'][0];

if(stristr($hot_b['title'], "Ask an Expert")){
  $url_b = l($hot_b['title'], $hot_b['url'],
  array('attributes' => array('class' => 'ctools-use-modal ctools-modal-ctools-ajax-register-style')));
}else{
  $url_b = l($hot_b['title'], $hot_b['url']);
}

$hot_c = $cat1_term->field_hot_link_c['und'][0];
$url_c = l($hot_c['title'], $hot_c['url']);

?>

<?php
/**
 * @file
 * Alpha's theme implementation to display a single Drupal page.
 */
?>
<div<?php print $attributes; ?>>
  <?php if (isset($page['header'])) : ?>
    <?php print render($page['header']); ?>
  <?php endif; ?>


  <?php if (isset($page['content'])) : ?>
  <?php
    $preface_first = $page['content']['preface']['preface_first'];
    $preface_second =  $page['content']['preface']['preface_second'];
    //$main = $preface_first['system_main']['main'];
  ?>


    <div id="zone-preface-wrapper" class="zone-wrapper zone-preface-wrapper clearfix">
      <div id="zone-preface" class="zone zone-preface clearfix container-12">

        <div class="grid-12 region region-preface-first" id="region-preface-first">
          <div class="region-inner region-preface-first-inner">
            <div id="block-system-main" class="block block-system cat1-term-content">
              <div class="block-inner clearfix">
                <div class="content clearfix">
                 <?php print render($preface_first['system_main']); ?>
                </div>
              </div>
            </div>
            <div id="block-kih-categories-kih-categories-firstcats" class="block block-kih-categories contextual-links-region" >
              <?php
              //$page['content']['content']['system_main']['main']['#markup'] = $output;
              //   dpm($page['content']);
              //print render($page['content']);
              print render($preface_first['kih_categories_kih_categories_firstcats']);
              ?>
            </div>
          </div>
        </div>

        <div class="grid-4 region region-preface-second" id="region-preface-second">
          <div class="region-inner region-preface-second-inner">
            <?php //print render($preface_second) ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>



<div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">
  <div class="zone zone-content clearfix container-12">
    <div class="grid-12 region region-content">
      <div class="region-inner region-content-inner">

        <div class="ca1-links">
          <ul class="ca1-links-ul">
            <li class="cat1-link-li"><?php print $url_a ;?></li>
            <li class="cat1-link-li"><?php print $url_b ;?></li>
            <li class="cat1-link-li"><?php print $url_c ;?></li>
          </ul>
        </div>

        <div class="nav-menu">
          <div class="container-gung">
            <h3>CATEGORIES</h3>
            <?php
            //print_r($page['content']['preface']['preface_first']['kih_categories_kih_categories_firstcats']);
            $include_path = drupal_get_path("theme", "gung_theme") . "/templates";
            include ("$include_path/nav-cat1-landing-$term_id.tpl.php");
            //$first_year = taxonomy_get_children($term_id);
            //print "<pre>"; print_r($first_year); print "</pre>";;
            ?>

          </div> <!--END container-gung -->
        </div>


      </div>


      <?php if(isset($tabs)):?>
        <?php print render($tabs); ?>
      <?php endif;?>

    </div>
  </div>
</div>

<?php
//print "<pre>"; print_r ($variables['tabs'] ); print "</pre>";
 ?>

  <?php if (isset($page['footer'])) : ?>
    <?php print render($page['footer']); ?>
  <?php endif; ?>

  <div id="block-menu-menu-kith-mobile2-menu" class="block block-menu first odd" role="navigation">

    <?php
    $menu_depth = 2;
    $menu_mobile2 = menu_tree_output(menu_tree_all_data('menu-kith-mobile2-menu', null, $menu_depth));
    print(drupal_render($menu_mobile2));
    ?>

  </div>

</div>
