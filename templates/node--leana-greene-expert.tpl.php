<?php


//dpm($node);
//print 'does this print';

?>


<article<?php print $attributes; ?>>


<?php print render($content['field_large_portfolio']); ?>

<div id="parenting-trend-top-box">
<h2><?php print $title; ?></h2>
<span class="expert-tagline"><?php print render($content['field_expert_tagline']); ?></span>
<span class="open-contact"><?php print render($content['field_open_contact']); ?></span>
<span class="expert-bio"><?php print render($content['body']); ?></span>
</div>

  <?php print render($title_prefix); ?>
  
 
  <?php print render($title_suffix); ?>

  



 
  <div<?php print $content_attributes; ?>>
  
  

    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
	  hide($content['field_large_portfolio']);
	  hide($content['field_expert_tagline']);
	  hide($content['field_open_contact']);
	  hide($content['body']);
	  hide($content['field_ld_featured_video']);
	  hide($content['field_playlist_reference']);
	  hide($content['field_expert_slideshow']);
      print render($content);
    ?>
  
  
    <div class="parent-trend-video">
  <?php print render($content['field_ld_featured_video']);?>
  <?php print render($content['field_playlist_reference']);?>
  </div>
  <?php print render($content['field_expert_slideshow']); ?>
  </div>
  
  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
  </div>
</article>