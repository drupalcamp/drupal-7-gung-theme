<?php
/**
 * @file
 * Alpha's theme implementation to display a single Drupal page.
 */
 header ("Location: /breastfeeding/video");
//print "<pre>"; print_r($page); print "</pre>";
?>

<div<?php print $attributes; ?>>

  <?php if (isset($page['content'])) : ?>

    <div class="bgate-header">
    <div class="bcarelogo">
              <img src="/sites/all/themes/gung_theme/images/babycare/baby-care-logo.jpg" alt="baby care logo">
          </div>

          <div class="logo-soup">
              <img src="/sites/all/themes/gung_theme/images/babycare/ass-seen-on-full.jpg" alt="as seen on">
          </div>

    </div>
    <?php print render($page['content']); ?>
  <?php endif; ?>

  <div id="block-menu-menu-kith-mobile2-menu" class="block block-menu first odd" role="navigation">

    <?php
    $menu_depth = 2;
    print(drupal_render(menu_tree_output(menu_tree_all_data('menu-kith-mobile2-menu', null, $menu_depth))));
    ?>

  </div>

</div>
