<?php
    drupal_add_js(path_to_theme() . '/js/device.min.js', 'file');
    drupal_add_css(path_to_theme() . '/css/sytle-responsive-gung.css');
    drupal_add_css(path_to_theme() . '/css/jquery.fancybox.css');
    drupal_add_css(path_to_theme() . '/css/vip-booking.css');
    drupal_add_css(path_to_theme() . '/css/vip-grid.css');
    drupal_add_css(path_to_theme() . '/css/vip-style.css');
    kih_couponregister_utm_save_cookie();
?>


<div<?php print $attributes; ?>>
  <?php if (isset($page['header'])) : ?>
    <?php print render($page['header']); ?>
  <?php endif; ?>

  <?php if (isset($page['content'])) : ?>
  <style>.portrait { display:inline; }</style>
    <link rel="stylesheet" media="all" href="/sites/all/themes/gung_theme/css/vip-style-mobile.css?a0" />

  <!---------------------------------------------------------------------------------------------->
  <div class="vip-clear-bg"></div>
  <div class="vip-red-bar-bg">
    <div class="vip-signup-into">
      <h1 class="h1-black-eraser">VIP Signup for Premium</h1>
      <h6 class="black-eraser pc-display" > Congratulations, you have received an offer for Premium membership. <br>Please fill out the form below and check your email for confirmation.</h6>
      <h6 class="black-eraser mobile-display" > Congratulations, you have received an offer for Premium membership. Please fill out the form below and check your email for confirmation.</h6>
    </div>
  </div>
  <div style="background-position: 50% -36px;" class="parallax parallax01" data-parallax-speed="-0.4">
<section class="container">

<div class="row">
  <!-- <h1 class="red-eraser">Welcome to Gung Wang!</h1> -->

  <!-- <h1 class="black-eraser">VIP Signup for Premium</h1>
  <h6 class="black-eraser pc-display" > Congratulations, you have received an offer for <br>Premium membership. Please fill out the form <br>below and check your email for confirmation.</h6>
  <h6 class="black-eraser mobile-display" > Congratulations, you have received an offer for Premium membership. Please fill out the form below and check your email for confirmation.</h6> -->

    <div class="grid_6">
      <div class="promo-img" style="align-content:center; text-align:center;">
<iframe class="vip-iframe-youtube"  src="https://www.youtube.com/embed/7Ttfzvy2_GQ?rel=0" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>

    <div class="grid_5">
      <div class="booking wow fadeInRight">
        <!--
          <div style="align-content:center; text-align:center;"><img src="<?php echo path_to_theme()?>/images/vip/pin.png" border="0"></div>
        -->
      <h2 class="red-eraser">Sign up</h2>
      <!--<h4>To Receive <span class='red'>FREE</span> Membership & Access To Premium Content.</h4>-->
          <?php print render($page['content']); ?>


      </div>
    </div>


  </div>

</section>

</div><!--END div parallax parallax01 -->

<!--========================================================
						  CONTENT
=========================================================-->
<main>
  <section class="well" id="about">
		<div class="container">
			<!--<h2>Title?</h2>-->
			<div class="row">
        <!-- <div class="grid_12">
          <h2 style="align-content:center; text-align:center;">Become a Better Parent with Premium</h2><br />
        </div> -->
        <div class="grid_12">
          <ul class="marked-list">
            <li>Solve any parenting problem with our 9000 videos</li>
            <li>Get inspired from 500 of the nation’s top experts.</li>
            <li>Gung Wang premium content</li>
            <li>Laugh and learn with our funny and entertaining TV Show</li>
            <li>Special access to experts</li>
            <li>Use Gung Wang with no preroll ads</li>
            <li>Get special offers on products</li>
          </ul>
        </div>
        <!-- <div class="grid_6">
          <ul class="marked-list">
            <li>Solve any parenting problem with our 9000 videos</li>
            <li>Get inspired from 500 of the nation’s top experts.</li>
            <li>Gung Wang premium content</li>
          </ul>
        </div>
        <div class="grid_6">
          <ul class="marked-list">
            <li>Laugh and learn with our funny and entertaining TV Show</li>

            <li>Special access to experts</li>
            <li>Use Gung Wang with no preroll ads</li>
            <li>Get special offers on products</li>
          </ul>
        </div> -->

			</div>
		</div>
	</section>


      <!-- LOGOS -->

	<section class="well well_off1 bg-1">

		<div class="container">
			<div class="row">
                 <div class="grid_12">

            <ul class="inline-list">
                  <li><a href="#"><img src="<?php echo path_to_theme()?>/images/vip/logo-NBC.png" alt="NBC"/></a></li>
                  <li><a href="#"><img src="<?php echo path_to_theme()?>/images/vip/logo-bloomberg.png" alt="Bloomberg"/></a></li>
                  <li><a href="#"><img src="<?php echo path_to_theme()?>/images/vip/logo-CBS.png" alt="CBS"/></a></li>
                  <li><a href="#"><img src="<?php echo path_to_theme()?>/images/vip/logo-FOX.png" alt="Fox"/></a></li>
                  <li><a href="#"><img src="<?php echo path_to_theme()?>/images/vip/logo-ABC.png" alt="ABC"/></a></li>
                  <li><a href="#"><img src="<?php echo path_to_theme()?>/images/vip/logo-people.png" alt="People"/></a></li>
                  <li><a href="#"><img src="<?php echo path_to_theme()?>/images/vip/logo-USA.png" alt="USAToday"/></a></li>
                  <li><a href="#"><img src="<?php echo path_to_theme()?>/images/vip/logo-reuters.png" alt="Reuters"/></a></li>
              </ul>


			</div>
		</div>

	</section>

      <!-- END LOGOS -->

	<section class="parallax parallax02" data-parallax-speed="-0.4">

		<div class="container">

			<div class="row">
				<div class="grid_3">
					<div class="testimonials">
						<blockquote data-equal-group="2">As parents, any help we can get is a good thing. Award winning Gung Wang has experts waiting at your fingertips.<br /><em>- Maria Shriver, Anchor at NBC News, Journalist, Mother</em></blockquote>
						<p><img class="circle" src="<?php echo path_to_theme()?>/images/vip/shriver.jpg" alt="Maria Shriver"></p>
					</div>
				</div>
				<div class="grid_3">
					<div class="testimonials">
						<blockquote data-equal-group="2">Gung Wang is the ultimate online source for parenting - seeking advice on how to handle a situation with your children has never been easier and more effective.<br /><em>- Edward Hallowell, MD, EdD, Psychiatrist, Delivered From Distraction</em></blockquote>
						<p><img class="circle" src="<?php echo path_to_theme()?>/images/vip/hallowell.jpg" alt="Edward Hallowell"></p>
					</div>
				</div>
				<div class="grid_3">
					<div class="testimonials">
						<blockquote data-equal-group="2">A great new site for parents!<br /><em>- Geena Davis, Academy Award Winning Actor & Founder of Geena Davis Institute on Gender in Media</em></blockquote>
						<p><img class="circle" src="<?php echo path_to_theme()?>/images/vip/davis.jpg" alt="Geena Davis"></p>
					</div>
				</div>
				<div class="grid_3">
					<div class="testimonials">
						<blockquote data-equal-group="2">What a gift this is for parents...a go-to resource for anything to do with parenting!<br /><em>- Yalda T. Uhls, MBA, PhD, Regional Director of Common Sense Media</em></blockquote>
						<p><img class="circle" src="<?php echo path_to_theme()?>/images/vip/uhls.jpg" alt="Yalda T. Uhls"></p>
					</div>
				</div>
			</div>
		</div>
	</section>

</main>
<!------------------------------------------------------------------------------------------------>
<?php endif; ?>




  <?php if (isset($page['footer'])) : ?>
    <div class="copyright">
    				©2016 gungwang.com. All Rights Reserved.
    				<!-- {%FOOTER_LINK} -->
    </div>
  <?php endif; ?>

  <div id="block-menu-menu-kith-mobile2-menu" class="block block-menu first odd" role="navigation">

  <?php
   $menu_depth = 2;
   $menu_kth_mobile2 = menu_tree_output(menu_tree_all_data('menu-kith-mobile2-menu', null, $menu_depth));
  print(drupal_render($menu_kth_mobile2));
  ?>

 </div>

</div>
