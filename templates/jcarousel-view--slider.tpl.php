<?php
/**
 * @file jcarousel-view.tpl.php
 * View template to display a list as a carousel.
 */
?>
<ul class="<?php print $jcarousel_classes; ?>">
  <?php foreach ($rows as $id => $row): ?>
  
    <li class="home-slide-<?php print $id?> <?php print $row_classes[$id]; ?>"><?php print $row; ?></li>
  <?php endforeach; ?>
</ul>
