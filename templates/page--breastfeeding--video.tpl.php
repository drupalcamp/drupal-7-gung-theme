<?php
/**
 * @file
 * Alpha's theme implementation to display a single Drupal page.
 */
 drupal_add_js("/jwplayer/jwplayer.js");
 drupal_add_js('jwplayer.key="BNjEbsJqJrn7XGgzaS03+1+ueB2wC7mHsq/NwVnnwWJdskJr";', 'inline');
?>
<style>
#section-content{
  min-height: 300px;
  margin-top: -10px;
}
#region-content{

  background-image: url("/sites/all/themes/gung_theme/images/gated_content/breastfeeding-left.png");
  background-repeat: no-repeat;
  background-size: 360px 364px;
  background-position: 0 24px;
  /*background-color: rgb(255, 255, 255);*/
}
#baby-care-new-wrapper{
  background-color: #ffffff;
}

#gated-content-quick-registration-form{
 padding-top:8px;
}

.special-video {
    font-size: 17px;
    margin-top: 0px;
}
#gated-content-reg h1 {
    font-size: 47px;
    font-weight: bold;
}
</style>
<script>
/*
jQuery(document).ready(function($) {
  jQuery("p.special-video").replaceWith('<p class="special-video">Preview Breastfeeding chapter from Baby Care 101 DVD!</p>');
});
*/
</script>

<div<?php print $attributes; ?>>

  <?php if (isset($page['content'])) : ?>

    <div class="bgate-header">
    <div class="bcarelogo">
              <img src="/sites/all/themes/gung_theme/images/babycare/baby-care-logo.jpg" alt="baby care logo">
          </div>

          <div class="logo-soup">
              <img src="/sites/all/themes/gung_theme/images/babycare/ass-seen-on-full.jpg" alt="as seen on">
          </div>

    </div>
    <!-- <div class="side-pic-content-gate">
    <img src="/sites/all/themes/gung_theme/images/babycare/baby.jpg" alt="Best Baby Shower Gift!"/>
    </div> -->
    <?php
    // $block = module_invoke('block', 'block_view', '193');
    //print render($block['content']);
    //print "<pre>"; print_r($page['content']); print "</pre>";
    ?>
    <?php print render($page['content']); ?>

      <?php
       $block = module_invoke('block', 'block_view', '192');
       $block_html = render($block['content']);
      ?>

      <div id="zone-content" class="zone zone-content clearfix container-12">
        <div class="grid-12 region region-content" id="region-content">
          <div class="region-inner region-content-inner">
                      <?php echo $block_html; ?>
          </div>
        </div>
      </div>

  <?php endif; ?>


  <div id="block-menu-menu-kith-mobile2-menu" class="block block-menu first odd" role="navigation">

    <?php
    $menu_depth = 2;
    print(drupal_render(menu_tree_output(menu_tree_all_data('menu-kith-mobile2-menu', null, $menu_depth))));
    ?>

  </div>

</div>
