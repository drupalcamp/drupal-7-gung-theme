<div id="professional-profile-wrapper">

<?php
//get user picture or default
$picture = theme('user_picture', array('account'=>$profile_user, 'style_name'=>'user_profile_thumbnail'));

//get username or real name
$name = theme('username', array('account' => $profile_user));
global $user;
$current_uid = $user->uid;
$profile_uid = $profile_user->uid;
//get playlists

$playlists = $user_profile['kih_playlist_node_references'];
$playlist_output = '';
$playlist_count = 0;
foreach($playlists['#items'] as $item) {
  $playlist_node = node_load($item['nid']);
  if(is_object($playlist_node)) {
    if($playlist_node->kih_public_private_option['und'][0]['value'] != 'private' &&
      $playlist_node->kih_playlist_type['und'][0]['value'] != 'save_for_later') {
      $playlist_view = node_view($playlist_node);
      $playlist_output .= render($playlist_view['kih_video_node_references']);
      $playlist_count++;
    }
  }
}

$videos = kih_videos_must_watch();
$must_watch_playlists = theme('kih_videos_carousel', array('items' => $videos,'link_type' => 'must_watch', 'ajax_vid' => FALSE, 'id' => 'must-watch-videos', 'extra_option' => '', 'scroll' => 3));
$videos = kih_videos_most_popular();
$most_popular_videos = theme('kih_videos_carousel', array('items' => $videos, 'ajax_vid' => FALSE, 'link_type' => 'most_popular', 'id' => 'most-popular-videos', 'extra_option' => 'views', 'scroll' => 3));


//get user info
$bages_obj = $user_profile['field_badges']['#items'];
$founding_member = "";
$top_commentor = "";
foreach ($bages_obj as $k=>$v) {
  if ($v['value'] == 'founding_member') {
    $founding_member = 'Founding Member';
  }
  if ($v['value'] == 'founding_member') {
    $top_commentor = '<img style="float: left" title="Top Commentor" src="/' . drupal_get_path("theme", "gung_theme") . '/images/' . 'top-commenter.png">';
  }
}
//show Friend/Message buttons based on relationships
$relations = $user_profile['user_relationships_ui']['relations']['#markup'];
$actions = $user_profile['user_relationships_ui']['actions']['#markup'];
//print "rel=" . $relations . " actions=" . $actions;
if ($current_uid == $profile_uid) {	//self
  $friend_btn = "";
  $message_btn = "";
} else if ($current_uid == 0) {
  $friend_btn = '<a class="submit-button" onClick="clickLogin();" href="javascript:void(0)">Add As Friend</a>';
  $message_btn = '<a class="submit-button-gray" onClick="clickLogin();" href="javascript:void(0)">Send Message</a>';
}
else if (preg_match("/Friend/", $relations)) {	//Friend
  $remove_link = substr($relations, strpos($relations, "<a href"), strpos($relations, "</a>") + 4 - strpos($relations, "<a href"));
  $remove_link = str_replace("Remove", "Remove Friend", $remove_link);
  $friend_btn = "<div class=\"user-button\"><a class=\"submit-button-trans\">✓&nbsp; Friends</a><div class=\"user-button-options\"><div class=\"user-button-options-arrow\"></div><ul><li>" . $remove_link . "</li></ul></div></div>";
  $message_btn = "<a class=\"submit-button-gray\" href=\"/messages/new/" . $profile_uid . "?destination=user/" . $profile_uid . "\">Send Message</a>";
}
else if (preg_match("/You have sent/", $actions)) {	//Friend request sent to this user
  $resend_link = "<a href=\"/relationship/" . $profile_uid . "/request/1?destination=user/" . $profile_uid . "\">Resend</a>";
  $pending_link = l('View Pending Requests', 'friends', array('fragment' => 'sent'));
  $friend_btn = "<div class=\"user-button\"><a class=\"submit-button-trans\">Friend Request Sent</a><div class=\"user-button-options\"><div class=\"user-button-options-arrow\"></div><ul><!--<li>" . $resend_link . "</li>--><li>" . $pending_link . "</li></ul></div></div>";
  $message_btn = "<a class=\"submit-button-gray\" href=\"/messages/new/" . $profile_uid . "?destination=user/" . $profile_uid . "\">Send Message</a>";
}
else if (preg_match("/This user has requested/", $actions)) {	//Friend request received from this user
//$approve_link = str_replace("pending requests", "View pending requests", substr($actions, strpos($actions, "<a href"), strpos($actions, "</a>") + 4 - strpos($actions, "<a href")));
//$decline_link = str_replace("pending requests", "View pending requests", substr($actions, strpos($actions, "<a href"), strpos($actions, "</a>") + 4 - strpos($actions, "<a href")));
//$pending_link = str_replace("pending requests", "View pending requests", substr($actions, strpos($actions, "<a href"), strpos($actions, "</a>") + 4 - strpos($actions, "<a href")));
  $pending_link = l('View Pending Requests', 'friends', array('fragment' => 'received'));
  $friend_btn = "<div class=\"user-button\"><a class=\"submit-button-trans\">Respond to Friend Request</a><div class=\"user-button-options\"><div class=\"user-button-options-arrow\"></div><ul><li>" . $pending_link . "</li></ul></div></div>";
  $message_btn = "<a class=\"submit-button-gray\" href=\"/messages/new/" . $profile_uid . "?destination=user/" . $profile_uid . "\">Send Message</a>";
}
else {	//not Friend
  $friend_btn = "<a class=\"submit-button user_relationships_popup_link\" href=\"/relationship/" . $profile_uid . "/request/1?destination=user/" . $profile_uid . "\">Add As Friend</a>";
  $message_btn = "<a class=\"submit-button-gray\" href=\"/messages/new/" . $profile_uid . "?destination=user/" . $profile_uid . "\">Send Message</a>";
}


$current_town = $user_profile['field_current_town'][0]['#markup'];
$home_town = $user_profile['field_home_town'][0]['#markup'];
$marital_status = $user_profile['field_marital_status'][0]['#markup'];
$profession = $user_profile['field_profession'][0]['#markup'];
$philosophy = $user_profile['field_philosophy'][0]['#markup'];
$current_town = $user_profile['field_current_town'][0]['#markup'];

//get badges
$badges_obj = $user_profile['field_badges']['#items'];
$founding_member = "";
$top_commenter = "";
foreach ($badges_obj as $k=>$v) {
  if ($v['value'] == 'founding_member') {
    $founding_member = 'Founding Member';
  }
}
?>

<?php
/**********************************************
 *** show Professional or non-Professional user profile
 **********************************************/

  //get Professional fields and show Professional user profile
  $short_description = $user_profile['field_short_description'][0]['#markup'];
  $phone = $user_profile['field_phone'][0]['#markup'];
  $website = $user_profile['field_website'][0]['#markup'];
  $email = $user_profile['field_email'][0]['#markup'];
  $biography = $user_profile['field_biography'][0]['#markup'];

  $experts_obj = $user_profile['field_recommended_experts']['#object']->field_recommended_experts['und'];
  //print_r($experts_obj);
  $experts = "";

  if(count($experts_obj)>0){
    foreach ($experts_obj as $k=>$v) {
      if ($experts != "") $experts .= ", ";
      $experts .= l($v['node']->title, "node/" . $v['node']->nid);
    }
  }

  $resources_obj = $user_profile['field_recommended_resources'];
  $resources = "";

  if(count($resources_obj)>0){
    foreach ($resources_obj as $k=>$v) {
      if ($resources != "") $resources .= ", ";
      if (array_key_exists('#markup', $v)) $resources .= $v['#markup'];
    }
  }

  ?>

  <h2 style="font-family: Eraser; margin-top: 15px;">Gung Wang <span style="color: #EB1D23">PRO</span>file</h2>
  <div style="position: relative;">
    <div id="user-profile" class="pro-profile">
      <div class="user-pro-clip"></div>
      <div class="user-pro-books"></div>

      <div id="user-pic">
        <?php print $picture; ?>
        <div id="user-name"><?php print $name; ?>
          <?php if ($online_status == "Online"): ?>
            <img class="online-status" src="/sites/all/modules/custom/kih_p2p/advanced_forum_styles/gung_theme/images/forum_user_online.png" alt="Online" />
          <?php endif; ?></div>
        <!--<div id="user-profession"><?php print $profession; ?></div>-->
        <?php if ($founding_member != "") : ?>
          <div id="user-badge" class="founding-member"><?php print $founding_member; ?></div>
        <?php endif; ?>

      </div>

      <div style="float: left; margin-top: 10px; margin-left: 10px;" id="user-buttons">
        <?php print $friend_btn; ?>
        <?php print $message_btn; ?>
      </div>
      <div id="user-right">

        <?php if ($profession != "") : ?>
          <div class="pro-field"><strong><?php print $profession; ?></strong></div>
        <?php endif; ?>
        <?php if ($current_town != "") : ?>
          <div class="pro-field">Located in <strong><?php print $current_town; ?></strong></div>
        <?php endif; ?>
        <?php if ($short_description != "") : ?>
          <div class="pro-field"><?php print $short_description; ?></div>
        <?php endif; ?>
        <?php if ($phone != "") : ?>
          <div class="pro-field"><?php print $phone; ?></div>
        <?php endif; ?>
        <?php if ($website != "") : ?>
          <div class="pro-field"><?php print $website; ?></div>
        <?php endif; ?>
        <?php if ($email != "") : ?>
          <div class="pro-field"><?php print $email; ?></div>
        <?php endif; ?>

        <?php if ($experts != "") : ?>
          <div class="pro-field">Recommended Experts: <?php print $experts; ?></div>
        <?php endif; ?>
        <?php if ($resources != "") : ?>
          <div class="pro-field">Recommended Resources: <?php print $resources; ?></div>
        <?php endif; ?>
      </div>


    </div>
  </div>

  <?php if ($philosophy != "") : ?>
    <h3>My Philosphy</h3>
    <div id="user-pro-philosophy"><span><?php print $philosophy; ?></span></div>
  <?php endif; ?>

  <?php if ($biography != "") : ?>
    <h3>Biography</h3>
    <div id="user-pro-biography"><?php print $biography; ?></div>
  <?php endif; ?>

  <?php if ($playlist_output != "") : ?>
    <h3>My Playlist</h3>
    <?php print $playlist_output;?>
    <br><br>
  <?php endif; ?>


  <?php if($playlist_count <= 2) :?>
    <h3>Gung Wang Recommended Videos</h3>
    <h2 class="block-title">Must Watch Videos</h2>
    <?php print $must_watch_playlists; ?>
    <h2 class="block-title">Most Popular</h2>
    <?php print $most_popular_videos ;?>
    <br><br>
  <?php endif;  ?>



</div>
