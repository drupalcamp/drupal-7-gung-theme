

<div id="nav-menu-block">
  <div>
	<ul class="articles nav-menu">
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/editorials/all-parents">All Parents</a></span>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/editorials/pregnancy">Pregnancy</a></span>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/editorials/adoption">Adoption</a></span>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/editorials/baby">Baby</a></span>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/editorials/toddler">Toddler</a></span>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/editorials/preschool">Preschool</a></span>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/editorials/elementary">Elementary</a></span>
		</li>
	    <li class="menu-parent">
			<span class="cat2-h2"><a href="/editorials/teen">Teen</a></span>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/editorials/special-needs">Special Needs</a></span>
		</li>
		</ul>
	</div>

</div>
