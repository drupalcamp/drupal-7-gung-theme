<?php
// gung view template for top_products View
// May 31, 2017
//
/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
// print '<pre>';
//var_dump(get_defined_vars());
//var_dump($fields);
// print '</pre>';
//print $view->get_title();

drupal_add_css(base_path(). 'sites/all/themes/gung_theme/css/style-topproducts.css', array('type' => 'external'));
$url = drupal_get_path_alias(current_path());
$image_path = base_path(). 'sites/all/themes/gung_theme/images/top_products/';
//$output = $view;
//print "<pre>"; print_r($view); print "</pre>";

/*for ($i=0; $i<count($view_result); $i++){
 $node = $result[0];
 print "<pre>"; print_r($node->nid); print "</pre>";
}
*/
?>

<div class="products-bar">
  <h1 class="title">Top Products</h1>
  <div class="top-products-text ">
    <h1 class="red-eraser">Leana's Favorite New Products</h1>
  </div>
  <div class="leana-photo"><img src="<?php echo $image_path?>leana.png" alt="Leana"></div>
</div>

<div class="<?php print $classes; ?>">

  <?php if ($rows): ?>
    <div class="view-content">
       <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>


  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

</div>
