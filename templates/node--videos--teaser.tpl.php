<?php

if(!isset($node->path)) {
	$node->path = url('node/' . $node->nid );
}

if(isset($node->field_video_thumbnail['und']['0']['uri'])){
$image_url = image_style_url('video_thumbnail_large',$node->field_video_thumbnail['und']['0']['uri']);
}

if(isset($node->field_transcription[LANGUAGE_NONE][0]['value']) && !empty($node->field_transcription[LANGUAGE_NONE][0]['value'])) {
  $transcription = ' | Transcript:' . $node->field_transcription[LANGUAGE_NONE][0]['value'];
  $fulltranscript = $node->field_transcription[LANGUAGE_NONE][0]['value'];
} else {
	$transcription = '';
  $fulltranscript = '';
}


//dpm(arg(1));

if(arg(1) == 'term' || arg(1) == 'site'){
  $transcription = '';
  $fulltranscript = '';
}

$expert_path = url('node/' . $expert_nid);
$expert_node = node_load($expert_nid);
//Currently, $node->link_target is only used in kih_facebook_tabs.module
$link_target = isset($node->link_target) ? ' target="' . $node->link_target . '"' : '';

if(isset($_SERVER['HTTPS'])) {
	$target = '_blank';
} else {
	$target = '_top';
}


$rp = explode('/', request_path());
$rp = $rp[0];

if($rp == 'playlist'){

  //print 'lkjdsaflajsdf;lkasdjfas;dlfj';
  $videopath = '/'.request_path().'?id='.$node->nid.'#playvid/'.$node->nid;
  $teaserforplist = '<div class="fulltranscription" data-bc-video-id="'.$node->field_video_source['und'][0]['video_id'].'">'.$fulltranscript.'</div>';

}
else{
  $videopath = $node->path;
  $teaserforplist = '';
}


?>

<div class="video-search-result-video videos-thumb" data-bc-video-id="<?php print $node->field_video_source['und'][0]['video_id']; ?>"  id="video-<?php  print $node->nid?>">

<?php
//dpm($node);
if(!isset($node->ajax_vid)){ $node->ajax_vid = ""; }
if(!isset($node->extra_info)){ $node->extra_info = ""; }
?>
  <div class="video-thumb">
    <a title="<?php print $expert_name ?>, <?php print $job_title; ?>" class="expert-play-vid <?php $node->ajax_vid ? print 'use-ajax-vid' : ''?> " data-direct-path="<?php print $node->path; ?>" data-bc-video-id="<?php print $node->field_video_source['und'][0]['video_id']; ?>" href="<?php print $videopath;?>" target="<?php print $target; ?>">
	    <span class="play tooltip" title=""></span>
      <img alt="<?php print $expert_name ?>" title="<?php print $expert_name ?>, <?php print $job_title; ?>" class="show-for-landscape" src="<?php print $image_url; ?>">
    </a>
  <div class="nowPlayingMask">Now Playing</div>
 </div>

  <div class="video-thumb-text">
    <div class="title"><a title="<?php print $expert_name ?>, <?php print $job_title; ?> <?php print $transcription?>" data-direct-path="<?php print $node->path; ?>" class="<?php $node->ajax_vid ? print 'use-ajax-vid' : ''?> " href="<?php print $videopath;?>"<?php print $link_target;?>><?php print $title; ?></a></div>
    <div class="expert"><a title="<?php print $expert_name ?>, <?php print $job_title; ?>" href="<?php print $expert_path;?>"<?php print $link_target;?>><?php print $expert_name; ?></a></div>
    <div class="job_title"><?php print $job_title; ?></div>
    <div class="extra-info"><?php print $node->extra_info; ?></div>
    <?php if($teaserforplist): ?>
	<div class="teaser-bio-data" data-bc-video-id="<?php print $node->field_video_source['und'][0]['video_id'];?>">

	<?php

	//print kih_experts_more_from_data($expert_nid);
	$bio = kih_experts_more_from_data($expert_nid);
	print $bio['experts_bio'];
	print '<span class="more-expert-vids-link">';
	print l('More Parenting Videos from ' . $expert_node->title . ' >', 'node/' . $expert_nid, array('attributes' => array('class' => array('right', 'more_link'), 'title' => 'Parenting Videos from ' . $expert_node->field_job_title[LANGUAGE_NONE][0]['value'] . ', ' .  $expert_node->title . ' at gungwang.com' )   ));
	print '</span>';
	?>

	</div>
    <?php endif; ?>

 </div>

</div>
