<article<?php print $attributes; ?> itemtype="https://schema.org/Article">

	  <?php print render($content['field_article_image']); ?>

  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
  <header>
    <h2<?php print $title_attributes; ?> ><a href="<?php print $node_url ?>" title="<?php print $title ?>" itemprop="name"><?php print $title ?></a></h2>
  </header>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($display_submitted): ?>
  <footer class="submitted" itemprop="dateCreated"><?php print $date; ?> -- <?php print $name; ?></footer>
  <?php endif; ?>


  <?php
    $created = date('M d, Y', $created);
    print '<div class="date">'. $created. '</div>';
    //var_dump($field_other_author);
    if (isset($field_other_author['und'][0])) {
      $byline = $field_other_author['und'][0]['safe_value'];
    }else{
			$byline = false;
		}

		if ( isset($field_article_expert_reference['und'])
				&& is_array($field_article_expert_reference['und']) ){

			foreach ($field_article_expert_reference['und'] as $author) {
				//    $node = node_load($field_article_expert_reference['und'][0]['nid']);
				//    print '<div class="author">By: '. l($node->title, 'node/'. $node->nid). '</div>';
				$node = node_load($author['nid']);

				// We should really have a comma between everything but the last one
				// but Andrea assures there will never be more than two....
				if ($byline) {
					$byline .= ' & ' . l($node->title, 'node/'. $node->nid);
				} else {
					$byline .= l($node->title, 'node/'. $node->nid);
				}
			}
		}


//    if (strpos($byline, ', ')) {
//      $byline = substr_replace($byline, ' & ', strrpos($byline, ', '), 2);
//    }
	print '<div class="author">By: ' . $byline . '</div>';

  ?>

  <div<?php print $content_attributes; ?> itemprop="articleBody">
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
	  hide($content['field_article_image']);
      print render($content);
    ?>
  </div>

  <div class="article-readmore"> <?php

  $redarrow = 'Read More <img src="/'.drupal_get_path('theme', 'gung_theme').'/images/read-more-red-arrow.jpg" />';


  print l($redarrow, 'node/' . $node->nid, array('html'=>TRUE));?> </div>

</article>
