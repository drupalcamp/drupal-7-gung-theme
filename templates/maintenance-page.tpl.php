<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>">
  <style>
html { background: url('/sites/all/themes/gung_theme/images/landing_page_brick_bg.jpg') repeat !important; font-family: "Franklin Book Gothic", sans-serif; }
body { background: none !important; }

h1 { font-family: Eraser; font-size: 60px; font-weight: normal; color: #eb1d23; margin-bottom: 40px; }
#unsup-page { position: relative; width: 1110px; height: 674px; margin: 0px auto; background: url('sites/all/themes/gung_theme/images/unsupported_browser_page_bg.png') no-repeat; }
#unsup-content { position: absolute; top: 280px; left: 380px; width: 390px; font-size: 16px; line-height: 1.5em; color: #010101; }

</style>

<div id="unsup-page">

	<div id="unsup-content">
		<?php print $content; ?>
	</div>

</div>


</body>
</html>
