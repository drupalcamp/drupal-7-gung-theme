<?php //print "<pre>html--taxonomy.tpl.php "; print_r(arg()); print "</pre>";  ?>
<?php print $doctype; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf->version . $rdf->namespaces; ?>>
<head<?php print $rdf->profile; ?>>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo GACODE;?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', '<?php echo GACODE;?>');
</script>


<!-- End Google Tag Manager -->
<?php if(NOINDEX): ?>
<meta name="robots" content="noindex">
<?php endif;?>

  <?php
  $tid = arg(2);
  $term = taxonomy_term_load($tid);
  //dpm($term);

  if(isset($term->metatags['und'])){
    $title = $term->metatags['und']['title']['value'];
    $description = $term->metatags['und']['description']['value'];
  }
  else{
    $title = $term->name;
    $description = $term->description;
  }

  if(isset($title)){
    print '<title>'.$title.'</title>';
  }
  else{
    print '<title>'.$term->name.' Videos</title>';
  }

  if(isset($description)){
    print '<meta name="description" content="'.$description.'" />';
  }
  else{
    print '<meta name="description" content="'.$term->description.'" />';
    // print '<meta name="description" content="'.term->description.'" />';
  }
  ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php print $head; ?>
  <?php print $styles; ?>
  <link rel="stylesheet" media="all" href="/sites/all/themes/gung_theme/css/responsive.css?id=1" />
  <?php print $scripts; ?>
  <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<?php if ($plugin = context_get_plugin('reaction', 'block')) : ?>
  <?php
  $html_top = $plugin->block_get_blocks_by_region('html_top');
  print render($html_top);
   ?>
<?php endif; ?>

  <link rel="publisher" href="https://plus.google.com/+gungwang/">

  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer', '<?php echo GTMCODE;?>');</script>

</head>

<body <?php print $attributes;?>>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=<?php echo GTMCODE;?>"
 height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->

<!-- indicators for responsive jQuery -->
<div id="responsive-indicator"></div>
<div id="mobile-indicator"></div>


<?php if (drupal_get_path_alias($_GET["q"]) == "coming-soon"): ?>
<?php include("coming-soon.tpl.php"); ?>
<?php elseif (drupal_get_path_alias($_GET["q"]) == "validate-email"): ?>
<?php include("validate-email.tpl.php"); ?>
<?php elseif (drupal_get_path_alias($_GET["q"]) == "unsupported-browser"): ?>
<?php include("unsupported-browser.tpl.php"); ?>
<?php elseif (preg_match('/(?i)msie [2-7]/', $_SERVER['HTTP_USER_AGENT']) && (drupal_get_path_alias($_GET["q"]) != "unsupported-browser")): //browser less than IE8 '/(?i)msie [1-7]/' ?>
<?php  drupal_goto("unsupported-browser"); ?>
<?php else: ?>

  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
<?php endif; ?>

</body>
</html>
