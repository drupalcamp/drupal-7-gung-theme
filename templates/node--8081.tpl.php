
<?php
        ctools_include('ajax');
        ctools_include('modal');
        ctools_modal_add_js();

?>

<div id="about-banner"><img src="<?php print drupal_get_path('theme', 'gung_theme'); ?>/images/about_banner.png" /></div>

<div id="about-main">
	<div id="about-text">
		<h2 class="what-is-kith">What is gungwang?</h2>
      <a class="about-us-read-more" href="javascript: void(0);" onClick="jQuery('#about-content').toggle();">
          <img src="<?php print drupal_get_path('theme', 'gung_theme'); ?>/images/about-us-read-more.png" /></a>

		<?php
		/*
        ctools_include('ajax');
        ctools_include('modal');
        ctools_modal_add_js();
        */

		$imgtour =  '<img src="/'.drupal_get_path('theme', 'gung_theme') . '/images/about-us-ceo-leana.png" alt="About Leana Greene">';
		print ctools_modal_text_button($imgtour, 'content-modal/48583', t('About Gung Wang'),'about-intro-link ctools-modal-ctools-kith-wide-style');
		?>

	</div>
	<div id="about-video">
		<?php // $block = module_invoke('views', 'block_view', 'slider-block_1');
		//print render($block['content']); ?>
		<?php
		/*
        ctools_include('ajax');
		ctools_include('modal');
		ctools_modal_add_js();
        */
		$imgtour =  '<img src="/'.drupal_get_path('theme', 'gung_theme') . '/images/about-us-baby.jpg" alt="Gung Wang Tour">';
		print ctools_modal_text_button($imgtour, 'content-modal/48583', t('About Gung Wang'),'about-intro-link ctools-modal-ctools-kith-wide-style');
		?>
	</div>
	<div style="display: none" id="about-content">
	  <?php print $node->body['und'][0]['value']; ?>
	</div>
</div>


</div>



	<?php
	//$src = '../images/about_infographic.png';
    //$img = theme('image_style', array('path'=>$src));
    //$path =  "infographic";
    //print l($img, $path, array('attributes' => array('class' =>'link-image'),'html' => true, 'query' => drupal_get_destination()));
	?>
</div>
