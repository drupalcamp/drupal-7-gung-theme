<?php
/**
 * @file
 * Alpha's theme implementation to display a single Drupal page.
 */

 drupal_add_js("/jwplayer/jwplayer.js");
 drupal_add_js('jwplayer.key="BNjEbsJqJrn7XGgzaS03+1+ueB2wC7mHsq/NwVnnwWJdskJr";', 'inline');
?>

<style>
.video-outer-wrapper {
    height: 536px;
}
</style>
<div<?php print $attributes; ?>>
  <?php if (isset($page['header'])) : ?>
    <?php print render($page['header']); ?>
  <?php endif; ?>


  <div id="global-test-wrapper">

  <!-- ========= -->
  <!-- Your HTML -->




  <div itemprop="video" itemscope itemtype="https://schema.org/VideoObject">
  <meta itemprop="thumbnail" content="/sites/default/files/video-thumbnails/1322402276001_3340711391001_Tina-Meier_19.jpg" /><meta itemprop="duration" content="T975S"><div id="video_top_level_info">
  <h1><span itemprop="name">The tragic story of Megan Meier&#039;s suicide</span></h1>
  <div class="expert"></div>
  <div class="job_title"></div>
</div>

<div id="video_player_wrapper"><div class="field field-name-field-video-source field-type-brightcove-video field-label-hidden"><div class="field-items"><div class="field-item even">

  <div id="video_player">
<!--
<script type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>
			        <object id="myExperience3339897674001" class="BrightcoveExperience" >
								<param name="bgcolor" value="#FFFFFF" />
								<param name="width" value="100%" />
								<param name="height" value="400px" />
								<param name="playerID" value="3303933705001" />
								<param name="autoStart" value="TRUE" />
								<param name="playerKey" value="AQ~~,AAABM-VRYqE~,LzoKrsWwczferBvtw_09FSrQXzQwxG42" />
								<param name="isVid" value="true" />
								<param name="isUI" value="true" />
								<param name="dynamicStreaming" value="true" />
								<param name="includeAPI" value="true" />
								<param name="templateLoadHandler" value=myTemplateLoaded />
								<param name="templateReadyHandler" value=onTemplateReady />
								<param name="wmode" value="transparent" />
								<param name="@videoPlayer" value="3339897674001" />
								<param name="additionalAdTargetingParams" value="LR_DURATION=15" />
							</object>
							<script type="text/javascript">brightcove.createExperiences();</script>
							<div onClick="javascript: closeVideoPlayer();" id="videos_player_close">X</div>

-->
<?php print kih_videos_player("100%", "400", 'false', 3303933705001); ?>
							</div>
							<span id="main-player-info" data-mp-bcexperience="myExperience3339897674001" data-mp-width="100%" data-mp-height="400px" data-mp-player-id="3303933705001" data-mp-player-key="AQ~~,AAABM-VRYqE~,LzoKrsWwczferBvtw_09FSrQXzQwxG42" data-mp-video-id="3339897674001"></span></div></div></div><div id="video_info">

                <div style="display:none;" id="active_video_nid"></div>
                <div id="playlist_form"><form action="/teenager/bullying/types/tragic-story-megan-meiers-suicide?qt-more_videos=1" method="post" id="kih-playlist-add-form" accept-charset="UTF-8"><div><div class="form-item form-type-select form-item-playlist">
 <select id="edit-playlist" name="playlist" class="form-select"><option value="0">Watch Later</option><option value="17241">Add to Favorites</option><option value="17231">Watch Later</option><option value="4">+ New Playlist</option><optgroup label="Existing Playlists"><option value="8061">new playlist</option><option value="12971">test</option><option value="12991">another test</option><option value="13001">one more test</option><option value="27941">test again</option><option value="27951">asdfasdfa</option></optgroup></select>
</div>
<a href="?q=kih_playlist/new_playlist/nojs&video_nid=46898" class="ctools-modal-ctools-ajax-register-style  ctools-use-modal">New Playlist</a><input type="hidden" name="form_build_id" value="form-bFdemomPZygYgOLxwT5TZInNMLGLVPXzqbmMEP8f3QY" />
<input type="hidden" name="form_token" value="Qu7m06dcgweThcUJ15ACCskzHuG7DlvHoTmCAYKiV7Y" />
<input type="hidden" name="form_id" value="kih_playlist_add_form" />
</div></form></div><a class="button submit-button-gray" id="discuss_button" href="#comments">Comment</a><div id="social_buttons" class=""><!-- AddThis Button BEGIN -->
<div addthis:url="/teenager/bullying/types/tragic-story-megan-meiers-suicide" addthis:title="The tragic story of Megan Meier&#039;s suicide" class="addthis_toolbox addthis_default_style addthis_32x32_style">
<a class="addthis_button_email"></a>
<a class="addthis_button_facebook"></a>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_google_plusone_share"></a>
</div>
<script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js?domready=1#pubid=xa-50be98440a7f3ec0"></script>
<!-- AddThis Button END --></div>



<a class="sample-link">Sample link to test event firing</a>


<h2>Releated Videos</h2>

<div id="related_videos"><table>
<tbody>
 <tr class="odd"><td><a title="" href="/teenager/bullying/what-to-do/when-your-child-instigates-cyberbullying"><div class="play tooltip"> </div>When your child instigates cyberbullying</a></td><td>Tina Meier</td> </tr>
 <tr class="even"><td><a title="" href="/teenager/bullying/prevention/how-technology-fuels-bullying"><div class="play tooltip"> </div>How technology fuels bullying</a></td><td>Tina Meier</td> </tr>
 <tr class="odd"><td><a title="" href="/teenager/bullying/prevention/tips-preventing-kids-cyberbullying"><div class="play tooltip"> </div>Tips for preventing kids from cyberbullying</a></td><td>Tina Meier</td> </tr>
 <tr class="even"><td><a title="" href="/teenager/bullying/what-to-do/what-works-in-combating-bullying"><div class="play tooltip"> </div>What works in combating bullying</a></td><td>Tina Meier</td> </tr>
 <tr class="odd"><td><a title="" href="/teenager/technology-and-media/cyberbullying/using-likes-cyberbullying"><div class="play tooltip"> </div>Using &quot;likes&quot; for cyberbullying</a></td><td>Tina Meier</td> </tr>
 <tr class="even"><td><a title="" href="/teenager/bullying/types/how-elementary-school-kids-experience-bullying"><div class="play tooltip"> </div>How elementary school kids experience bullying</a></td><td>Tina Meier</td> </tr>
 <tr class="odd"><td><a title="" href="/teenager/bullying/role-of-the-school/what-can-i-do-if-the-school-isnt-responsive"><div class="play tooltip"> </div>What can I do if the school isn&#039;t responsive?</a></td><td>Tina Meier</td> </tr>
 <tr class="even"><td><a title="" href="/teenager/bullying/types/how-teens-experience-bullying"><div class="play tooltip"> </div>How teens experience bullying</a></td><td>Tina Meier</td> </tr>
 <tr class="odd"><td><a title="" href="/teenager/bullying/what-to-do/what-to-do-if-your-kid-is-bullied-online"><div class="play tooltip"> </div>What to do if your kid is bullied online</a></td><td>Tina Meier</td> </tr>
 <tr class="even"><td><a title="" href="/teenager/bullying/what-to-do/what-you-dont-know-about-your-kids-experience-of-bullying"><div class="play tooltip"> </div>What you don&#039;t know about your kids&#039; experience of bullying</a></td><td>Tina Meier</td> </tr>
</tbody>
</table>
</div>



<h2>Videos by Expert</h2>
  <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/all-parents/experts/introductions/meet-tina-meier?qt-more_videos=1#qt-more_videos" class="more-expert-vids">Meet Tina Meier</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/bullying/types/tragic-story-megan-meiers-suicide?qt-more_videos=1#qt-more_videos" class="more-expert-vids">The tragic story of Megan Meier&#039;s suicide</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/technology-and-media/cyberbullying/using-likes-cyberbullying?qt-more_videos=1#qt-more_videos" class="more-expert-vids">Using &quot;likes&quot; for cyberbullying</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/bullying/role-of-the-school/what-can-i-do-if-the-school-isnt-responsive?qt-more_videos=1#qt-more_videos" class="more-expert-vids">What can I do if the school isn&#039;t responsive?</a></span>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/technology-and-media/setting-limits/basic-guidelines-child-social-media-profiles?qt-more_videos=1#qt-more_videos" class="more-expert-vids">Basic guidelines for child social media profiles</a></span>  </div>  </div>
  <div class="views-row views-row-6 views-row-even">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/technology-and-media/facebook-and-social-networks/effects-anonymous-websites-askfm?qt-more_videos=1#qt-more_videos" class="more-expert-vids">Effects of anonymous websites like Ask.fm</a></span>  </div>  </div>
  <div class="views-row views-row-7 views-row-odd">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/all-parents/parenting/golden-nuggets/golden-nugget-parenting-advice?qt-more_videos=1#qt-more_videos" class="more-expert-vids">Golden nugget of parenting advice</a></span>  </div>  </div>
  <div class="views-row views-row-8 views-row-even">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/social-life/peer-pressure/helping-teens-unmet-desire-fit?qt-more_videos=1#qt-more_videos" class="more-expert-vids">Helping teens with the unmet desire to fit in</a></span>  </div>  </div>
  <div class="views-row views-row-9 views-row-odd">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/bullying/types/how-elementary-school-kids-experience-bullying?qt-more_videos=1#qt-more_videos" class="more-expert-vids">How elementary school kids experience bullying</a></span>  </div>  </div>
  <div class="views-row views-row-10 views-row-even">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/technology-and-media/setting-limits/how-much-social-media-too-much?qt-more_videos=1#qt-more_videos" class="more-expert-vids">How much social media is too much</a></span>  </div>  </div>
  <div class="views-row views-row-11 views-row-odd">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/parenting-teens/bonding-with-your-teen/how-parents-can-be-there-for-their-adolescents?qt-more_videos=1#qt-more_videos" class="more-expert-vids">How parents can be there for their adolescents</a></span>  </div>  </div>
  <div class="views-row views-row-12 views-row-even">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/bullying/prevention/how-technology-fuels-bullying?qt-more_videos=1#qt-more_videos" class="more-expert-vids">How technology fuels bullying</a></span>  </div>  </div>
  <div class="views-row views-row-13 views-row-odd">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/bullying/types/how-teens-experience-bullying?qt-more_videos=1#qt-more_videos" class="more-expert-vids">How teens experience bullying</a></span>  </div>  </div>
  <div class="views-row views-row-14 views-row-even">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/bullying/what-to-do/resources-for-teens-and-parents?qt-more_videos=1#qt-more_videos" class="more-expert-vids">Resources for teens and parents</a></span>  </div>  </div>
  <div class="views-row views-row-15 views-row-odd">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/all-parents/grief-and-loss/support/resources-for-the-survivors-of-a-child-suicide?qt-more_videos=1#qt-more_videos" class="more-expert-vids">Resources for the survivors of a child suicide</a></span>  </div>  </div>
  <div class="views-row views-row-16 views-row-even">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/bullying/prevention/tips-preventing-kids-cyberbullying?qt-more_videos=1#qt-more_videos" class="more-expert-vids">Tips for preventing kids from cyberbullying</a></span>  </div>  </div>
  <div class="views-row views-row-17 views-row-odd">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/elementary/bullying/what-to-do/what-can-i-do-as-a-parent-to-help-our-kids?qt-more_videos=1#qt-more_videos" class="more-expert-vids">What can I do as a parent to help our kids</a></span>  </div>  </div>
  <div class="views-row views-row-18 views-row-even">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/bullying/what-to-do/what-to-do-if-your-kid-is-bullied-online?qt-more_videos=1#qt-more_videos" class="more-expert-vids">What to do if your kid is bullied online</a></span>  </div>  </div>
  <div class="views-row views-row-19 views-row-odd">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/bullying/what-to-do/what-works-in-combating-bullying?qt-more_videos=1#qt-more_videos" class="more-expert-vids">What works in combating bullying</a></span>  </div>  </div>
  <div class="views-row views-row-20 views-row-even">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/bullying/what-to-do/what-you-dont-know-about-your-kids-experience-of-bullying?qt-more_videos=1#qt-more_videos" class="more-expert-vids">What you don&#039;t know about your kids&#039; experience of bullying</a></span>  </div>  </div>
  <div class="views-row views-row-21 views-row-odd">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/bullying/what-to-do/when-your-child-instigates-cyberbullying?qt-more_videos=1#qt-more_videos" class="more-expert-vids">When your child instigates cyberbullying</a></span>  </div>  </div>
  <div class="views-row views-row-22 views-row-even views-row-last">

  <div class="views-field views-field-title">        <span class="field-content"><a href="/teenager/technology-and-media/cyberbullying/why-kids-create-fake-accounts-be-mean-online?qt-more_videos=1#qt-more_videos" class="more-expert-vids">Why kids create fake accounts to be mean online</a></span>  </div>  </div>
    </div>


  <?php if (isset($page['content'])) : ?>
    <?php print render($page['content']); ?>
  <?php endif; ?>
  <!-- ========= -->

  <!-- ========= -->
  <!-- Libraries -->
  <!-- ========= -->



  </div>
</div>
</div>

  <?php if (isset($page['footer'])) : ?>
    <?php print render($page['footer']); ?>
  <?php endif; ?>


    <div id="block-menu-menu-kith-mobile2-menu" class="block block-menu first odd" role="navigation">

      <?php
      $menu_depth = 2;
      print(drupal_render(menu_tree_output(menu_tree_all_data('menu-kith-mobile2-menu', null, $menu_depth))));
      ?>

    </div>
