
<article<?php print $attributes; ?>>
  <header>
    <?php if ($new): ?>
      <em class="new"><?php print $new ?></em>
    <?php endif; ?>
    <?php if (isset($unpublished)): ?>
      <em class="unpublished"><?php print $unpublished; ?></em>
    <?php endif; ?>
  </header>

  <?php print $picture; ?>

  <footer class="comment-submitted">
   <?php
      $uid = $comment_author->uid;
      print $author_image. '<div class="author">'. l($comment_author->name, 'user/'. $comment_author->uid). '</div>';
      $date = strtotime($datetime);
      $date = date('M j, Y', $date);
      $calc = time() - strtotime($datetime);
      // print 'time ago' if the comment was made less than 3 days ago
      if($calc < 60*60*24*3) {
        print '<div class="time"><time datetime="' . $datetime . '">' . $created . '</time></div>';
      }
      else {
        print '<div class="time">'. $date. '</div>';
      }
    ?>
  </footer>

  <div<?php print $content_attributes; ?>>
    <?php
      hide($content['links']);
      
	  //dpm($content);
	  
	  //dpm($content);
	  $comment_body = $content['comment_body']['#items']['0']['safe_value'];
	  $comment_length = strlen($comment_body);
	  $commentid = $content['comment_body']['#object']->cid;
	  //dpm($comment_length);
	  
	  if($comment_length < '255'){
	  print render($content);
	  }
	  else{
	//  print 'long comment';
	  $pos = strpos($comment_body, ' ', 200);
	  
	  print '<div class="beginning-comment">';
      print '<div class="innercomment" id="short-inner-comment-body-'.$commentid.'">';
	  print substr($comment_body, 0, $pos).'...';
	  print '<a class="comment-read-more" id="comment-body-'.$commentid.'">More</a>';
	  print '</div>';
	  	 

	//	 $long_body = substr($comment_body, $pos );
    print '<div class="ending-comment" id="full-comment-body-'.$commentid.'" style="display: none;">';
	print $comment_body;
	print '<a class="comment-read-less" id="inner-comment-body-'.$commentid.'">Less</a>';
	//  $options = array('handle' => 'Read More', 'content' => $long_body, 'collapsed' => TRUE);
  //print theme('ctools_collapsible',$options);
  print '</div>';
//	  dpm($long_body);
   
	  print '</div>';
	  
  
	  }
    ?>
  </div>

  <?php if ($signature): ?>
    <div class="user-signature"><?php print $signature ?></div>
  <?php endif; ?>

  <?php if (!empty($content['links'])): ?>
    <nav class="links comment-links clearfix"><?php print render($content['links']); ?></nav>
  <?php endif; ?>

</article>
