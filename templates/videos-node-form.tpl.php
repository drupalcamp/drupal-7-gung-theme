<?php
global $user;

if (in_array('transcribers', $user->roles)) {
?>
	
	<style> 
	.vertical-tabs, #edit-field-video-source, .field-name-field-video-category, .field-name-field-video-expert,
	.field-name-field-vid-counter, .field-name-field-popular, .field-name-field-latest, .field-name-field-related, .field-name-field-video-tags,
	.field-name-field-link, input#edit-preview, input#edit-preview-changes, input#edit-delete {
		display: none;
	}
	</style>
	
	<?php
	
	$nid = arg(1);
	$node = node_load($nid);

	$form['title']['#attributes']['disabled'] = 'disabled';
	print render($form['title']);

	print render(field_view_field('node', $node, 'field_video_source'));

	print render($content['field_video_source']);
	print render($form['field_transcription']);

	?>
	
	<div style="float: left">
	<?php 
	/*hide($form['field_video']);
	hide($form['field_video_expert']);
	hide($form['field_video_category']);
	hide($form['field_vid_counter']);
	hide($form['field_video_tags']);
	hide($form['field_popular']);
	hide($form['field_latest']);
	hide($form['field_related']);
	hide($form['field_link']);
	hide($form['field_video_source']);

	hide($form['actions']['preview']);
	hide($form['actions']['preview_changes']);
	hide($form['actions']['delete']);*/
	
	print drupal_render_children($form);
	?>
	</div>
	
	<div style="float: right; margin-top: 1em;">
	<?php print $next; ?><br/>
	<?php print $prev; ?><br/>
	</div>
	
	<?php
	//print_r($form);
}
else {
	print drupal_render_children($form);
}






