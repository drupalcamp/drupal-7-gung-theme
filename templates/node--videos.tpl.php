<link rel="stylesheet" media="all" href="<?php echo base_path();?>sites/all/themes/gung_theme/css/style-video.css" />
<!--  changed and add  itemprop="url" -->
<!-- <div itemprop="video" itemscope itemtype="http://schema.org/VideoObject"> -->

<?php
//drupal_add_js("/jwplayer/jwplayer.js");
//drupal_add_js('jwplayer.key="BNjEbsJqJrn7XGgzaS03+1+ueB2wC7mHsq/NwVnnwWJdskJr";', 'inline');
//$css = '#dsq-app3, #dsq-app4, #dsq-app5, #dsq-app6, #dsq-app7, #dsq-app8, #dsq-app9, #dsq-app10, #dsq-app11 {  display: none; } ';
//$css = '#post-list .advertisement{display: none;}';
//drupal_add_css($css, 'inline');

// Gung: move code to kih_videos.module : line 829 on Dec. 7 2017
// drupal_add_js(path_to_theme() . '/js/scripts-autoplay.js',
//     array('type' => 'file', 'weight' => 50, 'group' => JS_THEME, 'scope' => 'footer'));
?>
<!-- <script>jwplayer.key="BNjEbsJqJrn7XGgzaS03+1+ueB2wC7mHsq/NwVnnwWJdskJr";</script> -->
<div itemscope itemtype="http://schema.org/VideoObject">

  <meta itemprop="name" content="<?php print $node->title; ?>" />
  <link itemprop="url" href="https://gungwang.com<?php  print base_path() . request_path(); ?>"/>

  <meta itemprop="productionCompany" content="Gung Wang" />
  <meta itemprop="playerType" content="HTML5 Flash" />
  <meta itemprop="encodingFormat" content="MP4" />
  <meta itemprop="width" content="620" />
  <meta itemprop="height" content="400" />
  <meta itemprop="videoQuality" content="SD" />
  <meta itemprop="isFamilyFriendly" content="True" />

  <!--  disabled  two properties -->
  <!--  <meta itemprop="status" content="published"> -->
  <!-- <meta itemprop="content_id" content="<?php print $node->nid; ?>"> -->

  <?php


    //$video_cookie[$nid] = $node->field_video_category['und'];
    //print "<Pre>Gung"; print_r($node); print "</pre>";
    if (!user_is_logged_in()){
      kih_videos_watched_handle_cookie($node->nid);
      //print "<Pre>WatchedVideos cookie $node->nid "; print_r($_SESSION['WatchedVideos']); print "</pre>";
    }

  // Gung: thumbnailUrl, uploadDate, status, content_id
  $uploadDate = date("F j, Y, g:i a", $node->created);
  print '<meta itemprop="uploadDate" content="'.$uploadDate.'" />';

  if(isset($node->field_video_expert['und']['0']['node'])){
    $expert_node = $node->field_video_expert['und']['0']['node'];
    $expert_name = $expert_node->title;
    $expert_bio = $expert_node->field_expert_bio['und']['0']['value'];

    $expert_bio = check_plain (drupal_html_to_text( $expert_bio)) ;

    print '<div itemprop="actor" itemscope itemtype="https://schema.org/Person">
         <meta itemprop="name" content="'. $expert_name . '"/>
         <meta itemprop="description" content="'. $expert_bio . '"/>
       </div>';

      // print '<pre>URL: ' ;  print_r($expert_node);  print "</pre>";
  }

  if(isset($node->field_video_thumbnail['und']['0']['uri'])){
    $uri = $node->field_video_thumbnail['und']['0']['uri'];
    $thumbnailurl = file_create_url($uri);
    //$thumbnailurl = '<meta itemprop="thumbnail" content="'.$thumbnailurl.'" />';
  //  dpm($thumbnailurl);
    print '<meta itemprop="thumbnail" content="'.$thumbnailurl.'" />';
    print '<link itemprop="thumbnailUrl" href="'.$thumbnailurl.'">';
  }

  //print '<pre>URL: '. $thumbnailurl ;  print_r($node);  print "</pre>";

  if(isset($node->metatags['und']['description']['value'])){
    print '<meta itemprop="description" content="'.$node->metatags['und']['description']['value'].'" />';
  }

  $duration = $node->field_video_duration['und']['0']['value'];
  if(isset($duration)){

    $minutes = (int)($duration / 60);
    $seconds = (int)($duration - $minutes);

    print '<meta itemprop="duration" content="PT'.$minutes.'M'.$seconds.'S" />';
  }
  ?>

<div id="video_top_level_info">
  <h1><span itemprop="name"><?php print $title; ?></span></h1>
  <div class="expert"><?php //print render($content['field_video_expert']); ?></div>
  <div class="job_title"><?php // print $job_title; ?></div>
</div>

<div id="video_player_wrapper" class="gungtest2">
  <?php
  $vid = $node->field_video_source['und']['0']['value'];
  print kih_videos_player("601", "338", "true", $vid, "");
  //print render($content['field_video_source']);
  ?>

<?php //$str_checkout = "/commerce-express-checkout/" . PRODUCT_CODE . "/" . MEMBERSHIP_CODE. "?"; ?>
<?php $str_checkout ="/kith-register/nojs/choice?expresscheckout=go-premium-to-view&"; ?>
<?php if(!in_array('premium member', array_values($user->roles) )
      && !in_array('premium promotion', array_values($user->roles) ) ): ?>
<div class="signup-premium">
  <a href="<?php echo $str_checkout;?>" class="landing-btn-red livetv-signup">
    <img class="livetv-star" src="/sites/all/themes/gung_theme/images/livetv/star1.png">
    GET ACCESS TO ALL PREMIUM CONTENT WITH NO ADS FOR $4.99/MONTH
    <img class="livetv-star" src="/sites/all/themes/gung_theme/images/livetv/star1.png">
  </a>
</div>
<?php endif; ?>

<div id="video_info"><?php print kih_videos_extra_info($node->nid, $title, url('node/' . $node->nid, array('absolute' => true))); ?>

  <?php
  if(isset($node->metatags['und']['description']['value'])){
    print '<span itemprop="description" class="video-description">';
    print $node->metatags['und']['description']['value'];
    print '</span>';
  }

  ?>

</div>
</div>
<script></script>

  <div itemprop="provider" itemscope itemtype="https://schema.org/Organization">
    <meta itemprop="name" content="Gung Wang">
    <meta itemprop="url" content="https://gungwang.com">
  </div>

</div>

<div id="video-accordion">
    <h3 class="related">Related Videos</h3>
    <div class="related"></div>
    <h3 class="transcript">Transcript</h3>
    <div class="transcript"></div>
    <h3 class="expert-bio">Expert Bio</h3>
    <div class="expert-bio"></div>
    <h3 class="more-expert">More from Expert</h3>
    <div class="more-expert quicktabs-style-nostyle"></div>
</div>

<?php
//$block = block_load('apachesolr-search', 'mlt-001');
//var_dump($arg);

//print render(_block_get_renderable_array( _block_render_blocks( array($block) )));
?>

<?php
$expert_nid = $node->field_video_expert['und']['0']['nid'];
$qmachinename = 'more_videos';
$quicktabs = quicktabs_load($qmachinename);

foreach ($quicktabs->tabs as $tabkey => $tabvalue){

  //dpm($tabvalue);
  if(isset($tabvalue['vid']) && $tabvalue['vid'] == 'videos_by_expert'){
    $quicktabs->tabs[$tabkey]['args'] = $expert_nid;
  }

}
//dpm($quicktabs);

$quicktabs =  quicktabs_build_quicktabs($qmachinename);
//dpm($quicktabs);



//dpm($quicktabs);
//print 'expert nid is '.$expert_nid;
print render($quicktabs);
//dpm($quicktabtheme);
//print 'this is a test';
/* disabled disqus
print '<a name="comments"></a>';
$block = module_invoke('disqus', 'block_view', 'disqus_comments');
print render($block['content']);
*/
?>
