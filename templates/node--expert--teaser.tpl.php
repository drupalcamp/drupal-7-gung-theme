<div class="video-search-result-video expert-thumb" id="expert-<?php print $node->nid?>">
<a class="thumb" href="<?php print url('node/' . $node->nid) ?>">
  <span class="play tooltip" title=""></span>
  <?php
  if(stristr($thumb, '<img ') && stristr($thumb, ' src=') && stristr($tumb, 'brightcove.com')){
    $thumb = kih_experts_thumb_local_img($thumb);
  }
  print render($thumb);
  //print_r($thumb);

  ?>
</a>
<div class="expert_info">
<?php if (isset($node->poloroidImage) && $node->poloroidImage) { ?>
<div class="title"><?php print (!empty($content['field_expert_short_name']) ?  l(render($content['field_expert_short_name']), 'node/' . $node->nid, array('html' => true)) : l($title, 'node/' . $node->nid, array('attributes' => array( 'title' => $title))) ); ?></div>
<div class="job_title"><?php print render($content['field_poloroid_job_title']); ?></div>
<?php } else {?>
<div class="title"><?php  $title =  html_entity_decode($title, ENT_QUOTES); print l($title, 'node/' . $node->nid, array('attributes' => array( 'title' => $title))); ?></div>
<div class="job_title"><?php print render($content['field_job_title']); ?></div>
<?php } ?>
</div>


</div>

<?php


?>
