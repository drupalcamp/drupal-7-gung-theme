<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
// print "<pre>"; print_r($variables); print "</pre>";
$out_view = $variables['view']->result;
//print "<pre>"; print_r($out_view); print "</pre>";
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <?php
  $product = $out_view[$id];
  $node = $product->_field_data['nid']['entity'];
  $node_wrapper = entity_metadata_wrapper('node', $node);
  $layout = $node_wrapper->field_product_layout_template->value();
  //print "<pre>$id | layout= "; print_r($layout); print "</pre>";

  if ($classes_array[$id]) {
    //print ' class="' . $classes_array[$id] .'"';
    $row_classes = $classes_array[$id] . " product-layout-$layout";
  }
  else{
    $row_classes = "views-row product-layout-$layout";
  }
  ?>
  <!-- <?php if($id==7):?><div class="views-row-89"> <?php endif;?> -->

  <div<?php  print ' class="' . $row_classes .'"'; ?>>
  <div class="layout-inner-<?php echo $layout;?>">
    <?php print $row; ?>
  </div>
  </div>
  <!-- <div class="clear clear-<?php echo $layout;?>"></div> -->

  <!-- <?php if($id==8):?></div><?php endif;?> -->

<?php endforeach; ?>
