

<div class="quote-pic shadow polaroid4 new facepile_item" style="padding-right: 12px; padding-bottom: 15px; margin-left: 30px;">
<?php $node->poloroidImage = TRUE;
$expert_poloroid = render(node_view($node, 'teaser'));
//print $expert_poloroid; ?>



<div class="video-search-result-video expert-thumb" id="expert-<?php print $node->nid?>">
<a class="thumb" href="<?php print url('node/' . $node->nid) ?>">
  <span class="play tooltip" title=""></span>
  <?php print render($thumb); ?>
</a>
<div class="expert_info">
<?php if (isset($node->poloroidImage) && $node->poloroidImage) { ?>
<div class="title"><?php print (!empty($content['field_expert_short_name']) ?  l(render($content['field_expert_short_name']), 'node/' . $node->nid, array('html' => true)) : l($title, 'node/' . $node->nid, array('attributes' => array( 'title' => $title))) ); ?></div>
<div class="job_title"><?php print render($content['field_poloroid_job_title']); ?></div>
<?php } else {?>
<div class="title"><?php  $title =  html_entity_decode($title, ENT_QUOTES); print l($title, 'node/' . $node->nid, array('attributes' => array( 'title' => $title))); ?></div>
<div class="job_title">
<small><span style="font-size: 11px;">
<?php print render($content['field_job_title']); ?>
</small></span>
</div>
<?php } ?>
</div>


</div>







</div>



<div class="expertbio-newsletter" style="clear: both;"><?php print render($content['field_expert_bio']); ?></div>

<?php
hide($content['comments']);
hide($content['links']);
?>

		  
