<?php
  /* @TODO: This code has potential performance
  implications and should be refactored to not
  load the entire node.
   */
  if(isset($result['loadedNode'])){
    $node =  $result['loadedNode'];
  }else{
    return;
  }
  // if(empty($node)) {
  // 	return;
  // }
  $output = '';

  //print "<pre>$node->type; >>> </pre>" . $node->type;
  //print " <pre>template:  "; 	var_dump($node); print "</pre>";

 if($node->type == 'videos') {
	  $node->extra_info = '';
		$node->ajax_vid = FALSE;

		$path = url('node/' . $node->nid);

		$node->path = $path;
		$output .= render(node_view($node, 'teaser'));

		//print "<pre>videos; >>> </pre>" . $node->type;
 }
 elseif($node->type == 'expert') {
 		$output .= '<div class="shadow new facepile_item polaroid4">';
		$node->poloroidImage = TRUE;;
		$output .= render(node_view($node, 'teaser'));
		$output .= '</div>';
 }
 elseif($node->type == 'playlist') {
 	  $cover_image = field_get_items('node', $node, 'field_playlist_cover_image');
	  if(!empty($cover_image)) {
		  $thumb = render(field_view_field('node', $node, 'field_playlist_cover_image', 'default'));
	  }
	  $play = l('<div class="play tooltip"> </div>', 'node/' . $node->nid, array('html' => true));

	  $playlist_video_nodes = field_get_items('node', $node, 'kih_video_node_references');
    $count = count($playlist_video_nodes);


	  $output = '<div class="playlist_content_item_thumb">';
	  $output .= $play;
	  $output .= $thumb;
	  $output .= '</div>';
	  $output .= '<div class="text">';
	  $output .= '<div class="title">' . l($node->title, 'node/' . $node->nid) . '</div>';
	  $output .= '<div style="color: #4B4B4B">' . $count . ' Videos in Playlist</div>';
	  $output .= '</div>';
 }
 // dpm(get_defined_vars(), 'defined vars');
?>
<!-- <div class="panel"> -->
  <?php
  $row_class = ( isset($result['bundle']) && $result['bundle'] == 'videos' )
                ? '<li class="search_row twelve mobile-twelve columns">'
                : '<li class="search_row">';
    print $row_class;
  ?>
	<div class="video-search-result-video">
  	<?php
  	  print $output;
  	?>
	</div>
 </li>
<!-- </div> -->
