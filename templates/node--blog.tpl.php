<link rel="stylesheet" media="all" href="<?php echo $base_url;?>/sites/all/themes/gung_theme/css/style-blog.css" />
<?php
$username = $node->name;
$usernamenospace = str_replace(' ', '-', $username);
$shortdate =  	format_date($created, 'tiny');
//dpm($content['field_article_image']);
//print "<pre>"; print_r($content['field_article_image']); print "</pre>";
$thumbnail_google = $base_url . "/sites/default/files/styles/thumbnail/public/".
          $content['field_article_image']['#items'][0]['filename'];
print "<meta name=\"thumbnail\" content=\"$thumbnail_google\" />";

$social_buttons = '<div id="video_info"><div id="social_buttons">';
$social_buttons .= kih_videos_social_buttons($node->title,  url('node/' . $node->nid, array('absolute' => true)));
$social_buttons .= '</div></div>';

?>

<article<?php print $attributes; ?> itemtype="https://schema.org/BlogPosting">
  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>

   <header>
    <?php
    print '<div class="article-title" itemprop="name"><h1><a href="'.$node_url.'">'.$title. '</a></h1>'
          . $social_buttons. '</div>';
    ?>
  </header>
  <?php print render($title_suffix); ?>
  <?php if ($display_submitted): ?>
  <footer class="submitted"><?php print $shortdate; ?> by <a itemprop="author" href="/blogs/<?php print $usernamenospace; ?>"><?php print $node->name ?></a></footer>
  <?php endif; ?>



  <div<?php print $content_attributes; ?> itemprop="articleBody">
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
    /* print '<div id="video_info"><div id="social_buttons">';
     print kih_videos_social_buttons($node->title,  url('node/' . $node->nid, array('absolute' => true)));
     print '</div></div>'; */

    //print "<pre>"; print_r($content); print "</pre>";
    if( isset($content['field_blog_video'])) {
      print render($content['field_blog_video']);
    }else{
      print render($content['field_article_image']);
    }
    print render($content['body']);
    ?>
  </div>

  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
  </div>
</article>
