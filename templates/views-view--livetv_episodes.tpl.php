<?php
// gung view template for livetv_episodes
// May 31, 2017
//
/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
 drupal_add_js(drupal_get_path('theme', 'gung_theme') . '/js/jquery.masonry.min.js');
 drupal_add_js(drupal_get_path('theme', 'gung_theme') . '/js/jquery.autopager.min.js');
 drupal_add_js(drupal_get_path('theme', 'gung_theme')  . '/js/livetv-episodes.js');

 drupal_add_css(base_path(). 'sites/all/themes/gung_theme/css/style-livetv.css', array('type' => 'external')   );
 $url = drupal_get_path_alias(current_path());
?>

<?php if($url != "premium"):?>
<div class="livetv-home">
  <div class="livetv-text">
    <span class="livetv-words">check&nbsp;out what's happening&nbsp;on Gung Wang live&nbsp;tv!&nbsp;&nbsp;&nbsp;</span>
  </div>
</div>
<div class="signup-premium">
  <a href="/commerce-express-checkout/4/7tWvSjf_xx4YSn5xmNEjQir6WOHbMG5xvINezzCBwf4?" class="landing-btn-red">SIGN UP FOR PREMIUM $4.99/M</a>
</div>
<?php endif;?>

<div id="videos-livetv-episodes">
<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div>

</div>
<div id="videos-livetv-episodes-next"></div>
<?php
$load_items = 20;
$page = isset($_GET['page']) ? $_GET['page'] : 0  ;
$from = $page * $load_items;

$pager_output = l(
    'Next', 'livetv/previous-episodes',
    array('query' => array('page' => $page + 1),
    'attributes' => array('id' => 'next') ));
// the code is working
$pager_output .= '<div class="autopager_loaders" id="solrSearchListsLoader"></div>';
echo $pager_output;
?>
