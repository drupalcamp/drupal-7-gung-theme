<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
<link rel="stylesheet" media="all" href="/sites/all/themes/gung_theme/css/style-jwplayer.css">
<?php
if(!user_is_logged_in()){
  $js_stop = "";
}else{
  $js_js_stop='';
}
print render($content['kih_video_node_references']);
?>

<script>

var list='', html;
var htmlTemp='<div class="ListItems"> ' +
  '  <ul class="playlist-sidebar">' +
  ' <div id="list" style="position:relative;">&nbsp;</div></ul></div>';

var htmlExpert = '<div id="jwplayer-expert-box"></div>';

jQuery('#block-kih-experts-kih-experts-more-from').append(htmlTemp);
jQuery('#video_player_wrapper').after(htmlExpert);

if(document.getElementById("list") != null){
  list = document.getElementById("list");
  html = list.innerHTML;
  // list.innerHTML = htmlTemp;
  console.log("playlist: getById");
}else{
  //console.log(list);
}


var playerInstance = jwplayer("container_jwplayer");
playerInstance.setup({
  playlist:playlistKith,
  displaytitle: true,
  width: "100%",
  autostart: true,
  aspectratio: "16:9",
  ga: {},
  type: "hls",
  hlshtml: true,
  hlsLabels: { "3900":"1080p", "2626":"720p", "1825":"540p", "794":"360p", "512":"270p" },
  qualityLabels: { "3900":"1080p", "2626":"720p", "1825":"540p", "794":"360p", "512":"270p" },
  <?php echo kih_videos_ads_js();?>
});

var cookie_value = Drupal.settings.userid;
//console.log('drupal uid:' + cookie_value);

playerInstance.on('ready',function(){

  var playlist = playerInstance.getPlaylist();
  var v1 = playerInstance.getPlaylistItem(0);
  //setActive(playlist[0]);
  //console.log(fisrtVideo);
  setActiveJquery(v1.index, v1.vid, v1.title, v1.expert, v1.transcript);

  for (var index=0; index<playlist.length; index++){
    var playindex = index +1;
    //console.log(playlist[index].desc);
    var active = (index ==0) ? " active" : "";

    html += "<li class='video" + index + " " + active + " playlist-item-link'><span class='dropt' title='"+ playlist[index].title
    + "'><a href='javascript:playThis(" + index + ")'><img class='thumbnail'src='"
    + playlist[index].image + "'</img><span class='title'>"
    + playlist[index].title + "</span></a><p class='desc'>"
    + playlist[index].desc  + "</p></span></li>";
    list.innerHTML = html;

    jQuery('li.jcarousel-item-' + playindex + ' a.click-processed-processed').attr("href", "javascript:playThis(" + index + ")");
    jQuery('li.jcarousel-item-' + playindex + ' a.click-processed-processed').attr("target", "_self");
  }//END for()

});


initPlaylist(playerInstance);

function initPlaylist(player) {
  jQuery.ajax(player.getPlaylist()).then(function(data) {

    player.on('playlistItem', setActive);

  });
}

function setActive(e) {
  //console.log(e);
  // limit to watch 3 videos only
  var currentIndex = e.item.index;
  //console.log('setActive index=' + currentIndex);
  //console.log('user id=' + cookie_value);
  if(cookie_value == 0 && currentIndex >= 2) {
    playerInstance.stop();
    //document.getElementsByClassName('jw-icon-next')[0].style.display='none';

    if( currentIndex == 2){
      setActiveJquery(e.item.index, e.item.vid, e.item.title, e.item.expert, e.item.transcript);
      videoLockStop();
    }
  }else{
    setActiveJquery(e.item.index, e.item.vid, e.item.title, e.item.expert, e.item.transcript);
  }

}

function setActiveJquery(id, vid, title, expert, transcript){
  //console.log('setActiveJquery index=' + id);

  jQuery('li.playlist-item-link').removeClass('active');
  jQuery('li.video' + id).addClass('active');

  jQuery('#jwplayer-expert-box').html(expert);
  jQuery('#transcription_text').html(transcript);

  jQuery('#jwplayer-video-title').html("<h1>"+ title + "</h1>");
  // load mask of carousel
  jQuery('.nowPlayingMask').hide();
  jQuery('.expert-play-vid[data-bc-video-id="'+ vid +'"]').siblings('.nowPlayingMask').show();
  jQuery('.expert-play-vid[data-bc-video-id="'+ vid +'"]').parent('.video-search-result-video').addClass('active');

  jQuery('.bc-player-ceded457-c411-4c71-9ffe-58f496e7eb74_default').css('background-color', '#fff');


}

function playThis(index) {
  // limit to watch 3 videos only
  if(cookie_value == 0 && index > 2) {
    playerInstance.stop();
    console.log('Stop index=' + index);
  }else{
    playerInstance.playlistItem(index);
  }
}

function videoLockStop(){
  var redirection = window.location.href;
  var oops_lockup = '<div class="video-lockout"><div class="left-vid-sm"><span class="lg-red-ttl">OOPS! </span> <span class="lg-black-stl">Sign Up to Continue Watching Videos. </span> </div> <div class="right-vid-sm"> <img src="/sites/all/themes/gung_theme/images/premium/oops-small.jpg" alt="Oops Sign Up" /> </div> <div class="clear-all"> <a class="grey-button signup-free-bar" href="/kith-register/nojs/choice?destination=' + redirection + '">Sign up for FREE  Access</a></div></div>';
  setTimeout(function() {
    jQuery('#video_player_wrapper').addClass('videocountwrapper');
    //         jQuery('#video_player').addClass('hidevidplayer');
    jQuery('#video_player_wrapper #fullPlaylist').html(oops_lockup);
    //jQuery('#video_player_inner_wrapper').html(oops_lockup);
  }, 2000);
}


</script>
