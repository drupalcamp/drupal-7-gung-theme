<link rel="stylesheet" media="all" href="<?php echo base_path(); ?>sites/all/themes/gung_theme/css/style-livetv.css" />
<script type="text/javascript" src="<?php echo base_path(); ?>sites/all/libraries/jquery.countdown/jquery.countdown.min.js"></script>
<div id="livetv-content" class="grid-8 region region-preface-first">
<?php
kih_couponregister_utm_save_cookie();
// $register_link = ctools_modal_text_button(t('Register for Live Broadcast'), '/kith-register/nojs/choice',
//   t('Register for Live Broadcast'), 'ctools-modal-ctools-kith-wide-style landing-btn-red');
//print "<pre>"; print_r($_COOKIE); print "</pre>";

$i_url = $node->field_icalendar['und'][0]['value'];
$g_url = $node->field_google_calendar['und'][0]['value'];
$o_url = $node->field_outlook_calendar['und'][0]['value'];
if($user->uid ==1){
  //print "<pre>"; print_r($node->disqus); print "</pre>";
}

$destination="";
//$destination = 'destination='. base_path() . drupal_get_path_alias();
//print '<div class="article-title" itemprop="name"><h1 class="red-eraser-2">'. $title. '</h1></div>';

$t = $node->field_tv_event_date['und'][0]['value'];
//2016/11/19 16:0:0 UTC-08:00
$event_time = date( 'Y/m/d H:i:s', $t) . ' UTC-08:00';
$str_social_buttons = kih_videos_social_buttons($node->title,  url('node/' . $node->nid, array('absolute' => true)));

$str_checkout = "/commerce-express-checkout/" . PRODUCT_CODE . "/" . MEMBERSHIP_CODE. "?";

$sign_ban = "<div class=\"signup-premium\">
<a href=\"$str_checkout\" class=\"landing-btn-red livetv-signup\">
<img class=\"livetv-star\" src=\"/sites/all/themes/gung_theme/images/livetv/star1.png\">
GET ACCESS TO FULL VIDEO AND ALL PREMIUM CONTENT - $4.99/MO
<img class=\"livetv-star\" src=\"/sites/all/themes/gung_theme/images/livetv/star1.png\">
</a>
</div>";

$end = 3900 + $t;
$now = time();
$now_time = date( 'Y/m/d H:i:s', $now);
$end_time = date( 'Y/m/d H:i:s', $end);
// print "<code>ini=$t ($event_time) :<br> now=$now ($now_time) <br> end=$end ($end_time)</code>" ;
$sizzle_item = field_get_items('node', $node, 'field_sizzle_reel');
$sizzle_reel = $sizzle_item[0]['value'];
//print "gungwang<code>$sizzle_reel</code>";

if(in_array('premium member', array_values($user->roles) )
      || in_array('premium promotion', array_values($user->roles) ) ){
  $stream_html = $content['field_live_tv_code'];
}
else{
  $stream_html = $sizzle_reel;
  //$stream_html = "<a href=\"$str_checkout\"><img class='sizzle-img' src='/sites/all/themes/gung_theme/images/livetv/sizzle.jpg'></a>";
}

if($now > $t){
  $hide_overlay = "none";
  // $hide_vote_overlay = "none";
    $sign_ban = "";
}
else{
  $hide_overlay = ($node->field_tv_overlay['und'][0]['value']) ?  "block" : "none";
  // $hide_vote_overlay = "block";
  $sign_ban = "";
}

?>

<div class="article-title" itemprop="name">
  <h1 class="red-eraser-2"><?php echo $title; ?></h1>
  <div id="video_info_left"><div id="social_buttons">
    <?php print $str_social_buttons;?>
  </div></div>
</div>

  <div class="livetv-container">
   <!-- <script src="https://content.jwplatform.com/players/nY4eF4in-c3duTpRy.js"></script> -->
  <?php
    print render($stream_html);
    //print '<img class="livetv-bg" src="' . base_path() . 'sites/all/themes/gung_theme/images/livetv/red-couch.jpg" >';

  //$hide_overlay = ($node->field_tv_overlay['und'][0]['value']) ?  "block" : "none";

  $hide_books = ($node->field_show_hide_books['und'][0]['value']) ?  "block" : "none";
  $hide_sponsors = ($node->field_show_hide_sponsors['und'][0]['value']) ?  "block" : "none";
  //print "<pre>"; print_r($node->field_tv_overlay['und'][0]['value']); print "</pre>";
  $hide_media_partners = ($node->field_show_hide_media['und'][0]['value']) ?  "block" : "none";
  $hide_host = ($node->field_show_hide_host['und'][0]['value'])? "block" : "none";

  if(!user_is_logged_in()){
    $overlay_class = "livetv-overlay";
    $register_class = "livetv-register";
    $register_link = '<a href="/kith-register/nojs/choice" class="landing-btn-red" title="Register for Live Broadcast">Register for Live Broadcast</a>'  ;
    $reminder_class = 'livetv-reminder';
    $countdown_class = 'countdown';
    //S$hide_overlay = "block";
  }else{
    $overlay_class = "livetv-overlay-bar";
    $register_class = "livetv-register-hidden";
    $register_link = "";
    $reminder_class = 'livetv-reminder-hidden';
    $countdown_class = 'countdown-top';
  }

  ?>

  <div class="<?php echo $overlay_class; ?>" style="display:<?php echo $hide_overlay?>">
      <div class="<?php echo $countdown_class; ?>">
        <div class="lable">COUNTDOWN:</div>
        <div id="getting-started"></div>
        <script type="text/javascript">
          // PST time
          //var nextTime = new Date("2016/11/19 16:0:0 UTC-08:00").getTime();
          var nextTime = new Date("<?php echo $event_time; ?>").getTime();

          jQuery("#getting-started")
          .countdown(nextTime, function(event) {
            jQuery(this).text(
              event.strftime('%-n day%!D, %H:%M:%S')
            );
          });
        </script>
      </div>
      <div class="<?php echo $register_class; ?>">
        <?php echo $register_link;?>
      </div>
      <!--
      <div class="<?php echo $reminder_class; ?>">
        <div class="lable">SET REMINDER:</div>
        <div class="all-calendar">
          <div class="calendar"><a href="<?php echo $o_url;?>" target="_blank">Outlook</a></div>
          <div class="calendar"><a href="<?php echo $g_url;?>" target="_blank">Google Calendar</a></div>
          <div class="calendar"><a href="<?php echo $i_url;?>" target="_blank">iCalendar</a></div>
        </div>
      </div>
    -->
  </div><!-- END: class="overlay" -->

<?php if($now > $t && $now < $end) : ?>
  <script type="text/JavaScript">
  var intervalSeconds = 18000;
  setInterval(function(){
    jQuery.ajax({
      url: "/livetv/poll-id-json",
      dataType: "json",
      type: "GET",
      data: "",
      cache: false,
        success: function(resp) {
          // console.dir(resp);
           var json = resp[0].field_poll_id;
          // console.dir(json);
          if(json){
            cSrc= 'https://dev2.gungwang.com:8888/#/polls/' + json;
            // cSrc += json;
            if (jQuery("#iframe_poll").attr("src") != cSrc) {
              jQuery("#iframe_poll").fadeIn("slow");
              jQuery("#iframe_poll").attr("src", cSrc);
              console.dir(json);
              //intervalSeconds = 30000;
            }else{
              setTimeout(function(){
                jQuery("#iframe_poll").fadeOut("slow");
              }, 5000);
              //intervalSeconds = 5000;
            }
          }
        }
    });
  },intervalSeconds);
  </script>

  <div class="vote-overlay" style="display:block">
    <iframe id="iframe_poll" class="vote-nodejs" src=""></iframe>
  </div>
<?php endif;?>


  </div><!-- END: class="livetv-container" -->

<?php echo $sign_ban; ?>

<div id="comments_episodes">
  <div  id="comments_button">
    <a class="button submit-button-gray" href="#postcomments">Comment</a>
  </div>
  <div id="episodes_left"><a href="/livetv/previous-episodes" target="_blank">
    <img src="<?php echo base_path(); ?>sites/all/themes/gung_theme/images/livetv/previouse_episodes.jpg" alt="Previous Episodes"></a>
  </div>
</div>


  <div id="livetv-body" itemprop="articleBody">
  <?php
    // We hide the comments and links now so that we can render them later.
    //print "<pre>"; print_r($content); print "</pre>";
    print '<div class="subtitle"><h3><span class="event-date">Live Broadcast: </span>'
    . render($content['field_subtitle']) . '</h3></div>';
    hide($content['comments']);
    hide($content['links']);
    print render($content['body']);
  ?>
  </div>

  <div id="livetv-guest">
  <?php
    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    if(stristr($path, 'index.php')){
      $q_str = explode('=', $_SERVER['QUERY_STRING']) ;
      $view_args = explode('/', $q_str[1]);
      $view_arg = count($view_args) == 2 ? $view_args[1] : '';
      //print $_SERVER['QUERY_STRING'];
      //print_r ($view_args);
    }else{
      $view_args = explode('/', $path);
      $view_arg =  count($view_args) >= 3 ? $view_args[2] : '';

    }
  ?>

  <h1 class="livetv-h1">Appearing on the show:</h1>
  <?php echo views_embed_view('live_tv_special_guest', 'block_4', $view_arg); ?>
  </div>

  <div id="livetv-host" style="display:<?php echo $hide_host?>">
  <h1 class="livetv-h1">Host:</h1>
  <?php echo views_embed_view('live_tv_special_guest', 'block_1', $view_arg); ?>
  </div>

  <div id="livetv-book-product" style="display:<?php echo $hide_books?>">
    <h1 class="livetv-h1">Books and Products:</h1>
    <?php echo views_embed_view('live_tv_special_guest', 'block_5', $view_arg); ?>
  </div>

  <div id="livetv-sponsor" style="display:<?php echo $hide_sponsors?>">
    <h1 class="livetv-h1">Sponsors:</h1>
    <?php echo views_embed_view('live_tv_special_guest', 'block_2', $view_arg); ?>
  </div>

  <div id="livetv-media-partners" style="display:<?php echo $hide_media_partners?>">
    <h1 class="livetv-h1">Media Partners:</h1>
    <?php echo views_embed_view('live_tv_special_guest', 'block_6', $view_arg); ?>
  </div>


  <div class="clearfix">    </div>
</div><!--END id="livetv-content"-->



<div id="livetv-right" class="grid-4 region region-preface-second" >
  <div id="video_info_right"><div id="social_buttons">
    <?php print $str_social_buttons; ?> </div>
  </div>
  <div id="episodes_right"><a href="/livetv/previous-episodes" target="_blank">
    <img src="<?php echo base_path(); ?>sites/all/themes/gung_theme/images/livetv/previouse_episodes.jpg" alt="Previous Episodes"></a>
  </div>
  <div id="livetv-right-content">
  <img id="postcomments" alt="Ask an expert" src="/sites/all/themes/gung_theme/images/livetv/askexpert2.png">
  <!-- <div class="total-comments">  <?php echo $node->comment_count;?> Comments </div> -->
  <?php
   $block = module_invoke('disqus', 'block_view', 'disqus_comments');
   print render($block['content']); // OK OK OK
   ?>
</div>
</div><!--END id="livetv-right"-->
