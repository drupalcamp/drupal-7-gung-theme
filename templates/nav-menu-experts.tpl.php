<?php

//Nav Menu level 1 (All Parents, Pregnancy, etc.)
$medical = taxonomy_get_children(15292);
$therapists = taxonomy_get_children(15289);
$parents = taxonomy_get_children(15291);
$law_money = taxonomy_get_children(15294);
$safety_security = taxonomy_get_children(15290);
$educators = taxonomy_get_children(15287);
$special_needs = taxonomy_get_children(15293);
$other = taxonomy_get_children(15295);

?>

<div id="experts-menu-responsive">
    <select id="experts-cat" class="noSelectBox">
        <option class="all" value="">- Filter by Category -</option>
        <option class="glossary" value="/experts/glossary">A-Z</option>
        <option class="medical" value="/experts/medical">Medical</option>
        <option class="therapists" value="/experts/therapists">Therapists</option>
        <option class="parents" value="/experts/parents">Parents</option>
        <option class="law-money" value="/experts/law-money">Law & Money</option>
        <option class="safety-security" value="/experts/safety-security">Safety & Security</option>
        <option class="educators" value="/experts/educators">Educators</option>
        <option class="special-needs" value="/experts/special-needs">Special Needs</option>
        <option class="other-specialists" value="/experts/other-specialists">Other Specialties</option>
    </select>
    <select id="experts-subcat" class="noSelectBox" disabled="disabled">
        <option class="all" value="">- Select Subcategory -</option>
        <option class="all" value="">All</option>
        <?php show_experts_submenu_options(array(), "glossary"); ?>
        <?php show_experts_submenu_options($medical, "medical"); ?>
        <?php show_experts_submenu_options($therapists, "therapists"); ?>
        <?php show_experts_submenu_options($parents, "parents"); ?>
        <?php show_experts_submenu_options($law_money, "law-money"); ?>
        <?php show_experts_submenu_options($safety_security, "safety-security"); ?>
        <?php show_experts_submenu_options($educators, "educators"); ?>
        <?php show_experts_submenu_options($special_needs, "special-needs"); ?>
        <?php show_experts_submenu_options($other, "other-specialists"); ?>
    </select>
</div>


<div id="nav-menu-block">
	<ul id="" class="experts nav-menu">
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/experts/glossary">A-Z</a></span>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/experts/medical">Medical</a></span>
			<div class="container">
				<table><tr>
				<td width="25%">
					<?php show_submenu($medical, 0); ?>
					<?php show_submenu($medical, 1); ?>
					<?php show_submenu($medical, 2); ?>
					<?php show_submenu($medical, 3); ?>
					<?php show_submenu($medical, 4); ?>
				</td>
				<td width="25%">
					<?php show_submenu($medical, 5); ?>
					<?php show_submenu($medical, 6); ?>
					<?php show_submenu($medical, 7); ?>
					<?php show_submenu($medical, 8); ?>
					<?php show_submenu($medical, 9); ?>
				</td>
				<td width="25%">
					<?php show_submenu($medical, 10); ?>
					<?php show_submenu($medical, 11); ?>
					<?php show_submenu($medical, 12); ?>
					<?php show_submenu($medical, 13); ?>
				</td>
				<td width="25%">

					<?php show_submenu($medical, 14); ?>
					<?php show_submenu($medical, 15); ?>
					<?php show_submenu($medical, 16); ?>
					<?php show_submenu($medical, 17); ?>
				</td>
				</tr></table>
			</div>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/experts/therapists">Therapists</a></span>
			<div class="container">
				<table><tr>
				<td width="25%">
					<?php show_submenu($therapists, 0); ?>
					<?php show_submenu($therapists, 1); ?>
					<?php show_submenu($therapists, 2); ?>
				</td>
				<td width="25%">
					<?php show_submenu($therapists, 3); ?>
					<?php show_submenu($therapists, 4); ?>
					<?php show_submenu($therapists, 5); ?>
				</td>
				<td width="25%">
					<?php show_submenu($therapists, 6); ?>
					<?php show_submenu($therapists, 7); ?>
					<?php show_submenu($therapists, 8); ?>
				</td>
				<td width="25%">
					<?php show_submenu($therapists, 9); ?>
					<?php show_submenu($therapists, 10); ?>
					<?php show_submenu($therapists, 11); ?>
				</td>
				</tr></table>
			</div>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/experts/parents">Parents</a></span>
			<div class="container">
				<table><tr>
				<td width="25%">
					<?php show_submenu($parents, 0); ?>
					<?php show_submenu($parents, 1); ?>
					<?php show_submenu($parents, 2); ?>
					<?php show_submenu($parents, 3); ?>
				</td>
				<td width="25%">
					<?php show_submenu($parents, 4); ?>
					<?php show_submenu($parents, 5); ?>
					<?php show_submenu($parents, 6); ?>
					<?php show_submenu($parents, 7); ?>
				</td>
				<td width="25%">
					<?php show_submenu($parents, 8); ?>
					<?php show_submenu($parents, 9); ?>
					<?php show_submenu($parents, 10); ?>
				</td>
				<td width="25%">
					<?php show_submenu($parents, 11); ?>
					<?php show_submenu($parents, 12); ?>
					<?php show_submenu($parents, 13); ?>
				</td>
				</tr></table>
			</div>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/experts/law-money">Law &amp; Money</a></span>
			<div class="container">
				<table><tr>
				<td width="25%">
					<?php show_submenu($law_money, 0); ?>
					<?php show_submenu($law_money, 1); ?>

				</td>
				<td width="25%">
				   <?php show_submenu($law_money, 2); ?>
					<?php show_submenu($law_money, 3); ?>

				</td>
				<td width="25%">
				    <?php show_submenu($law_money, 4); ?>
					<?php show_submenu($law_money, 5); ?>
				</td>
				</tr></table>
			</div>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/experts/safety-security">Safety &amp; Security</a></span>
			<div class="container">
				<table><tr>
				<td width="25%">
					<?php show_submenu($safety_security, 0); ?>
					<?php show_submenu($safety_security, 1); ?>
					<?php show_submenu($safety_security, 2); ?>

				</td>
				<td width="25%">
				<?php show_submenu($safety_security, 3); ?>
					<?php show_submenu($safety_security, 4); ?>
					<?php show_submenu($safety_security, 5); ?>

				</td>
				<td width="25%">
				   <?php show_submenu($safety_security, 6); ?>
					<?php show_submenu($safety_security, 7); ?>
					<?php show_submenu($safety_security, 8); ?>

				</td>
				<td width="25%">
				<?php show_submenu($safety_security, 9); ?>
					<?php show_submenu($safety_security, 10); ?>
				</td>
				</tr></table>
			</div>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/experts/educators">Educators</a></span>
			<div class="container">
				<table><tr>
				<td width="25%">
					<?php show_submenu($educators, 0); ?>
					<?php show_submenu($educators, 1); ?>
					<?php show_submenu($educators, 2); ?>


				</td>
				<td width="25%">
				  <?php show_submenu($educators, 3); ?>
				  <?php show_submenu($educators, 4); ?>
				   <?php show_submenu($educators, 5); ?>
				</td>
				<td width="25%">

					<?php show_submenu($educators, 6); ?>
					<?php show_submenu($educators, 7); ?>


				</td>
				<td width="25%">
						<?php show_submenu($educators, 8); ?>
					<?php show_submenu($educators, 9); ?>

				</td>
				</tr></table>
			</div>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/experts/special-needs">Special Needs</a></span>
			<div class="container">
				<table><tr>
				<td width="25%">
					<?php show_submenu($special_needs, 0); ?>
					<?php show_submenu($special_needs, 1); ?>
				</td>
				<td width="25%">
					<?php show_submenu($special_needs, 2); ?>
					<?php show_submenu($special_needs, 3); ?>
				</td>
				<td width="25%">
					<?php show_submenu($special_needs, 4); ?>
					<?php show_submenu($special_needs, 5); ?>
				</td>
				<td width="25%">
					<?php show_submenu($special_needs, 6); ?>
					<?php show_submenu($special_needs, 7); ?>
				</td>
				</tr></table>
			</div>
		</li>
		<li class="menu-parent">
			<span class="cat2-h2"><a href="/experts/other-specialists">Other Specialties</a></span>
			<div class="container">
				<table><tr>
				<td width="28%">
					<?php show_submenu($other, 0); ?>
					<?php show_submenu($other, 1); ?>
					<?php show_submenu($other, 2); ?>
					<?php show_submenu($other, 3); ?>
				</td>
				<td width="21%">
					<?php show_submenu($other, 4); ?>
					<?php show_submenu($other, 5); ?>
					<?php show_submenu($other, 6); ?>
				</td>
				<td width="26%">
					<?php show_submenu($other, 7); ?>
					<?php show_submenu($other, 8); ?>
					<?php show_submenu($other, 9); ?>
					<?php show_submenu($other, 10); ?>
				</td>
				<td width="25%">
					<?php show_submenu($other, 11); ?>
					<?php show_submenu($other, 12); ?>
					<?php show_submenu($other, 13); ?>
					<?php show_submenu($other, 14); ?>
				</td>
				</tr></table>
			</div>
		</li>
	</ul>

</div>
