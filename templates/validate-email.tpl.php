<style>
html { background: url('/sites/all/themes/gung_theme/images/landing_page_brick_bg.jpg') repeat !important; font-family: "Franklin Book Gothic", sans-serif; }
body { background: none !important; }

h1 { font-family: Eraser; font-size: 60px; font-weight: normal; color: #eb1d23; margin-bottom: 40px; }
#unsup-page { position: relative; width: 1110px; height: 674px; margin: 0px auto; }
#unsup-content { position: absolute; top: 10px; left: 10px; width: 390px; font-size: 16px; line-height: 1.5em; color: #010101; }

#action-buttons{position: absolute; top: 508px; left: 200px; width: 300px;}
#action-buttons li{ list-style-type: none; float: right; margin-right: 10px;}

</style>
<?php if (preg_match('/(?i)msie [1-7]/', $_SERVER['HTTP_USER_AGENT'])): //browser less than IE8 '/(?i)msie [1-7]/' ?>
<style>
h1 { font-size: 0px; color: transparent; width: 229px; height: 55px; background: url('/sites/all/themes/gung_theme/images/oops.png') no-repeat; }
#unsup-content { top: 210px; }
</style>
<?php endif; ?>

<div id="unsup-page">

	<div id="unsup-content">
	  <img src="/sites/all/themes/gung_theme/images/validate-email.png">
	  <ul id="action-buttons">
	    <li><a class="submit-button" href="/user/me/edit">Finish Profile</a></li>
	    <li><a class="submit-button" href="/home">Watch Videos</a></li>
	  </ul>
		
	</div>

</div>
