<?php print $doctype; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf->version . $rdf->namespaces; ?>>
<head<?php print $rdf->profile; ?>>

<!-- Global site tag (gtag.js) - Google Analytics, Feb. 6, 2018 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo GACODE;?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', '<?php echo GACODE;?>');
</script>


<?php if(NOINDEX): ?>
<meta name="robots" content="noindex">
<?php endif;?>

<?php
/*  Gung: new function to add meta tags for Pinterest Rich Pins */
if(arg(0) == 'node' && !empty(arg(1))) {
  $node = node_load(arg(1));
  if($node->type=="videos"){
    echo kih_videos_schema_org_properties_header($node);
  }
}

global $user;
if (isset($user->uid) && isset($user->name) ) {
  if(drupal_is_front_page()){
    //print "<pre> cache: "; print_r($user); print "</pre>";
    print "";
     drupal_add_http_header('cache-control', 'no-cache, no-store, must-revalidate');
  }
}
?>

<link rel="alternate" href="/" hreflang="en-us" />

  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>

  <link rel="stylesheet" media="all" href="/sites/all/themes/gung_theme/css/responsive.css" />

  <script type="text/javascript">var switchTo5x=true;</script>

  <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1">

<?php if ($plugin = context_get_plugin('reaction', 'block')) : ?>
  <?php
  $html_top = $plugin->block_get_blocks_by_region('html_top');
  print render($html_top);
  ?>
<?php endif; ?>
  <link rel="publisher" href="https://plus.google.com/+gungwang/">


<?php if( isset($hide_checkout_header) && $hide_checkout_header ) : ?>
  <style>
  #zone-menu-wrapper{display: none;}
  #block-search-form{display: none;}
  #region-branding .logo-img {margin-top: 0;}
  #zone-footer {display: none;}
  #section-footer{padding-bottom: 40px;}
  .review-image img{width:300px;}
  .review-image img{ margin:0; padding:0; width:300px;}
  .review-text { padding-top: 38px;}
  .review-text h1 { font-size: 40px;}
  .view-commerce-cart-summary {min-height: 120px;}
  </style>
<?php endif; ?>

<!-- Piwik -->
<script type="text/javascript">
/* tracker methods like "setCustomDimension" should be called before "trackPageView"
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//dev.gungwang.com/stats/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '<?php echo PIWIKSITEID; ?>']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
  */
</script>
<!-- End Piwik Code -->

<!-- Google Tag Manager: Gung: Production or DEV-->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer', '<?php echo GTMCODE;?>');</script>
<!-- End Google Tag Manager -->

</head>
<body <?php print $attributes;?>>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=<?php echo GTMCODE;?>"
  height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>


<!-- indicators for responsive jQuery -->
<div id="responsive-indicator"></div>
<div id="mobile-indicator"></div>

<?php if (drupal_get_path_alias($_GET["q"]) == "coming-soon"): ?>
<?php include("coming-soon.tpl.php"); ?>
<?php elseif (drupal_get_path_alias($_GET["q"]) == "validate-email"): ?>
<?php include("validate-email.tpl.php"); ?>
<?php elseif (drupal_get_path_alias($_GET["q"]) == "unsupported-browser"): ?>
<?php include("unsupported-browser.tpl.php"); ?>
<?php elseif (preg_match('/(?i)msie [2-7]/', $_SERVER['HTTP_USER_AGENT']) && (drupal_get_path_alias($_GET["q"]) != "unsupported-browser")): //browser less than IE8 '/(?i)msie [1-7]/' ?>
<?php  drupal_goto("unsupported-browser"); ?>
<?php else: ?>

  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>


  <?php print $page_top; ?>

  <?php print $page; ?>
  <?php print $page_bottom; ?>
<?php endif; ?>

</body>
</html>
