<?php if (user_access("edit any playlist content")) : ?>
<input type="hidden" id="is_editor" name="is_editor" value="1" />
<style>
.node-playlist-form .vertical-tabs {
	display: block; /* show vertical tabs on editor Playlists page */
}
</style>
<?php else: ?>
<input type="hidden" id="is_editor" name="is_editor" value="0" />
<?php hide($form['field_playlist_cover_image']); ?>
<?php endif; ?>

<div id="playlist-node-form">
  <div style="margin-bottom: 5px; padding-bottom: 5px; border-bottom: 1px solid #ccc;">
   <h3 class="title"><?php print l(t('Play Playlist'), 'node/' . $form['#node']->nid); ?></h3>
   
  </div>

<?php
hide($form['kih_playlist_type']);
hide($form['actions']['preview']);
hide($form['actions']['preview_changes']);

print drupal_render_children($form);
?>
</div>