<?php
$thumbnail_google = $base_url . "/sites/default/files/styles/thumbnail/public/".
          $content['field_article_image']['#items'][0]['filename'];
print "<meta name=\"thumbnail\" content=\"$thumbnail_google\" />";
?>
<article<?php print $attributes; ?> itemtype="https://schema.org/Article">
  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
  <header>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>" itemprop="name"><?php print $title ?></a></h2>
  </header>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($display_submitted): ?>
  <footer class="submitted"><?php print $date; ?> -- <?php print $name; ?></footer>
  <?php endif; ?>


  <?php
    print '<div class="article-title" itemprop="name"><h1>'. $title. '</h1></div>';
    $created = date('M d, Y', $created);
    print '<div class="date" itemprop="dateCreated">'. $created. '</div>';
    //var_dump($field_other_author);
    if (isset($field_other_author['und'][0])) {
      $byline = $field_other_author['und'][0]['safe_value'];
    }
    foreach ($field_article_expert_reference['und'] as $author) {
//    $node = node_load($field_article_expert_reference['und'][0]['nid']);
//    print '<div class="author">By: '. l($node->title, 'node/'. $node->nid). '</div>';
      $node = node_load($author['nid']);

// We should really have a comma between everything but the last one
// but Andrea assures there will never be more than two....
      if ($byline) {
        $byline .= ' & ' . l($node->title, 'node/'. $node->nid);
      } else {
        $byline .= l($node->title, 'node/'. $node->nid);
      }
    }
//    if (strpos($byline, ', ')) {
//      $byline = substr_replace($byline, ' & ', strrpos($byline, ', '), 2);
//    }
	print '<div class="author" itemprop="author">By: ' . $byline . '</div>';
  print '<div id="video_info"><div id="social_buttons">';
  print kih_videos_social_buttons($node->title,  url('node/' . $node->nid, array('absolute' => true)));
  print '</div></div>';

  ?>

  <div<?php print $content_attributes; ?> itemprop="articleBody">
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
      print '<h3>Related Articles You May Like</h3><div class="related-articles">'. $related_articles. '</div>';
      print $expert_bio;
    ?>
  </div>

  <div class="clearfix">


    <?php print render($content['comments']); ?>
  </div>
</article>
