<article<?php print $attributes; ?>>
  <header>
    <?php if ($new): ?>
      <em class="new"><?php print $new ?></em>
    <?php endif; ?>
    <?php if (isset($unpublished)): ?>
      <em class="unpublished"><?php print $unpublished; ?></em>
    <?php endif; ?>
  </header>

  <?php print $picture; ?>

  <footer class="comment-submitted">
   <?php
      $uid = $comment_author->uid;
      print $author_image. '<div class="author">'. l($comment_author->name, 'user/'. $comment_author->uid). '</div>';
      $date = strtotime($datetime);
      $date = date('M j, Y', $date);
      $calc = time() - strtotime($datetime);
      // print 'time ago' if the comment was made less than 3 days ago
      if($calc < 60*60*24*3) {
        print '<div class="time"><time datetime="' . $datetime . '">' . $created . '</time></div>';
      }
      else {
        print '<div class="time">'. $date. '</div>';
      }
    ?>
  </footer>

  <div<?php print $content_attributes; ?>>
    <?php
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <?php if ($signature): ?>
    <div class="user-signature"><?php print $signature ?></div>
  <?php endif; ?>

  <?php if (!empty($content['links'])): ?>
    <nav class="links comment-links clearfix"><?php print render($content['links']); ?></nav>
  <?php endif; ?>

</article>
