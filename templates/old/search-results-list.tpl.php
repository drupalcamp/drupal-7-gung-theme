<?php

/**
 * @file
 * Default theme implementation for displaying search results.
 *
 * This template collects each invocation of theme_search_result(). This and
 * the child template are dependent to one another sharing the markup for
 * definition lists.
 *
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 *
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 *
 *
 */
?>
<?php if ($search_results): ?>
<div class="view-video-search-results-list">
<div class="view-content">
<h3 class='search-term'>
	<span>"<?php print $search_term; ?>"</span>
</h3>
 <table class="search-table views-table">
	<thead>
		<tr>
			<th class="play-button">
				Play	
			</th>
			<th class="name-title">
				Title
			</th>
			<th class= "expert-title">
				Expert
			</th>
			<th class= "job-title">
				Job Title	
			</th>
		</tr>
	</thead>
	
  <tbody class="search-results <?php print $module; ?>-results">
    <?php foreach($search_results as $result): ?>
    	<?php print $result; ?>
  	<?php endforeach; ?>
  </tbody>
  <?php print $pager; ?>
<?php else : ?>
  <h2><?php print t('Your search yielded no results');?></h2>
  <?php print search_help('search#noresults', drupal_help_arg()); ?>
</div>
</div>
<?php endif; ?>
