<?php
/* @TODO: This code has potential performance
implications and should be refactored to not
load the entire node.
 */
$title = $result['title'];
$nid =  $result['node']->entity_id;
$node = node_load($nid);
$video_id = $node->field_video_source['und'][0]['video_id'];
//$video = brightcove_video_load($video_id);
$expert_nid = $node->field_video_expert['und'][0]['nid'];
//$path = $video->thumbnailURL;
$url = $result['link'];
$node_expert = node_load($expert_nid);
$expert_name = $node_expert->title;
$expert_job = $node_expert->field_job_title['und'][0]['value'];

  $expert_url = drupal_lookup_path( 'alias', 'node/'.$node_expert->nid  );
if (!$expert_url) {
	//true
	$expert_url = "expert/". $nid;
	}

  ?>

  <tr class="search_row">
  <td class="play-button">
    <a class="use-ajax-vid" href="<?php print $url; ?>"><span class="play"></span></a>
  </td>
  <td class="videoname views-field views-field-title">
    <a class="use-ajax-vid tooltip" href="<?php print $url; ?>" title="<?php print $title; ?>"><?php print $title; ?></a>
  </td>
  <td class="expert-name">
    <a title="<?php print $expert_name; ?>" href="/<?php print $expert_url; ?>" class="use-modal"><?php print $expert_name; ?></a>
  </td>
  <td class="expert-job">
    <?php print $expert_job; ?>
  </td>
  </tr>
