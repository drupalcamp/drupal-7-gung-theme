
<?php
$current_uid = $user->uid;
$profile_uid = arg(1);
if ($profile_uid == "me") $profile_uid = $current_uid;
$profile_user = user_load($profile_uid);

//get if user is a Professional
$is_professional = false;
if (in_array('professional', array_values($profile_user->roles))) {
	$is_professional = true;
}

//clean out orphaned playlist node references (from before 2013-01-22 when Edit Playlist node reference bug was fixed)
$playlist_nids = $form['#user']->kih_playlist_node_references['und'];
foreach ($playlist_nids as $k=>$v) {

	//$playlist_node = node_load(array('nid' => $v['nid']));
	$playlist_node = node_load( $v['nid']);

	if ($playlist_node == false) {
		$num_deleted = db_delete('field_data_kih_playlist_node_references')			//clear node references
			->condition('kih_playlist_node_references_nid', $v['nid'])
			->execute();
		$num_deleted = db_delete('field_revision_kih_playlist_node_references')		//clear node reference revisions
			->condition('kih_playlist_node_references_nid', $v['nid'])
			->execute();
		$num_deleted = db_delete('cache_field')		//clear out user cache row
			->condition('cid', 'field:user:' . $profile_uid)
			->execute();
	}
}
?>

<div id="user-profile-form">

	<div id="user-profile-tabs">
		<ul id="user-profile-tabs-list">
		<li id="tab-1">About You</li>
		<?php  if(true):// if (!$is_professional): ?>
		<li id="tab-2">Family</li>
		<?php endif; ?>
		<li id="tab-3">Account Information</li>
		</ul>
	</div>

	<div id="user-profile-content">
		<div id="content-1" class="content">
			<h2>About You</h2>
			<?php
			print render($form['field_first_name']);
			print render($form['field_last_name']);
			print render($form['picture']);
			if (!$is_professional) {
				print render($form['field_username_or_real_name']);
				print render($form['field_dob']);
				print render($form['field_current_town']);
				print render($form['field_home_town']);
				print render($form['field_marital_status']);
				print render($form['field_profession']);
				print render($form['field_interests']);
				print render($form['field_advice']);
			}
			else {
				print render($form['field_profession']);
				$form['field_current_town']['und'][0]['value']['#title'] = "Located In";
				print render($form['field_current_town']);
				print render($form['field_short_description']);
				print render($form['field_phone']);
				$form['field_website']['und'][0]['url']['#size'] = 45;
				print render($form['field_website']);
				print render($form['field_email']);
				print render($form['field_recommended_experts']);
				print render($form['field_recommended_resources']);
				$form['field_philosophy']['und'][0]['value']['#title'] = "My Philosophy";
			}
			print render($form['field_philosophy']);
		 print render($form['field_badges']);
			?>
		</div>

		<?php if(true): //if (!$is_professional): ?>
		<div id="content-2" class="content">
			<h2>Family</h2>
			<?php
			print render($form['field_is_parent']);
			print render($form['field_is_expecting']);
			print render($form['field_is_trying']);
			print render($form['field_is_adopting']);
			print render($form['field_is_in_parenting']);
			?>

			<div id="children-info">
			<h3>Children</strong></h3>
			<?php
			print render($form['field_children']);
			?>
			</div>
		</div>
		<?php endif; ?>

		<div id="content-3" class="content">
			<h2>Account Information</h2>
			<!--<div class="form-item form-type-textfield form-item-name">
			<label for="edit-name">Username</label>
			<?php print $form['account']['name']['#value']; ?>
			</div>-->
			<?php
			print render($form['account']['name']);
			print render($form['account']['mail']);
			print render($form['account']['pass']);
			print render($form['account']['current_pass']);
			?>
		</div>

	</div>

	<!--<h2>Playlists</h2>
	<div id="playlist-info">
	<?php
	//print render($form['kih_playlist_node_references']);
	?>
	</div>-->

	<?php
	//print_r($form);
	?>
</div>

<br/>
<?php
print render($form['actions']);
print render($form['form_build_id']);
print render($form['form_id']);
print render($form['form_token']);
