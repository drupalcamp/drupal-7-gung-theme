<?php
//handle IE9 gradient backgrounds
header('Content-type: image/svg+xml; charset=utf-8');
echo '<?xml version="1.0"?>
';

$from_stop = isset($_GET['from']) ? $_GET['from'] : '000000';
$to_stop = isset($_GET['to']) ? $_GET['to'] : '000000';
$to_pct = isset($_GET['to_pct']) ? $_GET['to_pct'] : '80';
?>
<svg xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" version="1.0" preserveAspectRatio="none" width="100%" height="100%">
    <defs>
        <linearGradient id="linear-gradient" x1="0%" y1="0%" x2="0%" y2="100%" spreadMethod="pad">
            <stop offset="0%" stop-color="#<?=$from_stop?>" stop-opacity="1"/>
            <stop offset="<?=$to_pct?>%" stop-color="#<?=$to_stop?>" stop-opacity="1"/>
        </linearGradient>
    </defs>
    <rect width="100%" height="100%" style="fill: url(#linear-gradient);"/>
</svg>
