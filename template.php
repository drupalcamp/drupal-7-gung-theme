<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 *
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */


function gung_theme_menu_link(array $variables){

  if(in_array('leaf',$variables['element']['#attributes']['class'])){
    $variables['element']['#attributes']['class'][] = 'is-leaf';
  }

  if(in_array('expanded',$variables['element']['#attributes']['class'])){
    $variables['element']['#attributes']['class'][] = 'is-expanded';
  }

  $element = $variables ['element'];
  $sub_menu = '';

  if ($element ['#below']) {
    $sub_menu = drupal_render($element ['#below']);
  }
  $output = l($element ['#title'], $element ['#href'], $element ['#localized_options']);
  return '<li' . drupal_attributes($element ['#attributes']) . '>' . $output . $sub_menu . "</li>\n";

}

function gung_theme_preprocess_html(&$variables) {

  //print "<pre>"; print_r($variables); print "</pre>";
	drupal_add_css('//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700|Oswald|Yanone+Kaffeesatz', array('type' => 'external'));

  drupal_add_js(drupal_get_path('theme', 'gung_theme') . '/js/jquery.textfill.min.js');
	drupal_add_js(drupal_get_path('theme', 'gung_theme') . '/js/jquery.hoverIntent.min.js');
	drupal_add_js(drupal_get_path('theme', 'gung_theme') . '/js/jquery-checkbox-switch.js');
	drupal_add_js(drupal_get_path('theme', 'gung_theme') . '/js/jquery.placeholder.min.js');
	drupal_add_css(drupal_get_path("theme", "gung_theme") . '/css/font-face.css');
	drupal_add_css(drupal_get_path("theme", "gung_theme") . '/css/navigation.css');
	drupal_add_css(drupal_get_path("theme", "gung_theme") . '/css/ie-lte-8.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'preprocess' => FALSE));

	//for Coming Soon page
	$theme = alpha_get_theme();
	$variables['messages'] = $theme->page['messages'];

	if(isset($theme->page['node']->body['und'][0]['value'])){
    $variables['content'] = $theme->page['node']->body['und'][0]['value'];
  }

     $term = menu_get_item();
     if(isset($term['page_arguments'][0]->vid)) {
       $expert_vocab = taxonomy_vocabulary_machine_name_load('experts_categories');
	     if($term['page_arguments'][0]->vid == $expert_vocab->vid) {
  		   $variables['classes_array'][] = 'page-experts';
	     }
     }
  $variables['classes_array'][] = 'kith-desktop';

  if( preg_match('/checkout\/(\d+)/', $_GET['q']) ){
        $variables['hide_checkout_header'] = TRUE;
  }

  // Gung: Google Pagespeed Insight:  Compressing responsive.css;
  //drupal_add_css( drupal_get_path("theme", "gung_theme") . '/css/responsive.css',  array('group' => CSS_THEME, 'weight' => CSS_THEME));

     //print"gungwangmobile";
  if($_GET['q'] == "vip"){
     //print $_GET['q'];
     //print "<pre>"; print_r($variables['theme_hook_suggestions']); print "</pre>";
     //$variables['theme_hook_suggestions'][] = "html__vip_form";
     //print "<pre>"; print_r($variables['theme_hook_suggestions']); print "</pre>";
  }
}

/**
 * Implements hook_process_zone().
 */
function gung_theme_process_zone(&$vars) {
  $theme = alpha_get_theme();

  if ($vars['elements']['#zone'] == 'preface') {
    $vars['messages'] = $theme->page['messages'];
    $vars['breadcrumb'] = $theme->page['breadcrumb'];
  }

   $vars['mobile_searchform_other'] = __get_mobile_searchform_other();
}

/**
 * Override or insert variables into the page templates.
 */
// -- Delete this line if you want to use these functions
function gung_theme_preprocess_page(&$vars) {
//  dpm($vars);
  //print "<pre>"; print_r($vars); print "</pre>";
  if (!empty($vars['node'])) {
    $vars['theme_hook_suggestions'][] = 'page__node_' . $vars['node']->type;
  }
  if(isset($vars['page']['content']['content']['content']['system_main']['content']['no_content'])) {
    unset($vars['page']['content']['content']['content']['system_main']['content']['no_content']);
    // unset($variables['page']['content']['system_main']['no_content']);
  }


  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $term = taxonomy_term_load(arg(2));
    $parents = taxonomy_get_parents(arg(2));
    if($term->vocabulary_machine_name == 'categories' && empty($parents) ){
      $vars['theme_hook_suggestions'][] = 'page__taxonomy_categories';
    }
  }
  /* Gung CJ tracking code  */
  //print $_GET['q']; print_r(arg());
	//print "<pre>"; print_r($vars); print "</pre>";
	// $_GET['q']) = node/47772
  /*
	if( preg_match('/baby-care-101/', drupal_get_path_alias($_GET['q']) ) ){
	    $_SESSION['PRODUCTID'] = 1;
        __set_tracking_code_cj(1);
	    //print "<pre>pro_id="; print_r($order_array); print "</pre>";
    }else
    if( preg_match('/go-premium-to-view/', drupal_get_path_alias($_GET['q']) ) ){
        $_SESSION['PRODUCTID'] = 2;
        __set_tracking_code_cj(2);
    }

    //print  getcwd();  // /home/gung_theme/apps/drupal/dev2/www/sites/dev2.gungwang.com/settings.php
    if( preg_match('/checkout\/(\d+)\/review/', $_GET['q'], $order_array)  ){
        //print "<pre>"; print_r($order_array); print "</pre>";
        __set_tracking_code_shareasale($order_array);
    }
    if( preg_match('/orders\/me/', $_GET['q']) ){
        $vars['shareasale'] = __get_tracking_code_shareasale();
        __add_cj_udo_code();
    }
    */
    if( preg_match('/checkout\/(\d+)\/review/', $_GET['q'], $order_array)  ){
      __set_tracking_code($order_array);
      //__set_tracking_code_shareasale($order_array);
      //print "<pre>"; print_r($order_array); print "</pre>";
    }
    __add_cj_affiliate_js();

    if( preg_match('/orders\/me/', $_GET['q']) ){
      $vars['shareasale'] = __get_tracking_code_shareasale();
      __add_cj_udo_code();
      unset($_SESSION['PRODUCTID']);
    }

}

function gung_theme_preprocess_region(&$vars) {

	if ($vars['elements']['#region'] == 'content') {
		$path = drupal_get_path_alias();
		//set up template for top level menu category pages (All Parents, Pregnancy, etc.)
		if (($path == "videos/all-parents") || ($path == "videos/pregnancy") || ($path == "videos/adoption") || ($path == "videos/first-year") || ($path == "videos/toddler") || ($path == "videos/preschool") || ($path == "videos/elementary") || ($path == "videos/teen") || ($path == "videos/special-needs")) {


		/*//this is the better way to determine if user is on top level category page, but not working in Context PHP for website_videos, so hard code instead
		if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
			print drupal_get_path_alias();
			$tid = arg(2);
			$result = db_query("SELECT COUNT(*) as cnt FROM taxonomy_term_hierarchy LEFT JOIN taxonomy_term_data ON taxonomy_term_hierarchy.tid = taxonomy_term_data.tid WHERE taxonomy_term_data.vid=2 AND taxonomy_term_hierarchy.parent=0 AND taxonomy_term_hierarchy.tid=" . $tid);
			foreach ($result as $record) {
				$count = $record->cnt;
			}
			if ($count > 0) */
			$vars['theme_hook_suggestions'] = array();
			$vars['theme_hook_suggestions'][] = 'node__taxonomy';
		}
	}
}

/**
 * Override or insert variables into the page templates.
 */
// -- Delete this line if you want to use these functions
function gung_theme_preprocess_node(&$vars) {

  //print "<pre>"; print_r($vars); print "</pre>";
	if(isset($vars['view_mode']) && isset($vars['node'])) {
		$vars['theme_hook_suggestions'][] = 'node__' . $vars['node']->type . '__' . $vars['view_mode'];
	}
  if($vars['type'] == 'article'){
    //Create About Author Section
    //    $expert = node_load($vars['field_article_expert_reference']['und'][0]['nid']);
  //    $vars['expert_bio'] = expert_bio_block($expert);
  if ( isset($vars['field_article_expert_reference']['und'])
    && is_array($vars['field_article_expert_reference']['und']) ){

      foreach ($vars['field_article_expert_reference']['und'] as $ex_ref) {
        $expert = node_load($ex_ref['nid']);
        $vars['expert_bio'] = expert_bio_block($expert);
      }
    }


    $vars['related_articles'] = related_articles_block();
  }


}
function expert_bio_block($expert) {
  $expert_photo = $expert->field_profile_photo['und'][0]['uri'];
  $view = views_get_view('mobile_expert_videos');
  $view->set_display('mobile_expert_videos-block');
  $view->set_arguments(array($expert->nid));
  $view->execute();
  $count = count($view->result);

  $output = '<div class="expert-bio-block"><div class="expert-bio-block-inner"><span class="paperclip"></span>';
  $output .= '<div class="left"><div class="image-group">';
  $output .= l('<img src="'. file_create_url($expert_photo). '">', 'node/'. $expert->nid, array('html' => TRUE));
  $output .= l('<div class="info"><div class="title">'. $expert->title. '</div>', 'node/'. $expert->nid, array('html' => TRUE));
  $output .= $expert->field_job_title['und'][0]['value']. '</div></div></div>';
  $output .= '<div class="right"><h2>'. $expert_title = 'About '. $expert->title. '</h2>';
  $output .=  $expert->field_expert_bio['und'][0]['value']. '</div>';
  $output .= '</div></div>';
  $output .= '<div class="expert-bio-block-bottom"><div class="inner">'. l(t('Watch more videos by '). $expert->title, 'node/'. $expert->nid). '<div class="count">'. $count. t(' videos'). '</div></div></div>';
  return $output;
}

function related_articles_block() {
  $output="";
  $related_content = module_invoke('apachesolr_search', 'block_view', 'mlt-003');

  if( isset($related_content['content']['#docs']) &&
      is_array($related_content['content']['#docs']) )
  foreach($related_content['content']['#docs'] as $related) {
    $related = node_load($related->entity_id);
    if(isset($related->field_article_image)){
      $uri = $related->field_article_image['und'][0]['uri'];
      $output .= '<div class="related-row">'. l('<img src="'. file_create_url($uri). '">', 'node/'. $related->nid, array('html' => TRUE));
      $output .= '<div class="info"><div class="title"><h3>'. l($related->title, 'node/'. $related->nid). '</h3></div>';
      $expert = node_load($related->field_article_expert_reference['und'][0]['nid']);
      $output .= '<div class="expert">'. l($expert->title, 'node/'. $expert->nid). '</div>';
      $output .= '<div class="job-title">'. $expert->field_job_title['und'][0]['value']. '</div></div></div>';
    }
  }
  return $output;
}
//theme User Login/Reg/Pass forms
function gung_theme_form_alter(&$form, &$form_state, $form_id) {

    global $user;

	if ($form_id == "user_register_form") {
		$form['account']['name']['#size'] = 30;
		$form['account']['mail']['#size'] = 30;
		$form['account']['pass']['#size'] = 30;
		$form['field_children']['und']['add_more']['#value'] = 'Add another child';
	}
	else if ($form_id == "user_pass") {
		$form['name']['#size'] = 55;
	}
	else if ($form_id == "user_login") {
		$form['name']['#size'] = 15;
		$form['pass']['#size'] = 15;
		$form['name']['#title'] = "E-mail";
	}
	else if ($form_id == "user_profile_form") {


  if(!in_array('blogger', array_values($user->roles))){
    unset($form['#groups']['group_profile_blog_info']);
    unset($form['#fieldgroups']['group_profile_blog_info']);
  }

  if(!in_array('professional', array_values($user->roles))){
    unset($form['#groups']['group_profile_professional']);
    unset($form['#fieldgroups']['group_profile_professional']);
    unset($form['#groups']['group_profile_professional']);
    unset($form['#fieldgroups']['group_profile_professional']);
  }

 if (!in_array('administrator', array_values($user->roles)) && !in_array('editor', array_values($user->roles))) {
  // Do something.
  //dpm($form);
 // $form['#groups']['group_profile_other']->format_type = 'hidden';
  unset($form['#groups']['group_profile_other']);
  unset($form['#fieldgroups']['group_profile_other']);

  unset($form['#groups']['group_profile_ambassador']);
  unset($form['#fieldgroups']['group_profile_ambassador']);

  unset($form['#groups']['group_profile_refered_by']);
  unset($form['#fieldgroups']['group_profile_refered_by']);
  }


		$form['picture']['picture_upload']['#size'] = 36;
		//$form['actions']['submit']['#submit'][] = 'kith_user_profile_form_submit';
	}
	else if ($form_id == "playlist_node_form") {
		$form['kih_video_node_references']['und']['add_more']['#value'] = 'Add another video';
		$form['actions']['submit']['#submit'][] = 'kith_playlist_node_form_submit';
	}
	else if ($form_id == "forum_node_form") {
		$form['taxonomy_forums']['#title'] = 'Forum';
	}
	else if ($form_id == "videos_node_form") {
		$form['actions']['submit']['#submit'][] = 'kith_videos_node_form_submit';
  }
  elseif($form_id == "comment_node_article_form") {
    $form['actions']['submit']['#value'] = t('Post');
    $form['author']['#access'] = FALSE;
  }
}
function gung_theme_user_profile_form_submit($form, &$form_state) {
	//$form_state['redirect'] = 'user/me';
}

function gung_theme_videos_node_form_submit($form, &$form_state) {
	//$form_state['redirect'] = 'node/1/edit';
}


function gung_theme_playlist_node_form_submit($form, &$form_state) {
	$form_state['redirect'] = 'user/me/playlists';
}


//set up User Register and User Profile form templates
function gung_theme_theme(&$existing, $type, $theme, $path) {
	$hooks = array();
	$hooks['user_register_form'] = array(
		'render element' => 'form',
		'template' => 'templates/user-register',
	);
	$hooks['user_pass'] = array(
		'render element' => 'form',
		'template' => 'templates/user-pass',
	);
	$hooks['user_login'] = array(
		'render element' => 'form',
		'template' => 'templates/user-login',
	);
	$hooks['user_profile_form'] = array(
		'render element' => 'form',
		'template' => 'templates/user-profile-form',
	);
	$hooks['playlist_node_form'] = array(
		'render element' => 'form',
		'template' => 'templates/playlist-node-form',
	);
	$hooks['forum_node_form'] = array(
		'render element' => 'form',
		'template' => 'templates/forum-node-form',
	);
	$hooks['videos_node_form'] = array(
		'render element' => 'form',
		'template' => 'templates/videos-node-form',
		'preprocess functions' => array('kith_preprocess_videos_node_form'),
    );
  $hooks['user_profile_professional_display'] = array(
    'variables' => array('
      user_profile' => NULL,
      'profile_user' => NULL
    ),
    'template' => 'templates/user-profile-professional',
  );
  $hooks['user_profile_regular_display'] = array(
    'variables' => array('
      user_profile' => NULL,
      'profile_user' => NULL
    ),
    'template' => 'templates/user-profile-regular',
  );

	return $hooks;
}

function gung_theme_preprocess_videos_node_form(&$variables) {
	$nid = arg(1);
	$result = db_query("SELECT nid, title FROM node WHERE type='videos' AND title > (SELECT title FROM node WHERE nid=" . $nid . ") ORDER BY title LIMIT 1");
	foreach ($result as $record) {
		$next_nid = $record->nid;
		$next_title = $record->title;
	}
	$variables['next'] = "<a class=\"submit-button\" href=\"/node/" . $next_nid . "/edit\">NEXT VIDEO (" . $next_title . ") &rarr; </a>";

	$result = db_query("SELECT nid, title FROM node WHERE type='videos' AND title < (SELECT title FROM node WHERE nid=" . $nid . ") ORDER BY title DESC LIMIT 1");
	foreach ($result as $record) {
		$prev_nid = $record->nid;
		$prev_title = $record->title;
	}
	$variables['prev'] = "<a class=\"submit-button\" href=\"/node/" . $prev_nid . "/edit\">&larr; PREVIOUS VIDEO (" . $prev_title . ") </a>";

}


function gung_theme_apachesolr_search_mlt_recommendation_block($vars) {

	if($vars['delta'] == 'mlt-002') {
		$expert = menu_get_object();
		foreach($expert->field_expert_category[LANGUAGE_NONE] as $term) {
			$children = taxonomy_get_children($term['tid']);
			if(empty($children)) {$child_tid = $term['tid'];}
		}
		$docs = $vars['docs'];
		$output = '<div id="related_experts">';
    foreach ($docs as $result) {
       $expert_node = node_load($result->entity_id);
       $show = false;
       foreach($expert_node->field_expert_category[LANGUAGE_NONE] as $term) {
         if($term['tid'] == $child_tid) {
         	 $show = true;
         }
       }

       if($show) {
         $expert_node->bcThumbnailOnly = TRUE;
	       $node = node_view($expert_node, 'teaser');
	       $output .= render($node);
       }
    }

    $output .= '</div>';

    return $output;

	} else if($vars['delta'] == 'mlt-001') {
	  $docs = $vars['docs'];
		$output = '<div id="related_videos">';
		$rows = array();
    foreach ($docs as $result) {
      $path = url('node/' . $result->entity_id );
  	  $play = '<div class="play tooltip"> </div>';
  	  $title = '<a title="" href="' . $path . '">' . $play . $result->label . '</a>';

  	  $video_node = node_load($result->entity_id);
  	  $expert_nid = $video_node->field_video_expert[LANGUAGE_NONE][0]['nid'];
      global $theme;
      if($theme != 'kithmobile') {
		    $expert_node = node_load($expert_nid);
  	   $rows[] = array($title, $expert_node->title);
      }
      else {
        $arrow = '<span></span>';
        $rows[] = array($title, $arrow);
      }
    }

    $output .= theme('table', array( 'rows' => $rows));
    $output .= '</div>';


    return $output;
  }
  else {
  //  dpm('regular theme_apachesolr_search_mlt_recommendation_block was called');
		return theme_apachesolr_search_mlt_recommendation_block($vars);
	}
}


/*
 * Theme Overrides
 */
//function gung_theme_username($variables) {


    /*
	$account = $variables['account'];
	if (isset($account->field_username_or_real_name)) {
		if ($account->field_username_or_real_name[LANGUAGE_NONE][0]['value'] == 1) {
			$real_name = $account->field_first_name[LANGUAGE_NONE][0]['value'] . " " . $account->field_last_name[LANGUAGE_NONE][0]['value'];
			if (trim($real_name) != "") $name = $real_name;
		}
	}

	if (!isset($name)) {
		$name = $variables['name'];
	}

	global $user;
	if($account->uid == $user->uid) {
		$variables['link_path'] = 'user/me';
	}

	if (isset($variables['plain'])) {	//"plain" variable used to show Private Messages "To" field without link
		$output = $variables['name'];
	}
	else if (isset($variables['link_path'])) {
		// We have a link path, so we should generate a link using l().
		// Additional classes may be added as array elements like
		// $variables['link_options']['attributes']['class'][] = 'myclass';
		$output = l($name . $variables['extra'], $variables['link_path'], $variables['link_options']);
	}
	else {
		// Modules may have added important attributes so they must be included
		// in the output. Additional classes may be added as array elements like
		// $variables['attributes_array']['class'][] = 'myclass';
		$output = '<span' . drupal_attributes($variables['attributes_array']) . '>' . $name . $variables['extra'] . '</span>';
	}
	return $output;
    */
//}

//show user profile pic if it exists, otherwise show default profile pic
//$variables: account, style_name (e.g. "forum_user_medium"), attributes (e.g. array("class"->"forum-user-picture"))
function gung_theme_user_picture($variables) {
	$account = $variables['account'];
	if (isset($variables['style_name'])) $style_name = $variables['style_name'];
	else $style_name = "forum_user_medium";
	if (isset($variables['attr'])) $attr = $variables['attr'];
	else $attr = array();

	if (!empty($account->picture->uri)) {
		$output = l(theme_image_style(array('style_name'=>$style_name, 'path'=>$account->picture->uri, 'attributes'=>$attr)), 'user/'.$account->uid, array('html'=>true));
	}
	else {
		$output = l(theme('image_style', array('style_name'=>$style_name, 'path'=>'public://pictures/default_profile.png', 'attributes'=>$attr)), 'user/'.$account->uid, array('html'=>true));
	}
	return $output;
}


function gung_theme_preprocess_comment(&$variables){
  if($variables['elements']['#bundle'] == 'comment_node_article') {
    $variables['theme_hook_suggestions'] = array();
    $variables['theme_hook_suggestions'][] = 'comment__articles';
    $variables['attributes_array']['class'][] = 'article-comment';

    $user = user_load($variables['elements']['#comment']->uid);
    $picture = image_style_url('article-comment-thumb-48-48', $user->picture->uri);
    $variables['author_image'] = '<img src="'. $picture. '">';
    $variables['comment_author'] = $user;
    $variables['content']['links']['comment']['#links']['comment_forbidden']['title'] = '';
    $variables['content']['links']['comment']['#links'];
  } else if($variables['elements']['#bundle'] == 'comment_node_videos') {
  	$variables['theme_hook_suggestions'] = array();
    $variables['theme_hook_suggestions'][] = 'comment__videos';
    $variables['attributes_array']['class'][] = 'article-comment';

    $user = user_load($variables['elements']['#comment']->uid);
    $picture = image_style_url('article-comment-thumb-48-48', $user->picture->uri);
    $variables['author_image'] = '<img src="'. $picture. '">';
    $variables['comment_author'] = $user;
    $variables['content']['links']['comment']['#links']['comment_forbidden']['title'] = '';
    $variables['content']['links']['comment']['#links'];
  }
}

function gung_theme_preprocess_comment_wrapper(&$variables){
  if($variables['node']->type == 'article'){
    $variables['theme_hook_suggestions'] = array();
    $variables['theme_hook_suggestions'][] = 'comment_wrapper__articles';
    global $user;
    global $base_url;


    $picture = image_style_url('article-comment-thumb-48-48', $user->picture->uri);
    $output = ($user->uid != 0 ? '<img src="'. $picture. '">' : '<img src="'. $base_url. '/'. drupal_get_path('theme', 'gung_theme'). '/images/annonymous-icon.jpg">');
    $output .= '<div class="user-info"><div class="title">Post a new comment</div>';
    if($user->uid == 0){
      $output .= '<div class="login-button">'. ctools_modal_text_button(t('Log in'), 'ajax_register/login/nojs', t('Sign In'), 'ctools-modal-ctools-ajax-register-style'). '</div>';
    }
    $output .= "<span class='st_facebook'></span>
                <span class='st_google_bmarks'></span>
                <span class='st_twitter'></span>
                <span class='st_email'></span>";
    $output .= '</div>';
    $variables['comment_header'] = $output;
  }
}

/*
  Gung: 12/07/2016
  Purpose: show random background-image of polaroid on experts page
 */
function gung_theme_preprocess_views_view_fields(&$vars) {
  //print "<pre>gungwang:  "; print_r($vars); print "</pre>";
  if ($vars['view']->name == 'expert_directory' ) {
      $vars['classes_array'][] = 'polaroid'. rand(1, 4);
      $vars['classes_array'][] = 'polaroid-common';

  }
}


/*
  Gung: 10/12/2016
  Purpose: fixed the breadcrumb on blog page

 */
function gung_theme_breadcrumb($variables){
  //print "<pre>"; print_r($variables['breadcrumb']);   print "</pre>";
  $home = variable_get('site_name', 'drupal');
  $sep = ' &raquo; ';
  if (count($variables['breadcrumb']) > 0) {
    if( isset($variables['breadcrumb'][1]) && stristr($variables['breadcrumb'][1], 'blog') ) {
      return implode($sep, $variables['breadcrumb']) ;
    }
    else{
      return "";
    }
  }
  else {
     return "" ;
  }
  // else{
  //   $url = drupal_get_path_alias(current_path());
  //   $arry = explode ('/', $url);
  //   return implode($sep, $arry) ;
  // }

  //return ""; // old code to return null;
}


function gung_theme_comment_post_forbidden($variables){
  $node = $variables['node'];

  if($node->type == 'forum'){
    return '';
  }
  else{

    $node = $variables['node'];
    global $user;

    // Since this is expensive to compute, we cache it so that a page with many
    // comments only has to query the database once for all the links.
    $authenticated_post_comments = &drupal_static(__FUNCTION__, NULL);

    if (!$user->uid) {
      if (!isset($authenticated_post_comments)) {
        // We only output a link if we are certain that users will get permission
        // to post comments by logging in.
        $comment_roles = user_roles(TRUE, 'post comments');
        $authenticated_post_comments = isset($comment_roles[DRUPAL_AUTHENTICATED_RID]);
      }

      if ($authenticated_post_comments) {
        // We cannot use drupal_get_destination() because these links
        // sometimes appear on /node and taxonomy listing pages.
        if (variable_get('comment_form_location_' . $node->type, COMMENT_FORM_BELOW) == COMMENT_FORM_SEPARATE_PAGE) {
          $destination = array('destination' => "comment/reply/$node->nid#comment-form");
        }
        else {
          $destination = array('destination' => "node/$node->nid#comment-form");
        }

        if (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)) {
          // Users can register themselves.
          return t('<a href="@login">Log in</a> or <a href="@register">register</a> to post comments', array('@login' => url('user/login', array('query' => $destination)), '@register' => url('user/register', array('query' => $destination))));
        }
        else {
          // Only admins can add new users, no public registration.
          return t('<a href="@login">Log in</a> to post comments', array('@login' => url('user/login', array('query' => $destination))));
        }
      }
    }


  }
}

function gung_theme_preprocess_taxonomy_term(&$variables){
  // dpm($variables, 'variables');
  //print "<pre>"; print_r($variables); print "</pre>";
  //$variables['mobile_searchform_other'] = __get_mobile_searchform_other();
  //print "GUng taxnonmy";
}

function gung_theme_item_list($variables) {
  static $count = 0; // added

  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];

  // Only output the list container and title, if there are any list items.
  // Check to see whether the block title exists before adding a header.
  // Empty headers are not semantic and present accessibility challenges.
  $output = '<div class="item-list">';
  if (isset($title) && $title !== '') {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    $i = 0;
    foreach ($items as $item) {
	 $zebra = ($count % 2) ? 'odd' : 'even'; // added
     $count++; // added
      $attributes = array();
      $children = array();
      $data = '';
      $i++;
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 1) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items) {
        $attributes['class'][] = 'last';
      }
	  $attributes['class'][] = $zebra; // added
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }
  $output .= '</div>';
  return $output;
}


/*
   Gung: rewrite code to set sesstion for CJ and ShareASale code
   Date: March, 2017
   function to generate the tracking code of ShareASale
*/

function __set_tracking_code($order_array){
  // Set ShareASale
  $order = commerce_order_load( $order_array[1]);
  if(!$order){ return false;}
  $_SESSION["KITHCJCODEORDER"] = $order;
  $_SESSION["affiliatekith_ordernumber"] = $order_array[1] ;
  $_SESSION["affiliatekith_amount"] = $order->commerce_order_total['und'][0]['amount'];

  //  Set CJ
  $product_id = __get_product_id($order_array[1]);
  if(!$product_id){ return false;}

  if ($product_id == 1){
    $product_type = 380390;
  }elseif($product_id == PRODUCT_CODE){
    $product_type = 380389;
  }else{
    //$product_type = 380390;
  }
  $_SESSION['PRODUCTID']  = $product_id;
  $_SESSION["KITHCJCODE"] = commerce_product_load($product_id);
  $_SESSION["PRODUCTTYPE"] = $product_type;
  //print "<pre>order-After: "; print_r($_SESSION["KITHCJCODE"]); print "</pre>";
}

function __get_product_id($order_id){
  $args = array(
    ':order_id' => $order_id,
  );
  $product_ids = db_query("SELECT p.commerce_product_product_id
     FROM {commerce_line_item} li
     JOIN {field_data_commerce_product} p ON (p.entity_id = li.line_item_id)
     WHERE li.order_id = :order_id AND li.type = 'product'", $args)->fetchCol();

  return $product_ids[0];
}
/*
function __set_tracking_code_shareasale($order_array){
    //$order = commerce_order_load_multiple(  array(), array('uid' => $user->uid, 'status' => 'cart'), TRUE );
    //print "<pre>session: "; print_r($_SESSION); print "</pre>";
    /*
    if (!commerce_cart_order_session_exists())
    {
        $order = commerce_cart_order_new();
        $order->log = t("Created by checkout page.");
        commerce_cart_order_session_save($order->order_id);
        commerce_cart_order_session_save($order->order_id);
        print "<pre>commerce_cart_order_new :::: "; print_r($order); print "</pre>";
    } else
    {
        //@TODO - need to get the existing order id from somewhere
        $order = commerce_cart_order_load();
    }
    */
/*
    //$order = commerce_order_load_multiple(array($order_array[1]), array(), TRUE);
    $order = commerce_order_load( $order_array[1]);
    //print "<pre>order-After: "; print_r($order); print "</pre>";
    $_SESSION["KITHCJCODEORDER"] = $order;
    $_SESSION["affiliatekith_ordernumber"] = $order_array[1] ;
    $_SESSION["affiliatekith_amount"] = $order->commerce_order_total['und'][0]['amount'];
	return true;
}
*/
function __get_tracking_code_shareasale(){

    //print "<pre>get_tracking session: "; print_r($_SESSION); print "</pre>";
	if(!isset($_SESSION["affiliatekith_ordernumber"])
	   || !isset($_SESSION["affiliatekith_amount"]) ||!isset($_SESSION['PRODUCTID']) ){
		return false;
	}
    $ordernumber = $_SESSION["affiliatekith_ordernumber"];
    $amount = $_SESSION["affiliatekith_amount"]/100;
    $myid = '60172';
    //print "<pre>order-After: "; print_r($order); print "</pre>";

    $element = "<img src=\"https://shareasale.com/sale.cfm?amount=$amount&tracking=$ordernumber&transtype=sale&merchantID=$myid&xtype=special\" width=\"1\" height=\"1\"> ";

    return $element;

}

/*
function __set_tracking_code_cj($product_id){

    //$poduct_id = $_SESSION['PRODUCTID'];
    //print "<pre>product_id= $product_id : "; print_r($product_id); print "</pre>";
    // Baby Care 101: product_id =1;   Premium Subscription: product_id=2
    if ($product_id == 1){
        $product_type = 380390;
    }elseif($product_id == 2){
        $product_type = 380389;
    }
    $_SESSION["KITHCJCODE"] = commerce_product_load($product_id);
    $_SESSION["PRODUCTTYPE"] = $product_type;

    //print "<pre>__set_tracking_code_cj : "; print_r($_SESSION); print "</pre>";

}
*/

function __add_cj_affiliate_js() {

  $html_cj = '
  (function(e) {
    var t = "1978",
    n = document,
    r,
    i,
    s = {
        http : "http://cdn.mplxtms.com/s/MasterTMS.min.js",
        https : "https://secure-cdn.mplxtms.com/s/MasterTMS.min.js"
    },
    o = s[/\w+/.exec(window.location.protocol)[0]];
    i = n.createElement("script"),
    i.type = "text/javascript",
    i.async = !0,
    i.src = o + "#" + t,
    r = n.getElementsByTagName("script")[0],
    r.parentNode.insertBefore(i, r),
    i.readyState ? i.onreadystatechange = function () {
        if (i.readyState === "loaded" || i.readyState === "complete")
            i.onreadystatechange = null
    }
     : i.onload = function () {
        try {
            e()
        } catch (t) {}

    }
  })

  (function () {});

   ';


    drupal_add_js( $html_cj,
        array(
            'type' => 'inline',
            'scope' => 'header',
            'group' => JS_LIBRARY,
            'every_page' => TRUE,
            'weight' => -2,
            )
        );
}// End of add_cj_affiliate_js



function __add_cj_udo_code() {

    $ordernumber = isset($_SESSION["affiliatekith_ordernumber"]) ? $_SESSION["affiliatekith_ordernumber"] : false;
    $order = isset($_SESSION["KITHCJCODEORDER"]) ? $_SESSION["KITHCJCODEORDER"]  : false  ;

    $product = isset($_SESSION["KITHCJCODE"]) ?  $_SESSION["KITHCJCODE"] : false  ;

    $fire_cj = ($ordernumber) ? __check_payment($ordernumber) : false;

     $coupon="";
     $discount=0;
    //print $ordernumber; print $fire_ci;
    if($ordernumber && $order && $fire_cj && $product && isset($_SESSION['PRODUCTID']))
    {
        $fire_cj_str = 'TRUE';

        //$product = $_SESSION["KITHCJCODE"];
        //$wapper_product = entity_metadata_wrapper('commerce_product', $product);
        //print "<pre> wapper_prod : "; print_r($product); print "</pre>";

        //$order = $_SESSION["KITHCJCODEORDER"];
        //$wapper_order = entity_metadata_wrapper('commerce_order', $order);
        //print "<pre> wapper_order : "; print_r($order); print "</pre>";
        $type = isset($_SESSION["PRODUCTTYPE"]) ? $_SESSION["PRODUCTTYPE"] : "" ;

        if(isset($order->commerce_order_total["und"][0]['data']['components'][1])){
          $coupon = $order->commerce_order_total["und"][0]['data']['components'][1]['name'];
          $coupon = array_pop(explode("_", $coupon));
        }

        if(isset($order->commerce_order_total["und"][0]['data']['components'][1]))
          $discount = $order->commerce_order_total["und"][0]['data']['components'][1]['price']['amount'] /100;

        $discount = abs($discount);

        $item = $product->sku;
        $atm = $product->commerce_price["und"][0]["amount"] / 100;
        $currency = $product->commerce_price["und"][0]["currency_code"];
        $qty = 1;

        $cid = 1528650;

        $html_products = "{ 'ITEM' : '$item', 'AMT' : '$atm', 'QTY' : '$qty' }";
        /* public $name;   $amount;    $quantity;    $currency;    $sku;    $internal_refnumber;   $order_id; */

        if( $type==null || $item==null ||  $atm==null){
          $fire_cj_str = 'FALSE';
          $html_udo ="";
        }
        else{
          $html_udo = "
            var MasterTmsUdo = { 'CJ' : {
                'CID': '$cid',
                'TYPE': '$type',
                'DISCOUNT' : '$discount',
                'OID': '$ordernumber',
                'CURRENCY' : '$currency',
                'COUPON' : '$coupon',
                'FIRECJ' : '$fire_cj_str',
                PRODUCTLIST : [
                    $html_products
                    ]
                }
            };
          ";
        }

        drupal_add_js( $html_udo,
            array(
                'type' => 'inline',
                'scope' => 'header',
                'group' => JS_LIBRARY,
                'every_page' => TRUE,
                'weight' => -18,
                //'preprocess' => FALSE,
            )
        );
    }
    // $vars['scripts']

}// End of function add_cj_udo_code



function __check_payment($order_id){

  //if(!$order_id) return false;

  $payments = commerce_payment_transaction_load_multiple(array(), array('order_id' =>  $order_id));
  // If you just have one and that's all you want...

  //print "<pre> payments : "; print_r($payments); print "</pre>";

  $payment = !empty($payments) ? array_shift($payments) : false;

  return true;
  //return $payment;
}



function __get_mobile_searchform_other(){
  return "";
  $str = '
<div id="block-search-form--2-mobile" class="block block-search">
   <form action="/" method="post" id="search-block-form--2" accept-charset="UTF-8">
         <div class="form-item form-type-textfield form-item-search-block-form">
            <label class="element-invisible" for="edit-search-block-form--4">Search </label>
            <input title="Enter the terms you wish to search for." class="custom-solr-autocomplete unprocessed clear-defaults default
             form-text form-autocomplete" type="text" id="edit-search-block-form--4" name="search_block_form"
             value="Search &amp; learn from '. NUM_VIDEOS. ' videos from top experts &amp; parents" size="15" maxlength="128" autocomplete="off">
         </div>
         <div class="form-actions form-wrapper" id="edit-actions--2"><input type="submit" id="edit-submit--2" name="op" value="Search" class="form-submit-search"></div>
         <input type="hidden" name="form_build_id" value="form-50S-pzHpgew7Cx-zV3oXT-VWqiG1S47vkDGZ-mlaG2o">
         <input type="hidden" name="form_id" value="search_block_form">
   </form>
</div>
';

return $str;

}


?>
